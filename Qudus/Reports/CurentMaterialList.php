<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Material Report</h4>
														<span>This Is The Curent <code>Material</code> Store</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">

													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
										</div>

										<div class="card-block">
											<div class="dt-responsive table-responsive">
												<table id="lists" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>ID</th>
															<th>Name</th>
															<th>Unit</th>
															<th>Qty</th>
													</thead>
													<tbody>
													<?php
													require_once '../material/materialClass.php';
													require_once 'reportClass.php';
													$result=getMaterialList();
													$Total= 0;
													while ($row=$result->fetch()) {
														echo '
														<tr>
														<td>'.$row['Id'].'</td>
														<td>'.$row['Name'].'</td>
														<td>'.$row['Unit'].'</td>
														<td>'.CurentStoreMaterialById($row['Id']).'</td>
														</tr>';?>
														<?php
														$Total= CurentStoreMaterialById($row['Id']);
													}
													?>
												</tbody>
											</table>
										</div>
										<!-- Add Model -->



									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#lists').DataTable();
});
</script>
