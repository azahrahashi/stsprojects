<script type="text/javascript">
				$(document).ready(function() {
					$('#error').hide();
				});
				// project registration script
				function savematerial(){
					   // get values from Input
			     var Id =$("#Id").val();
			     var mName =$("#mName").val();
			     var mUnit =$("#mUnit").val();
			     var mUnitPrice =$("#mUnitPrice").val();
			     var mStockLevel =$("#mStockLevel").val();
			     var mstartDate =$("#mstartDate").val();

			    
			    if(mName==''){
			       $('#error').show();
			            $('#mName').focus();
			     }
			     if(mUnit==''){
			       $('#error').show();
			            $('#mUnit').focus();
			     }
			     if(mUnitPrice==''){
			        $('#error').show();
			            $('#mUnitPrice').focus();
			     }	
			     if(mStockLevel==''){
			       $('#error').show();
			            $('#mStockLevel').focus();
			     }
			     if(mstartDate==''){
			        $('#error').show();
			            $('#mstartDate').focus();
			     }			    
			   
			   
			    else{
			      $('#error').hide();           
			        $.post("addMaterial.php", {
			            Id:Id,
			            mName: mName,
			            mUnit: mUnit, 
			            mUnitPrice: mUnitPrice, 
			            mStockLevel: mStockLevel,
			            mstartDate: mstartDate,
			         	

			        }, function (data, status) {    

			     // alert(data);			        

			             swal("You have saved Successfully!", {
				              icon: "success",
				            }).then(function(){ 
				                   location.reload();
				                   }
				                );
			            // close the popup
			         $("#default-Modal").modal("hide");

			            // read records again
			            // clear fields from the popup
			            $("#mName").val("");
			            $("#mUnit").val("");
			            $("#mUnitPrice").val("");
			            $("#mStockLevel").val("");
			            $("#mstartDate").val("");
			          
			           
			        });
			        }				
				}
				/**********************************
            get Project  Detail
            ***********************************/ 
            function getMaterialDetail(Id) {
             $("#Id").val(Id);
               var Id = Id;
         // alert(hiddenId);
          //   // Add User ID to the hidden field for furture usage
            $.post("getMaterialDetail.php", {
                    Id: Id
                },
                function (data, status) {
                 // alert(data);    
                            // PARSE json data
                    var user = JSON.parse(data);
                    // Assing existing values to the modal popup fields      
                    $("#Id").val(user.Id);
                    $("#mName").val(user.Name);
                    $("#mUnit").val(user.Unit);
                    $("#mUnitPrice").val(user.UnitPrice);
                    $("#mStockLevel").val(user.StockLevel);
                    $("#mstartDate").val(user.StartDate);           
                  
                   }
            );
          //   // Open modal popup
              $("#default-Modal").modal("show");

        }

                /*******************************
			    Remove Project by updateing finishDate
				********************************/
    function RemoveMaterial(Id){
        var Id = Id;
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
         $.post("RemoveMaterial.php", {
                Id: Id
               }, function (data, status) {
          swal("Poof! Your imaginary file has been deleted!", {
              icon: "success",
            }).then(function(){ 
                   location.reload();
                   }
                );
            });;

          
          } else {
            swal("Your imaginary file is safe!");
          }
        });


    }
			</script>
			
			<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>