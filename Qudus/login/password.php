<?php
require_once'../Connection/dbconection.php';

function get_employee_password_by_user_id($username){

  $stmt=getCnx()->prepare("SELECT a.AuthorisedId, a.UserName, a.Password, ar.Role FROM authorised a
        INNER JOIN authorisedmembership am ON a.AuthorisedId = am.AuthorisedId
        INNER JOIN authorityrole ar ON am.AuthorityRoleId = ar.AuthorityRoleId
        where a.UserName = :username");

        $stmt->bindParam(':username', $username);
        $stmt->execute();
        return $stmt;
  }

  function update_employee_password_by_user_id($username, $password)
  {
    $stmt =getCnx()->prepare("UPDATE authorised SET authorised.Password = :password WHERE UserName = :username");
    $stmt->bindParam(':username', $username);
    $stmt->bindParam(':password', $password);
    $stmt->execute();
  }
  ?>
