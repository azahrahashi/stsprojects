<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Expenses</h4>
														<span>This Is The <code>Expenses</code> Managment Page.</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											<div>
												<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#AExpense"><i class="fa fa-plus"></i> Add</button>
											</div><br><br><br>
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Amount</th>
															<th>Reason</th>
															<th>Status</th>
															<th>Date</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>

													<tbody>
														
													</tbody>

												</tbody>
											</table>
										</div>

										<!-- Add Stagetype Model -->

								<div class="modal fade" id="AExpense" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Add Expense</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="Amount" name="Amount" class="form-control" placeholder="Amount">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="text" id="Reason" name="Reason" class="form-control" placeholder="Reason">
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<select name="Status" id="Status" class="form-control">
																		<option value="">Select Satus</option>
																		<option value="0">paid</option>
																		<option value="1">retuned</option>
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="date" id="StartDate" name="StartDate" class="form-control" placeholder="Date..">
																</div>
															</div>

															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light "onclick="addExpense()">Add</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- End Add Stagetype Model -->


										<!-- Update Model -->

									<div class="modal fade" id="UExpense" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Update Expense</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="hidden" name="UExpensesId" id="UExpensesId">
																	<input type="text" id="UAmount" name="UAmount" class="form-control" placeholder="Paid by">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="text" id="UReason" name="UReason" class="form-control" placeholder="received by">
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<select name="Ustatus" id="Ustatus" class="form-control">
																		<option value="">Select Satus</option>
																		<option value="0">paid</option>
																		<option value="1">retuned</option>
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="date" id="UStartDate" name="UStartDate" class="form-control" placeholder="Date..">
																</div>
															</div>

															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light "onclick="updateExpense()">Update</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>

										<!-- End Update Model -->


									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script type="text/javascript">

	getAllexpense();
	function getAllexpense() {
		$.ajax({
			url: "getExpenses.php",
			method: "GET",
			success: function (data) {
				$("tbody").html("");
				$("tbody").append(data);
			}
		});
	}
	function addExpense() {
		var Amount = $("#Amount").val();
		var Reason = $("#Reason").val();
		var Status = $("#Status").val();
		var StartDate = $("#StartDate").val();
		var ExpensesId = 0;

		if (Amount == "") {
			alert("Empty is not allowed");
		}
		else if(Reason == ""){
			alert("Empty is not allowed");
		}
		else if(Status == ""){
			alert("Empty is not allowed");
		}
		else if(StartDate == ""){
			alert("Empty is not allowed");
		}
		else{
			$.ajax({
				url: "addExpenses.php",
				method: "POST",
				data: {Amount:Amount,Reason:Reason,Status:Status,StartDate:StartDate,ExpensesId: ExpensesId},
				success: function (data) {
					getAllexpense();
					$('#AExpense').modal('hide');
				}
			});

		}



	}

	function setExpense(ExpensesId,Amount,Reason,Status,StartDate) {


		$("#UExpensesId").val(ExpensesId);
		$("#UAmount").val(Amount);
		$("#UReason").val(Reason);
		if (Status == 0) {
			$('#Ustatus').html('<option value="'+Status+'">paid</option>'
				+'<option value="1">retuned</option>'
				);
		}
		else if (Status == 1) {
			$('#Ustatus').html('<option value="'+Status+'">returned</option>'
				+'<option value="0">paid</option>'
				);
		}
		$("#UStartDate").val(StartDate);
		$("#UExpense").modal("show");
	}

	function updateExpense(){

		var ExpensesId=$("#UExpensesId").val();
		var Amount=$("#UAmount").val();
		var Status = $('#Ustatus').val();
		var Reason = $('#UReason').val();
		var StartDate=$("#UStartDate").val();

		$.post("addExpenses.php",{
			Amount:Amount,
			Status:Status,
			Reason:Reason,
			StartDate:StartDate,
			ExpensesId:ExpensesId
		}, function (data, status) {
			$("tbody").html("");
			getAllexpense();
			$('#UExpense').modal('hide');
		});

	}


	function removeExpense(ExpensesId) {

		$.post("removeExpense.php",{
			ExpensesId:ExpensesId
		},
		function (data, status) {
			$("tbody").html("");
			getAllexpense();
});

	}


</script>
