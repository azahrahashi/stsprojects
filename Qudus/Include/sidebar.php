
<div class="theme-loader">
	<div class="ball-scale">
		<div class='contain'>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
			<div class="ring">
				<div class="frame"></div>
			</div>
		</div>
	</div>
</div>

<nav class="pcoded-navbar">
	<div class="pcoded-inner-navbar main-menu">
		<div class="pcoded-navigatio-lavel">Navigation</div>

		<ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="none" subitem-border="true">
			<li class="active">
				<a href="../Reports/dashboard">
					<span class="pcoded-micon"><i class="feather icon-home"></i></span>
					<span class="pcoded-mtext">Dashboard</span>
				</a>
			</li>
			<li class="">
				<a href="../projects/projectList">
					<span class="pcoded-micon"><i class="icofont icofont-architecture-alt"></i></span>
					<span class="pcoded-mtext">Projects</span>
				</a>
			</li>
			<li class="">
				<a href="../productions/productionList.php">
					<span class="pcoded-micon"><i class="fa fa-gears"></i></span>
					<span class="pcoded-mtext">Productions</span>
				</a>
			</li>
			<li class="">
				<a href="../expense/expenseList">
					<span class="pcoded-micon"><i class="fa fa-line-chart"></i></span>
					<span class="pcoded-mtext">Expenses</span>
				</a>
			</li>
			<li class="">
				<a href="../Purchases/purchaseList">
					<span class="pcoded-micon"><i class="fa fa-cubes"></i></span>
					<span class="pcoded-mtext">Purchases</span>
				</a>
			</li>
			<li class="">
				<a href="../customer/customerList.php">
					<span class="pcoded-micon"><i class="icofont icofont-engineer"></i></span>
					<span class="pcoded-mtext">Customer</span>
				</a>
			</li>
			<li class="">
				<a href="../Selles/sellesList.php">
					<span class="pcoded-micon"><i class="fa fa-shopping-cart"></i></span>
					<span class="pcoded-mtext">Sales</span>
				</a>
			</li>
			<li class="">
				<a href="../projectDocument/projectDocument">
					<span class="pcoded-micon"><i class="fa fa-files-o"></i></span>
					<span class="pcoded-mtext">Projuct Document</span>
				</a>
			</li>
		</ul>

		<ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="none" subitem-border="true">
			<li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="fa fa-gear"></i></span>
					<span class="pcoded-mtext">Setups</span>
				</a>
				<ul class="pcoded-submenu">
					<li class=" ">
						<a href="../warehouse/warehouseList">
							<span class="pcoded-mtext">Warehouse</span>
						</a>
					</li>
					<li class=" ">
						<a href="../PaymentMethod/PaymentMethodList">
							<span class="pcoded-mtext">Payment</span>
						</a>
					</li>
					<li class=" ">
						<a href="../Supplier/SupplierList">
							<span class="pcoded-mtext">Supplier</span>
						</a>
					</li>
					<li class=" ">
						<a href="../Stage/stagelist">
							<span class="pcoded-mtext">Foundations</span>
						</a>
					</li>
					<li class=" ">
						<a href="../StageType/StageTypeList">
							<span class="pcoded-mtext">Foundation Types</span>
						</a>
					</li>
					<li class=" ">
						<a href="../products/productList.php">
							<span class="pcoded-mtext">Products</span>
						</a>
					</li>
					<li class=" ">
						<a href="../material/materialList">
							<span class="pcoded-mtext">Materials</span>
						</a>
					</li>

				</ul>
			</li>
		</ul>

		<ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="none" subitem-border="true">
			<li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="icofont icofont-engineer"></i></span>
					<span class="pcoded-mtext">Empoyee</span>
				</a>
				<ul class="pcoded-submenu">
					<li class=" ">
						<a href="../employee/addEmployee">
							<span class="pcoded-mtext">Add Employee</span>
						</a>
					</li>
					<li class=" ">
						<a href="../employee/employeeList">
							<span class="pcoded-mtext">Employee List</span>
						</a>
					</li>


				</ul>
			</li>
		</ul>

		<ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="none" subitem-border="true">
			<li class="pcoded-hasmenu" dropdown-icon="style1" subitem-icon="style1">
				<a href="javascript:void(0)">
					<span class="pcoded-micon"><i class="fa fa-folder-open"></i></span>
					<span class="pcoded-mtext">Reports</span>
				</a>
				<ul class="pcoded-submenu">
					<li class=" ">
						<a href="../Reports/dashboard">
							<span class="pcoded-mtext">Overview</span>
						</a>
					</li>
					<li class=" ">
						<a href="../Reports/sallaryReporList">
							<span class="pcoded-mtext">Sallery Report</span>
						</a>
					</li>
					<li class=" ">
						<a href="../Reports/selesReport">
							<span class="pcoded-mtext">Salles Report</span>
						</a>
					</li>
					<li class=" ">
						<a href="../Reports/purchaseReport">
							<span class="pcoded-mtext">Purchases Report</span>
						</a>
					</li>

					<li class=" ">
						<a href="../Reports/expensesReport">
							<span class="pcoded-mtext">Expenses Report</span>
						</a>
					</li>

					<li class=" ">
						<a href="../Reports/customerReport">
							<span class="pcoded-mtext">Customers Report</span>
						</a>
					</li>

					<li class=" ">
						<a href="../Reports/CurentMaterialList">
							<span class="pcoded-mtext">Material Report</span>
						</a>
					</li>

					<li class=" ">
						<a href="../Reports/CurentProductList">
							<span class="pcoded-mtext">Products Report</span>
						</a>
					</li>

				</ul>
			</li>
		</ul>

	</div>
</nav>
