<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Payments</h4>
														<span>This Is The <code>Payment</code> Mangment Page.</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">

													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<br>
										<div class="col-md-12">
											<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#aPaymentMethod"><i class="fa fa-plus"></i> Add </button>
										</div>
										<div class="card-block">
											<div class="dt-responsive table-responsive">
												<table id="lists" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Payment No.</th>
															<th>Payment Method</th>
															<th>StatrDate</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<tbody>

													</tbody>
												</tbody>
											</table>
										</div>
										<!-- Add Model -->

										<div class="modal fade" id="aPaymentMethod" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Add PaymentMethod </h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<br>
															<label> Enter PaymentMethod </label>
															<input type="text" name="Name" id="Name" class="form-control" />
															<br>
															<label> Start Date</label>
															<input type="date" name="StartDate" id="StartDate" class="form-control"  />
															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="addPaymentMethod()">Save changes</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>

										<!-- End Add Model -->

										<!-- Update Model -->

										<div class="modal fade" id="uPaymentMethod" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title"> Update PaymentMethod </h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<br>
															<label> Edit PaymentMethod </label>
															<input type="hidden" name="Id" id="Id" class="form-control" />
															<input type="text" name="uName" id="uName" class="form-control" />
															<br>
															<label> Edit Start Date</label>
															<input type="date" name="uStartDate" id="uStartDate" class="form-control"  />
															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="updatePaymentMethod()">Update</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>



										<!-- End Update Model -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script type="text/javascript">


get_payment_mthode();
function get_payment_mthode() {
	$.ajax({
		url: "get_payment_mthode.php",
		method: "GET",
		success: function (data) {
			$("tbody").html("");
			$("tbody").append(data);
		}
	});
}

function addPaymentMethod() {

	var PaymentMethod = $("#Name").val();
	var StartDate = $("#StartDate").val();

	if (PaymentMethod == "") {
		$("#Name").after('<p class="error_message">Payment methode name?</p>');
		$("#WarehouseName").closest('.form-group').addClass('has-error');
	}
	else if (StartDate == "") {
		$(".error_message").remove();
		$("#StartDate").after('<p class="error_message">The date?</p>');
		$("#StartDate").closest('.form-group').addClass('has-error');
	}
	else {
		$(".error_message").remove();
		$.post("addPaymentMethod.php", {
			PaymentMethod: PaymentMethod,
			StartDate: StartDate,
			PaymentMethodId:0
		},
		function (data, status) {
			$('#aPaymentMethod').modal('hide');
			$("tbody").html("");
			get_payment_mthode();
			$("input").val("");
		});
	}

}


function setPaymentMethod(PaymentMethodId,PaymentMethod,StartDate){

	// alert(PaymentMethodId+','+PaymentMethod+','+StartDate);
	$("#Id").val(PaymentMethodId);
	$("#uName").val(PaymentMethod);
	$("#uStartDate").val(StartDate);
	$("#uPaymentMethod").modal("show");

}


//       **************
//       Update function
//       **************

function updatePaymentMethod(){

	var PaymentMethodId=$("#Id").val();
	var PaymentMethod = $("#uName").val();
	var StartDate = $("#uStartDate").val();

	if (PaymentMethod == "") {
		$("#Name").after('<p class="error_message">Payment methode name?</p>');
		$("#WarehouseName").closest('.form-group').addClass('has-error');
	}
	else if (StartDate == "") {
		$(".error_message").remove();
		$("#StartDate").after('<p class="error_message">The date?</p>');
		$("#StartDate").closest('.form-group').addClass('has-error');
	}
	else {
		$(".error_message").remove();
		$.post("addPaymentMethod.php",{
			PaymentMethod:PaymentMethod,
			StartDate:StartDate,
			PaymentMethodId:PaymentMethodId
		}, function (data, status) {
			$('#uPaymentMethod').modal('hide');
			$("tbody").html("");
			get_payment_mthode();
			$("input").val("");
		});
	}

}


//                          //remove function

function removePaymentMethod(PaymentMethodId) {

	$.post("removePaymentMethod.php",{
		PaymentMethodId:PaymentMethodId
	},
	function (data, status) {
		get_payment_mthode();
		$("tbody").html("");
	});

}

</script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#lists').DataTable();
});
</script>
