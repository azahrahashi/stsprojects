<?php
require_once'../Connection/dbconection.php';

    /**********************************
            Add New Project
    ************************************/
function    addProject($pName,$pOwner,$pDetail,$startDate)
{
    $cnx = getCnx();
    $stmt = $cnx->prepare("INSERT INTO projects(Name,Owner,Describtion,StartDate)
            VALUES (:pName,:pOwner,:pDetail,:startDate)");


        $stmt->bindParam(':pName', $pName);
        $stmt->bindParam(':pOwner',$pOwner);
        $stmt->bindParam(':pDetail', $pDetail);
        $stmt->bindParam(':startDate', $startDate);
        $stmt->execute();

}
/**************************************
        Update Project
***************************************/
  function    updateProjectById($pId,$pName,$pOwner,$pDetail){
        $stmt = getCnx()->prepare("UPDATE projects SET Name = :pName ,  Owner = :pOwner ,  Describtion = :pDetail   WHERE  ProjectId = :pId");

        $stmt->bindParam(':pId', $pId);
        $stmt->bindParam(':pName', $pName);
        $stmt->bindParam(':pOwner',$pOwner);
        $stmt->bindParam(':pDetail', $pDetail);
        $stmt->execute();
        return $stmt;


  }
//   /*********************************
//           Terminate Income
//   **********************************/
  function terminateProject($pId)
    {
        $stmt = getCnx()->prepare("UPDATE projects SET   FinishDate = NOW()  WHERE ProjectId = :pId");
        $stmt->bindParam(':pId', $pId);
        $stmt->execute();
        return $stmt;

    }
// /***********************************
//         get Income Detail
// *************************************/
function getProjectDetail($pId)

{
       $stmt = getCnx()->prepare("SELECT * FROM projects WHERE   ProjectId = :pId");
       $stmt->bindParam(':pId', $pId);

        $stmt->execute();
        return $stmt;
}

/************************************
     get Suppliers List
**************************************/
  function getProjectList()
  {
     $stmt =getCnx()->query("SELECT * FROM projects WHERE  FinishDate IS NULL");
     $stmt->execute();
     return $stmt;
  }


    /************************************
         get Projects List
    **************************************/
function getAllProjects(){
   $stmt =getCnx()->query("SELECT * FROM projects WHERE FinishDate IS NULL");
   $stmt->execute();
   return $stmt;
}
   /************************************
         get Product List
    **************************************/
function getProduct(){
  $stmt=getCnx()->query("SELECT * FROM products
    WHERE FinishDate IS NULL");
          return $stmt;
}

       /************************************
             Add Projects List
        **************************************/ 
function addProjects($projects,$subtotal,$discount,$totalAmount,$PaymentMethodId,$date)

  {
    $stmt= getCnx()->prepare("INSERT INTO selles(ProjectId,Amount,Descount,TotalAmount,PaymentMethodId,TotalPaid,SellesDate)
          VALUES (:projects, :subtotal, :discount,:totalAmount, :PaymentMethodId,:totalAmount,:date)");

          $stmt->bindParam(':projects', $projects);
          $stmt->bindParam(':subtotal', $subtotal);
          $stmt->bindParam(':discount', $discount);
          $stmt->bindParam(':totalAmount', $totalAmount);
          $stmt->bindParam(':PaymentMethodId', $PaymentMethodId);
          $stmt->bindParam(':date', $date);
          $stmt->execute();

       return getCnx()->lastInsertId();
  }

      function addProjectsDetail($pId,$ProductId,$Qty,$UnitPrice,$total)
  {
    $stmt= getCnx()->prepare("INSERT INTO sellsdetail(SellsId,ProductId,Qty,Price,Total)
          VALUES (:pId, :ProductId, :Qty,:UnitPrice, :total)");

          $stmt->bindParam(':pId', $pId);
          $stmt->bindParam(':ProductId', $ProductId);
          $stmt->bindParam(':Qty', $Qty);
          $stmt->bindParam(':UnitPrice', $UnitPrice);
          $stmt->bindParam(':total', $total);
          $stmt->execute();

  }
?>
