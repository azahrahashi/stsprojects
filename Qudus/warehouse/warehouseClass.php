<?php 

require_once '../Connection/dbconection.php';
          // Get All Products Function
function getstAllwarehouse(){

  $stmt=getCnx()->query("SELECT * FROM warehouse 
    WHERE FinishDate IS NULL");

  return $stmt;
}

                 // Add Function
function addWarehouse($WarehouseName,$WarehouseAddress)
{
  $stmt= getCnx()->prepare("INSERT INTO warehouse (WarehouseName,WarehouseAddress)
    VALUES ( :WarehouseName, :WarehouseAddress)");

  $stmt->bindParam(':WarehouseName', $WarehouseName);
  $stmt->bindParam(':WarehouseAddress', $WarehouseAddress);
  $stmt->execute();

}

             // Update Funtion
function UpdateWarehouse($WarehouseId,$WarehouseName,$WarehouseAddress)
{
  $stmt =getCnx()->prepare(" UPDATE warehouse SET  WarehouseName=:WarehouseName, WarehouseAddress=:WarehouseAddress WHERE  WarehouseId = :WarehouseId");

  $stmt->bindParam(':WarehouseId', $WarehouseId);
  $stmt->bindParam(':WarehouseName', $WarehouseName);
  $stmt->bindParam(':WarehouseAddress', $WarehouseAddress);
  $stmt->execute();
}

function terminateWarehouse($WarehouseId)
{
  $stmt =getCnx()->prepare("UPDATE warehouse SET  FinishDate=NOW() WHERE  WarehouseId = :WarehouseId");

  $stmt->bindParam(':WarehouseId', $WarehouseId);
  $stmt->execute();
}




?>