<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>


					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Material</h4>
														<span>This Is The <code>Material</code> Managment Page.</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">

											<div>
												<button class="btn btn-success btn-round waves-effect md-trigger float-right" data-toggle="modal" data-target="#default-Modal"><i class="icofont icofont-plus-alt"> </i> Add New</button>
											</div>
											<br><br>
											<br>
											<div class="dt-responsive table-responsive">
												<table id="lists" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
				                    <th>No</th>
				                    <th>Material Name</th>
				                    <th>Material Unit</th>
				                    <th>Price</th>
				                    <th>Stock Level</th>
				                    <th>Start Date</th>
				                    <th width="5%">Action</th>
				                    </tr>
													</thead>
													<tbody>
														<?php
			                           // require_once '../common/commonValues.php';
			                           require_once'materialClass.php';
			                           // $i=0;
			                            $result = getMaterialList();
			                               if (!empty($result)){
			                               while ($row = $result->fetch())
			                            {
			                             // $i++;
			                              $Id = $row['Id'];

			                             echo '
			                         <tr>

			                           <td>'. $row['Id'] .' </td>
			                           <td>'. $row['Name'] .' </td>
			                           <td>'. $row['Unit'] .' </td>
			                           <td>'. $row['UnitPrice'] .' </td>
			                           <td>'. $row['StockLevel'] .' </td>
			                           <td>'.date("d/M/Y", strtotime($row['StartDate'])).' </td>
			                           <td >

			                                           <a href="#"class="btn btn-warning btn-icon" onclick="getMaterialDetail('.$Id.')"><i class="icofont icofont-pencil"></i></a>
			                             <a href="#" class="btn btn-danger btn-icon" onclick="RemoveMaterial('.$Id.')"><i class="icofont icofont-bin"></i></a>
			                                        </td>
			                         </tr>';
			                            }}
			                         ?>
													</tbody>
												</table>
											</div>

											<!-- Add Material Model -->

											<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
					              <div class="modal-dialog" role="document">
					                <div class="modal-content">
					                  <div class="modal-header">
					                    <h4 class="modal-title">Material Register</h4>
					                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					                      <span aria-hidden="true">&times;</span>
					                    </button>
					                  </div>

					                  <div class="modal-body">
					                    <span style="color: red;" Id="error">Fadlan Buuxi meelaha banaan !!!</span>
					                    <div class="form-group row">

					                      <div class="col-md-6">
					                        <label class="col-form-label" for="inputDefault">Material Name</label>
					                        <input type="hidden" class="form-control" id="Id">
					                        <input type="text" class="form-control" id="mName">
					                      </div>

					                      <div class="col-md-6">
					                        <label class="col-form-label" for="inputDefault">Material Unit</label>
					                        <input class="form-control" id="mUnit" type="text" placeholder="Unit">
					                      </div>

					                      <div class="col-md-6">
					                        <label class="col-form-label" for="inputDefault">Price</label>
					                        <input class="form-control" id="mUnitPrice" type="text" placeholder="Price">
					                      </div>

					                      <div class="col-md-6">
					                        <label class="col-form-label" for="inputDefault">Stock Level</label>
					                        <input class="form-control" id="mStockLevel" type="text" placeholder="StockLevel">
					                      </div>

					                      <div class="col-md-6">
					                        <label class="col-form-label" for="inputDefault">Start Date</label>
					                        <input type="Date" class="form-control" id="mstartDate">
					                      </div>


					                    </div>

					                  </div>



					                  <div class="modal-footer">
					                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
					                    <button type="button" class="btn btn-primary waves-effect waves-light " onclick="savematerial();">Save changes</button>
					                  </div>
					                </div>
					              </div>
					            </div>



										</div>
									</div>

								</div>
							</div>
						</div>
					</div>


				</div>
				<!--  -->

				<?php require_once '../Include/script.php'; ?>
			</body>
			</html><script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
			 <?php require_once 'materialJs.php'; ?>
			 <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
			 <script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
			 <script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
			 <script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
			 <script type="text/javascript">
			 $(document).ready(function() {
			 	$('#lists').DataTable();
			 });
			 </script>
