<?php 

require_once '../Connection/dbconection.php';
          // Get All Types Funtion
    function getstages(){

        $stmt=getCnx()->query("SELECT st.Name as StageType,s.* FROM stages s 
        INNER JOIN stagetypes st ON s.StageTypeId=st.Id
          WHERE s.FinishDate IS NULL AND st.FinishDate IS NULL");

                return $stmt;
      }
               // Get Types Funtion
     function getstagestype(){

        $stmt=getCnx()->query(" SELECT * FROM stageTypes WHERE FinishDate IS NULL");

                return $stmt;
      }

                 // Add Function
    function addstages($Name,$StageType,$StartDate)
      {
        $stmt= getCnx()->prepare("INSERT INTO stages(StageTypeId,Name,StartDate)
              VALUES (:StageType, :Name, :StartDate)");

              $stmt->bindParam(':Name', $Name);
              $stmt->bindParam(':StageType', $StageType);
              $stmt->bindParam(':StartDate', $StartDate);


              $stmt->execute();

           // return getCnx()->lastInsertId();
      }

             // Update Funtion
   function updateStages($Id,$Name,$StageType,$StartDate)
      {
        $stmt =getCnx()->prepare("UPDATE stages SET  Name=:Name, StageTypeId=:StageType,StartDate=:StartDate WHERE  Id = :Id");

      	  $stmt->bindParam(':Id', $Id);
      	  $stmt->bindParam(':Name', $Name);
      	  $stmt->bindParam(':StageType', $StageType);
          $stmt->bindParam(':StartDate', $StartDate);
      	  $stmt->execute();
      }
         
      function terminateStages($Id)
      {
        $stmt =getCnx()->prepare("UPDATE stages SET  FinishDate=NOW() WHERE  Id = :Id");

          $stmt->bindParam(':Id', $Id);
          $stmt->execute();
      }




 ?>