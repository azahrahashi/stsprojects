
<?php 
// session_start();
//unset($_SESSION['shopping_cart']);
?>
<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Salles</h4>
														<span>This Is The <code>Salles</code> Managment Page.</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">

													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
										</div>

										<div class="card-block">
											<div>
												<a href="newSales.php"><button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#"><i class="fa fa-plus"></i>AddSelles</button></a>
											</div>
											<div class="dt-responsive table-responsive">
												<table id="lists" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>NO</th>
															<th>Customer</th>
															<th>Total Amount</th>
															<th>Sub Total</th>
															<th>Descount</th>
															<th>Payment Method</th>
															<!-- <th>Date</th> -->
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<tbody>
													<?php
													require_once 'SellesClass.php';
													$result=getSelles();
													$i=0;
													while ($row=$result->fetch()) {
														$i++;
														echo '
														<tr>
														<td>#INV'.$i.'</td>
														<td>'.$row['Name'].'</td>
														<td>$'.$row['TotalAmount'].'</td>
														<td>$'.$row['Amount'].'</td>
														<td>$'.$row['Descount'].'</td>
														<td>'.$row['PaymentMethod'].'</td>
														<td>
														<a class="btn btn-success"
														href="#" onClick="gotoUpdate('.$row['SellsID'].')"> Update</a>
														 <button class="btn btn-info" onClick="seeDetail('.$row['SellsID'].')" ><i class="fa fa-bin">See Details</button>
															   <button class="btn btn-warning" onClick="Remove('.$row['SellsID'].')" ><i class="fa fa-bin">Remove</button>
														</td>
														</tr>';
													}
													?>
												</tbody>
											</table>
										</div>

										<!-- Detail Model -->
										 <?php require_once 'sellesDetailsModels.php' ?>
										<!-- End Detail Model -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once '../Include/script.php'; ?>
</body>
</html>
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script type="text/javascript" src="../files/assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="../files/assets/pages/j-pro/js/jquery-cloneya.min.js"></script>
<script type="text/javascript">

									$(document).ready( function () {
										// $('#Purchases').DataTable();

										// $('#fondationErr').hide();

									});
									function seeDetail(SellsID) {

						            $.post("getSellesDetails.php", {
						                SellsID: SellsID,
						            },function (data, status) {
						                  // alert(data)
						                 $('#Showtheresult').html(data);
						                $("#SellesDetials").modal("show");
						            });
								}

                                   function Remove(SellsID) {

						            $.post("removeSelles.php", {
						                SellsID: SellsID,
						            },function (data, status) {
										window.location.replace('sellesList.php');
						            });
								}

								function gotoUpdate(SellsID) {
									localStorage.setItem('SellsID',SellsID);
									location.replace('updateSales.php');
								}

                            </script>



								<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
								<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
								<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
								<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
								<script type="text/javascript">
								$(document).ready(function() {
									$('#lists').DataTable();
								});
								</script>
