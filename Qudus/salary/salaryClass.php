<?php 

require_once '../Connection/dbconection.php';
          // Get All salary Function

function getAllsalary(){

  $stmt=getCnx()->query("SELECT * FROM salary 
    WHERE FinishDate IS NULL");

  return $stmt;
}

                 // Add Function
function addsalary($Amount,$SalaryType,$Paidby,$receivedby,$Date)
{
  $stmt= getCnx()->prepare("INSERT INTO salary(Amount,SalaryType,Paidby,receivedby,Date)
    VALUES ( :Amount, :SalaryType, :Paidby, :receivedby, :Date)");

  $stmt->bindParam(':Amount', $Amount);
  $stmt->bindParam(':SalaryType', $SalaryType);
  $stmt->bindParam(':Paidby', $Paidby);
  $stmt->bindParam(':receivedby', $receivedby);
  $stmt->bindParam(':Date', $Date);

  $stmt->execute();

}

             // Update Funtion
function updatesalary($salaryId,$Amount,$SalaryType,$Paidby,$receivedby,$Date)
{

  $stmt =getCnx()->prepare(" UPDATE salary SET  Amount=:Amount, SalaryType=:SalaryType, Paidby=:Paidby, receivedby=:receivedby, Date=:Date WHERE  salaryId = :salaryId");

  $stmt->bindParam(':salaryId', $salaryId);
  $stmt->bindParam(':Amount', $Amount);
  $stmt->bindParam(':SalaryType', $SalaryType);
  $stmt->bindParam(':Paidby', $Paidby);
  $stmt->bindParam(':receivedby', $receivedby);
  $stmt->bindParam(':Date', $Date);
  $stmt->execute();
}

function terminatesalary($salaryId)
{
  $stmt =getCnx()->prepare("UPDATE salary SET  FinishDate=NOW() WHERE  salaryId = :salaryId");

  $stmt->bindParam(':salaryId', $salaryId);
  $stmt->execute();
}




?>