<?php session_start();
 ?>
<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; 
require_once '../supplier/supplierClass.php';
require_once 'PurchasesClass.php';

$total = 0;
$items = 0;
?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<!-- <?php require_once '../Include/sidebar.php'; ?> -->

					<!-- <div class="pcoded-content"> -->
						<div class="pcoded-inner-content">

							<div class="main-body">
								<div class="page-wrapper">

									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Clone Elements</h4>
														<span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">
														<li class="breadcrumb-item">
															<a href="index.html"> <i class="feather icon-home"></i> </a>
														</li>
														<li class="breadcrumb-item"><a href="#!">Ready To Use</a>
														</li>
														<li class="breadcrumb-item"><a href="#!">Clone Elements</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!-- halkaan waxaa ka bilaawanaayo tableka purchaseka  -->
										<div class="col-xl-4 col-md-12">
											<div class="card">
												<div class="card-header">
													<div class="row">

														<div class="col-sm-12 col-lg-6">
															<div class="input-group">
																<select class="custom-select col-12" name="pSupplier" id="supplier">
																	<option selected="" value="0" disabled="">Select Supplier</option>
																	<?php 
																	$res=getstAllsuppliers();
																	while ($row=$res->fetch()) {
																		echo '
																		<option value="'.$row['SupplierId'].'"> '.$row['CompanyName'].' </option>
																		';
																	}
																	?>
																</select>
															</div>
																<span class="text-danger" id="spErr"></span>
														</div>
														<div class="col-sm-12 col-lg-6">
															<div class="input-group">
																<input class="form-control input-sm" type="date" name="" id="date" value="<?php echo date('Y-m-d'); ?>">
															</div>
														</div>
													</div>


													<div class="row">

														<div class="col-sm-12">
															<label class="col-sm-6 col-form-label">Location</label>
															<div class="col-sm-12">
																<select class="custom-select col-12" name="location" id="location">
																	<option selected="" value="0" disabled="">Select Location</option>
																	<option value="1"> Olow Tower</option><option value="2"> Crown house</option>          </select>
																</div>
																<span class="text-danger" id="lErr"></span>
															</div>
														</div>
														<div class="row">														<div class="col-sm-12">
															<label class="col-sm-6 col-form-label">Status</label>
															<div class="col-sm-12">
																<select class="custom-select col-12" name="status" id="status">
																	<option selected="" value="0" disabled="">Select Status</option>
																	<option value="1">Ordered</option>
																	<option value="2">Recived</option>
																	<option value="3">Pending</option>

																</select>
															</div>
																<span class="text-danger" id="stErr"></span>
														</div>
													</div>
												</div>												
												<div class="card-block">
													<div class="table-responsive" id="cart" style="height: 133px; min-height: 278px;">
														<table class="table table-bordered">
															<tr>
																<th width="40%">Product Name</th>

																<th width="10%">Quantity</th>
																<th width="20%">Price</th>
																<th  width="15%">Total</th>
																<th width="5%"><i class="fa fa-trash-o"></i></th>
															</tr>
															<tbody>
																<?php
																if (!empty($_SESSION['shopping_cart']))
																{

																	foreach ($_SESSION['shopping_cart'] as $keys => $values) {
																		echo '
																		<tr>
																		<td>'.$values["product_name"].'</td>
																		<td align="right"><input id="prQty'.$values["product_id"].'" onblur="setProduct('.$values["product_id"].',1);" value="'.$values["product_qty"].'"></td>
																		<td align="right">$ '.$values["product_price"].'</td>
																		<td>$ '.number_format($values["product_qty"] * $values["product_price"],2).'</td>
																		<td><button name="delete" class="delete" id="'.$values["product_id"].'"><i class="fa fa-times tip pointer posdel"></i></button></td>
																		</tr>
																		';
																		$total = $total + ($values["product_qty"] * $values["product_price"]);
																		$items = $items +$values["product_qty"];
																	}
																	echo '
																	<tr>
																	<td colspan="3" align="right">Total</td>
																	<td id="subTotal" align="right">$ '.number_format($total,2).'</td>
																	<td></td>
																	</tr>
																	';
																}

																?>
															</tbody>
														</table>
													</div>
													<div class="card user-activity-card">

														<table id="totalTable" style="width:100%; float:right; padding:5px; color:#000; background: #FFF;">
															<tbody><tr>
																<td style="padding: 5px 10px;border-top: 1px solid #DDD;">Items</td>
																<td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;border-top: 1px solid #DDD;">
																	<span id="titems"><?php echo $items;?></span>
																</td>
															
															</tr>
															<tr>
															<td style="padding: 5px 10px;">Discount <a href="#" id="ppdiscount" tabindex="-1">
																<!-- <i class="fa fa-edit"></i> -->
															</a>
														</td>
														<td class="text-right" style="padding: 5px 10px;font-weight:bold;">
															<span id="tds"><input type="number" id="disc" name="disc" value="0.00" align="right" onblur="Discount(<?php echo $total;?>)";></span>
														</td>
													</tr>
													<tr>
														<td style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" colspan="2">
															Total Payable <a href="#" id="pshipping" tabindex="-1">
																<i class="fa fa-plus-square"></i>
															</a>
															<span id="tship"></span>
														</td>
														<td class="text-right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" colspan="2">
															<span id="gtotal"><?php echo "$ ".$total;?></span>
														</td>
													</tr>
												</tbody></table>
											</div>
										</div>
										<div> 
											<input type="button" name="" id="save" value="Submit" class="btn btn-success float-right" onclick="savePurchase()">
										</div>
									</div>
								</div><!-- /end of purchase table -->

								<!-- halkaan waxaa ka bilaawan doono payments disply area -->



										<!-- <div class="col-xl-8 col-md-12">
											<p>hhhhhhhhhh</p>
										</div> -->

										<div class="col-xl-8 col-md-12">
											<div class="card">
												<div class="card-header">
													<h4>Materials</h4>
												</div>
												<div class="card-block">
													<div class="row">
														<?php 
														$result=getMaterials();
														while ($Materialrow=$result->fetch()) {
															echo '
															<div style="height: 205px; min-height: 515px;">
															<div class="col-xl-4 col-lg-3 col-sm-3 col-xs-12">
															<button data-lightbox="example-set" type="button" class="btn btn-warning" data-container="body" tabindex="-1" onclick="setProduct('.$Materialrow['Id'].',0);">
															<span>'.$Materialrow['Name'].'</span>
															</button>
															<input type="hidden" name="hidden_qty" id="qty'.$Materialrow['Id'].'" value="1"/>
															<input type="hidden" name="hidden_price" id="price'.$Materialrow['Id'].'" value="'.$Materialrow['UnitPrice'].'"/>
															<input type="hidden" name="hidden_name" id="name'.$Materialrow['Id'].'" value="'.$Materialrow['Name'].'"/>
															</div>
															</div>';
														}
														?>
													</div>
												</div>
											</div>
										</div> 
										
									</div>


								</div>
							</div>
						</div>
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
		<?php require_once '../Include/script.php'; ?>
	</body>
	</html>
	<script>


		var i=actual_amount=quantity=price=0;var pId;
		var totalAmount=subtotal=0;
		var discount=0;
		var items=postItems=[];

		

		function setProduct(e,flag) 
		{
			var product_id = e;
			var product_name = $('#name'+product_id).val();
			var product_price = $('#price'+product_id).val();
			var product_qty;
				if (flag==0){
					product_qty=$('#qty'+product_id).val();
				}
				if(flag==1){
					product_qty=$('#prQty'+product_id).val();
				}
			// var product_qty = $('#qty'+product_id).val();
			var action = "add";

			if (product_qty > 0) 
			{
				$.ajax({
					url:"action.php",
					method:"POST",
					data:{
						flag:flag,
						product_id:product_id,
						product_name:product_name,
						product_price:product_price,
						product_qty:product_qty,
						action:action
					},
					success:function(data)
					{
					//alert(data);
						var parsedData= JSON.parse(data);
					$('#cart').html(parsedData.order_table);
					 items=parsedData.shop;
			// console.log(items);
					// alert("Product has been Added into Cart");
					// window.location.reload();
					$('#gtotal').text(parsedData.total);
					$('#titems').text(parsedData.cart_item);
					// $items=parsedData.cart_item;
				}
			});
			}
			else
			{
				alert("Error");
			}

		}

		function savePurchase() {
			var supplier = $('#supplier').val();
			var date = $('#date').val();
			var location = $('#location').val();
			var status =$('#status').val();
			subtotal = parseFloat($('#gtotal').text());
			// alert(subtotal)
			if (discount==0) 
			{
				totalAmount=subtotal;
			}
			 if (supplier==null) 
			{
				// alert('Please select Supplier');
				$('#spErr').text('Please select Supplier');
			}
			 if (location==null) 
			{
				$('#lErr').text('Please select Location');
			}

			 if (status==null) 
			{
				$('#stErr').text('Please select Status');
			}
			else{
			//items = <?php if (!empty($_SESSION['shopping_cart'])) { echo json_encode($_SESSION['shopping_cart']); }else { echo 0;}?>;
			console.log(items);
			// if (items.length==0) {
				
			// 		items = <?php if (!empty($_SESSION['shopping_cart'])) { echo json_encode($_SESSION['shopping_cart']); }else { echo 0;}?>;
				
			// }
			// alert(items.length);
			for (var i = items.length - 1; i >= 0; i--) {
				
				var info = items[i].product_id + "," + items[i].product_price + "," + items[i].product_qty;
				postItems.push(info);
			}
			// alert(items[0].product_id);
			
			// console.log(subtotal,totalAmount,parseFloat(discount));
			// console.log(supplier);
			// console.log(date);
			// console.log(location);
			console.log(postItems);
			$.ajax({
				url:'insertPurchase.php',
				method:'POST',
				data:{
					items:postItems,
					supplier:supplier,
					location:location,
					status:status,
					date:date,
					subtotal:subtotal,
					totalAmount:totalAmount,
					discount:discount
				},
				success:function(data) {
					console.log(data);
					// alert(data);
				 window.location.replace('purchaseList.php');
				}
			});
		}

		}
		function Discount(total){
			subtotal=parseFloat($('#gtotal').text());
			discount = $('#disc').val();

			totalAmount = subtotal - discount;

			$('#gtotal').text(totalAmount);
		}

		function AmountPaid(){
			var totalAmount  = $('#totalAmount').val();

			var Paid = $('#Paid').val();

			var Blance = totalAmount - Paid;
			$('#Blance').val(Blance+".00");
		}
		$(document).on('click','.delete',function(){
				var product_id = $(this).attr('id');
				var act = "remove";
				if (confirm("Are you sure you want to remove this product?")) 
				{
					$.ajax({
						url:"action.php",
						method:"POST",
						data:{product_id:product_id, action:act},
						success:function(data){
						 // alert(data);
						var parsedData= JSON.parse(data);
					    $('#cart').html(parsedData.order_table);

					$('#gtotal').text(parsedData.total);
					$('#titems').text(parsedData.cart_item);
					// window.location.reload();
						}
					})
				}
				else
				{
					return false;
				}
		});
		// $(document).ready(function() {
		// 	if ((localStorage.getItem('purchaseId'))!==null) {
		// 		//get purchhase detail
		// 		alert(localStorage.getItem('purchaseId'));
		// 		localStorage.clear();
		// 	}
		// });
	</script>
	<style>
	input {
		border:0px;
		/*border-bottom:2px solid #999;*/
	}
</style>