<?php
       require_once'../common/commonClass.php';
       require_once'../common/commonValues.php';
    isLoggedIn();
  ?>
<nav class="navbar header-navbar pcoded-header">
	<div class="navbar-wrapper">
		<div class="navbar-logo">
			<a class="mobile-menu" id="mobile-collapse" href="#!">
				<i class="feather icon-menu"></i>
			</a>
			<a href="../Reports/dashboard">
				<img class="img-fluid" src="../files/assets/images/logo.jpg" alt="Theme-Logo" width="50px" height="1px" />
			</a>
			<a class="mobile-options">
				<i class="feather icon-more-horizontal"></i>
			</a>
		</div>
		<div class="navbar-container container-fluid">
			<ul class="nav-left">
				<li>
					<a href="#!" onclick="javascript:toggleFullScreen()">
						<i class="feather icon-maximize full-screen"></i>
					</a>
				</li>
			</ul>
			<ul class="nav-right">
				<li class="header-notification">
							<button class="btn btn-info" >
					
								<a href="../Reports/dashboard.php" style="color: white"><span class="pcoded-micon"><i class="feather icon-home"></i></span></a>
							</button>

				</li>
				<li class="header-notification">
							<button class="btn btn-danger" >
								<a href="../Reports/OutOfstockList.php" style="color: white">out Stocks</a>
							</button>

				</li>
				<li class="user-profile header-notification">
					<div class="dropdown-primary dropdown">
						<div class="dropdown-toggle" data-toggle="dropdown">
							<img src="../employee/employeeImage/<?php echo getCurrentUserImageByUserName($_SESSION[getSessionName()]);?>" class="img-radius" alt="User-Profile-Image">
							<span><?php echo getCurrentUserNameByUserName($_SESSION[getSessionName()]);?></span>
							<i class="feather icon-chevron-down"></i>
						</div>
						<ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
							<li>
								<a href="../login/changePassword">
									<i class="feather icon-settings"></i> Change Password
								</a>
							</li>
							<li>
								<a href="user-profile.html">
									<i class="feather icon-user"></i> Profile
								</a>
							</li>
							<li>
								<a href="../login/logout.php">
									<i class="feather icon-log-out"></i> Logout
								</a>
							</li>
						</ul>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>
