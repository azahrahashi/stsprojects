<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Basic Form Inputs</h4>
														<span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											<div>
												<!-- <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#Aproductstore"><i class="fa fa-plus"></i> Add</button> -->
											</div>
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Product Name</th>
															<th>Unit</th>
															<th>Qty</th>
															<th>StockLevel</th>
															<th>StartDate</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<?php 
													require_once 'productstoreClass.php';
													$result=GetAllProductStore();
													while ($row=$result->fetch()) {

														echo '
														<tbody>
														<tr>
														<td>'.$row['Id'].'</td>
														<td>'.$row['Name'].'</td>
														<td>'.$row['Unit'].'</td>
														<td>'.$row['Qty'].'</td>
														<td>'.$row['StockLevel'].'</td>
														<td>'.$row['StartDate'].'</td>
														<td>';?>
														<button class="btn btn-success" 
														onClick="setproductstore(<?php echo $row['Id'];?>,<?php echo $row['ProductId'];?>,'<?php echo $row['Name'];?>','<?php echo $row['Unit'];?>',<?php echo $row['Qty'];?>,<?php echo $row['StockLevel'];?>,'<?php echo $row['StartDate'];?>')"> Update </button>
														<?php 
														echo '<button class="btn btn-warning" onClick="RemoveStageType('.$row['Id'].')" ><i class="fa fa-trush">Remove</button></td>
														</tr>';
													}
													?>
												</tbody>
											</table>
										</div>

										<!-- Add Stagetype Model -->
							<div class="modal fade" id="Aproductstore" tabindex="-1" role="dialog">
								<div class="modal-dialog" role="document">
									<form method="post" id="user_form" enctype="multipart/form-data">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title">Add Productstore </h4>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="form-group row">
													<div class="col-sm-8">
														<input type="hidden" id="Id" name="Id" value=""  class="form-control">
														<select class="form-control" id="aProductName">
														<option value="0"> Select product name</option>
															<?php
																$result = GetProductStore();
																if (!empty($result))
																{
																	
																	while ($row=$result->fetch()) {
																		echo '<option value ="' . $row["Id"] . '">' . $row["Name"]. '</option>';
																	}
																}
																?>		
														</select>
													</div><br>
													<div class="col-sm-4">
														<input type="text" id="Unit" name="Unit" class="form-control" placeholder="Unit">
													</div>
													<br>
												</div>
												<div class="form-group row">
													<div class="col-sm-4">
														<input type="text" id="Qty" name="Qty" class="form-control" placeholder="QTY">
													</div><br>
													<div class="col-sm-8">
														<input type="text" id="Stlev" name="Stlev" class="form-control" placeholder="Stake Level">
													</div>
													<br>
												</div>
												<div class="form-group row">
													<div class="col-sm-12">
														<input type="date" id="StartDate" name="StartDate" class="form-control">
													</div>
												</div>
											</div>
											<div class="modal-footer">
													<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary waves-effect waves-light" onclick="addproductstore()"> Add </button>
											</div>
										</div>
									</div>
								</form>
							</div> 
						</div>

										<!-- End Add Stagetype Model -->


										<!-- Update Model -->

							<div class="modal fade" id="uproductstore" tabindex="-1" role="dialog">
								<div class="modal-dialog" role="document">
									<form method="post" id="user_form" enctype="multipart/form-data">
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title">Update Productstore </h4>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="form-group row">
													<div class="col-sm-8">
														<input type="hidden" id="Id" name="Id" value=""  class="form-control">
														<select class="form-control" id="uProductName">
															<?php
																$result = GetProductStore();
																if (!empty($result))
																{
																	echo "<option id='producstoreSelect'></option>";
																	while ($row=$result->fetch()) {
																		echo '<option value ="' . $row["Id"] . '">' . $row["Name"]. '</option>';
																	}
																}
																?>		
														</select>
													</div><br>
													<div class="col-sm-4">
														<input type="text" id="uUnit" name="uUnit" class="form-control" placeholder="Unit">
													</div>
													<br>
												</div>
												<div class="form-group row">
													<div class="col-sm-4">
														<input type="text" id="uQty" name="uQty" class="form-control" placeholder="QTY">
													</div><br>
													<div class="col-sm-8">
														<input type="text" id="uStlev" name="uStlev" class="form-control" placeholder="Stake Level">
													</div>
													<br>
												</div>
												<div class="form-group row">
													<div class="col-sm-12">
														<input type="date" id="uStartDate" name="uStartDate" class="form-control">
													</div>
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
												<button type="button" class="btn btn-primary waves-effect waves-light " onclick="updateproductstore()">update</button>
											</div>
										</div>
									</div>
								</form>
							</div> 
						</div>
						<!-- End Update Model -->


								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once '../Include/script.php'; ?>
</body>
</html>

          <script type="text/javascript">

						  // function addproductstore() {

							// 	var Name = $("#aProductName").val();
							// 	var Unit = $("#Unit").val();
							// 	var Qty = $("#Qty").val();
							// 	var Stlev = $("#Stlev").val();
							// 	var StartDate = $("#StartDate").val();
							// 	 // alert(+Name+','+Unit+','+Qty','+Stlev','+StartDate);

							// 	$.post("addproductstore.php", {
							// 		Name: Name,
							// 		Unit:Unit,
							// 		Q:ty:Qty,
							// 		Stlev:Stlev,
							// 		StartDate: StartDate,
							// 		Id:0
							// 	},
							// 	function (data, status) {
							// 		alert("Successfully Registered");
							// 		window.location.reload();
							// 	});

							// }

							function setproductstore(Id,ProductId,Name,Unit,Qty,StockLevel,StartDate){

								    // alert(Id+','+Name','+Unit','+Qty','+StockLevel','+StartDate);
								   $("#Id").val(Id);
								   $("#uProductName").val(ProductId);
								   $("#uUnit").val(Unit);
								   $("#uQty").val(Qty);
								   $("#uStlev").val(StockLevel);
								   $("#uStartDate").val(StartDate);
								  $("#producstoreSelect").append('<option value="'+ProductId+'">'
								  	+Name+'</option>');
								   $("#uproductstore").modal("show");

							 	}

								function updateproductstore(){

									var Id=$("#Id").val();
									var Name=$("#uProductName").val();
									var Unit=$("#uUnit").val();
									var Qty=$("#uQty").val();
									var StockLevel=$("#uStlev").val();
									var StartDate=$("#uStartDate").val();

							     $.post("addproductstore.php",{
							    	Name:Name,
							    	Unit:Unit,
							    	Qty:Qty,
							    	StockLevel:StockLevel,
							    	StartDate:StartDate,
							    	Id:Id
							    }, function (data, status) {
							    	// alert(data);
							    	alert("Successfully Updated");
							    	window.location.reload();
							    });

							}

							function RemoveStageType(Id) {

								$.post("RemoveStageType.php",{
									Id:Id
								},
								function (data, status) {
									alert("Successfully Removed");
									window.location.reload();
								});

							}
							



						</script>