<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<style>
body {font-family: arial, Arial, Helvetica, sans-serif; font-size: 12px}
body {
        height: 297mm;/*297*/
        width: 210mm;
    }
dt { float: left; clear: left; text-align: right; font-weight: bold; margin-right: 10px } 
dd {  padding: 0 0 0.5em 0; }

page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}
</style> 
<style type="text/css" media="print">
@page 
    {
        size: auto;   /* auto is the initial value */
        margin: 0mm;  /* this affects the margin in the printer settings */
    }
</style>   
</head>

<body>
    <page size="A4">
<?php
    require_once '../Classes/stageClass.php';
    //send projectId
    if (isset($_GET['pId'])) 
        $ProjectId=$_GET['pId'];                                                            
    $result = getSamrateanalys($ProjectId);
    if (!empty($result)){

        while ($row=$result->fetch())  {$id =$row['Id']; ?>


<br /><br />

<table cellspacing='3' cellpadding='3' WIDTH='100%'>
<tr>
<td valign='top' WIDTH='33%'><img src="logo.png" alt="" class="logo" width="300" height='143' /></td>
<td valign='top' WIDTH='66%'>
    <h1 style="text-align: center;">Qududs Constrection Company <br> Shirkada Dhismaha Qudus</h1>
</td>
<td valign='top' WIDTH='0%'>
</td>
</tr>
</table>

<br /><br />

<table cellspacing='3' cellpadding='3' WIDTH='100%'>
<tr>
<td valign='top' WIDTH='33%'></td>
<td valign='top' WIDTH='33%'><h3>EXCAVATION</h3> </td>
<td valign='top' WIDTH='33%'></td>
</tr>
<tr>
<td valign='top' WIDTH='33%'><input type="checkbox" name="" value="RCC Columns Fondation">RCC Columns Fondation </td>
<td valign='top' WIDTH='33%'><h3></h3> </td>
<td valign='top' WIDTH='33%'></td>
</tr>
</table>

<hr />

<table cellspacing='3' cellpadding='3' WIDTH='100%'>

<tr>
<td valign='top'><strong>ITEM</strong> </td>
<td valign='top'><strong>Unit</strong> </td>
<td valign='top' align='right'><strong>Rate USD</strong> </td>
</tr>
<?php   echo "
<tr>
<td valign='top'>".$row['ItemDescription']."</td>
<td valign='top'>".$row['Unit']."</td>
<td valign='top' align='right'><span class='amount'>".$row['ReateUSD']."</span></td>
</tr>";?>
</table>

<hr />
<H2>SAMPLE RATE ANALYSIS</H2>
<table cellspacing='3' cellpadding='3' WIDTH='100%' style="height: 70%;">
    <thead>                                                   
        <tr>
        <th>Item Description</th>
        <th>Unit</th>
        <th>Quantity</th>
        <th>Rate USD</th>
        <th>Amount USD</th>
        
        </tr>
    </thead>
    <?php
    $result2 = getSamrateanalysDet($id);
        if (!empty($result2)){
            while ($row=$result2->fetch())  {
                echo '
            <tr>
                <td style="width:10%;">'.$row['Description'].'</td>
                <td> <strong>'.$row['Unit'].'</strong> </td>
                <td> <strong>'.$row['Qty'].'</strong> </td>
                <td> <strong>'.$row['ReateUSD'].'</strong> </td>    
                <td> <strong>'.$row['AmountUSD'].'</strong> </td>                                                               
            </tr>'; 
        }
    }
        }
                                                        }
                                                        ?>
</table>

<p>Thanks for buying with us, I hope the experience wasn't too painful for you.</p>
<p>Please visit us again soon and see what stuff we can offload onto you.</p>

<div style='position: absolute; width: 300px; height: 200px; bottom: 80px; left: 150px;'>
	DELIVERY ADDRESS<br /><br />1 Their Street<br />
Town<br />
County<br />
Postcode</div>

<div style='position: absolute; width: 300px; height: 200px; bottom: 80px; left: 600px;'>
	RETURN ADDRESS<br /><br />Our company<br />
Our building<br />
Town<br />
County<br />
Postcode
</div>
</page>
</body>
</html> 