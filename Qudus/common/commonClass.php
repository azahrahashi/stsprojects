<?php

require_once'../Connection/dbconection.php';
	function UpdatePlace($PlaceId,$Address,$PostCode)
	{
	$stmt =getCnx()->prepare("UPDATE place SET  Address=:Address,PostCode=:PostCode WHERE  PlaceId = :PlaceId");

	  $stmt->bindParam(':PlaceId', $PlaceId);
	  $stmt->bindParam(':Address', $Address);
	  $stmt->bindParam(':PostCode', $PostCode);
	   $stmt->execute();
	}

	function UpdateTelephone($TelephoneId,$TelephoneNo,$TelephoneType)
	{
	$stmt =getCnx()->prepare("UPDATE telephone SET  TelephoneNo=:TelephoneNo,TelephoneType=:TelephoneType WHERE  TelephoneId = :TelephoneId");

	  $stmt->bindParam(':TelephoneId', $TelephoneId);
	  $stmt->bindParam(':TelephoneNo', $TelephoneNo);
	  $stmt->bindParam(':TelephoneType', $TelephoneType);
	   $stmt->execute();

	}
	function UpdateName($name,$PersonId)
	{
	$stmt =getCnx()->prepare("UPDATE person set FirstName =:name WHERE PersonId = :PersonId");

	  $stmt->bindParam(':name', $name);
	  $stmt->bindParam(':PersonId', $PersonId);
	   $stmt->execute();

	}

function getPersonDetailsByID($PersonId)
{
  $stmt=getCnx()->prepare("SELECT  p.*, pl.PlaceId, pl.Address, pl.PostCode,pl.City,
        t.TelephoneId,t.TelephoneNo,t.TelephoneType
        FROM person p
        INNER JOIN lives l ON p.PersonId = l.PersonId
        INNER JOIN place pl ON pl.PlaceId = l.PlaceId
        INNER JOIN uses u ON p.PersonId = u.PersonId
        INNER JOIN telephone t ON t.TelephoneId=u.TelephoneId
        WHERE p.FinishDate is NULL AND l.FinishDate is NULL AND
        p.PersonId = :PersonId");

          $stmt->bindParam(':PersonId', $PersonId);
          $stmt->execute();
          return $stmt;
}

function getOwnerDetailsByVehicleId($vId)
{
  $stmt=getCnx()->prepare("SELECT  p.*,t.TelephoneNo
        FROM person p
        INNER JOIN uses u ON p.PersonId = u.PersonId
        INNER JOIN telephone t ON t.TelephoneId=u.TelephoneId
        INNER JOIN ownership o ON p.PersonId = o.PersonId
        WHERE p.FinishDate is NULL 
        AND o.FinishDate is NULL 
        AND o.VehicleId = :vId");

          $stmt->bindParam(':vId', $vId);
          $stmt->execute();
          return $stmt;
}

function getPersonListByProject($ProjectId)
{
  $stmt=getCnx()->prepare("SELECT p.*, pl.PlaceId, pl.Address, pl.PostCode,pl.City,
    tel.TelephoneId,tel.TelephoneNo,tel.TelephoneType,
        tel.TelephoneId,tel.TelephoneNo,tel.TelephoneType,d.ProjectId,d.Amount,
        d.CreatedBy,d.CreatedDate,d.StartDate
        FROM person p
        INNER JOIN lives l ON p.PersonId = l.PersonId
        INNER JOIN place pl ON pl.PlaceId = l.PlaceId
        INNER JOIN uses u ON p.PersonId = u.PersonId
        INNER JOIN telephone tel ON tel.TelephoneId=u.TelephoneId
        INNER JOIN donates d ON p.PersonId = d.PersonId
        WHERE p.FinishDate is NULL AND l.FinishDate is NULL AND
        d.ProjectId =:ProjectId");
        $stmt->bindParam(':ProjectId', $ProjectId);
        $stmt->execute();
        return $stmt;

}

  // List of Role
function selectRole()
{ 
    $result = getAuthorityRoleList();
      if (!empty($result))
          {
                  while ($row=$result->fetch()) {
                   echo '<option value ="' . $row["AuthorityRoleId"] . '">' . $row["Role"] . '</option>';
                }
            }

}//Dib ayaa looga daray

function addAuthorised($RoleId,$CreatedBy,$fName, $lName){
  $userName = generateUserName($fName, $lName);
  $stmt=getCnx()->prepare(
        "INSERT INTO authorised(UserName, Password,CreatedBy,CreatedDate,Enabled)
        VALUES (:UserName, :Password, :CreatedBy,NOW(),:Enabled)");

        $stmt->bindParam(':CreatedBy', $CreatedBy);
        $stmt->bindValue(':Enabled', "1");
        $stmt->bindParam(':UserName',$userName );
        $stmt->bindValue(':Password', "Pass");

        $stmt->execute();
     return getCnx()->lastInsertId();
}





  function userExist($userName)
  {
    $stmt = getCnx()->prepare("SELECT AuthorisedId FROM Authorised WHERE UserName = :userName");

    $stmt->bindParam(':userName', $userName);
    $stmt->execute();
    while ($row = $stmt->fetch())
    {
      return true;
    }
    return false;
  }

  function generateUserName($fName, $lName){
    $n = 101;
    $fName = strtolower($fName);
    $lName = strtolower($lName);

    
    $userName = $fName{0}.''.$lName{0}.''.$n;
    
  
    while (userExist($userName))
    {
      $n++;
      $userName = $fName{0}.''.$lName{0}.''.$n;
    }

    return $userName;

  }

function getEmploymentList()
{
  return  getCnx()->query("SELECT * FROM employment e
INNER JOIN person p ON p.PersonId=e.PersonId
WHERE e.FinishDate is NULL");

}   
function getStaffDetails($PersonId)
{
  

  $stmt=getCnx()->prepare("SELECT  p.*, pl.PlaceId, pl.Address, pl.PostCode,pl.City,e.EmploymentId,e.StartDate as sDate,
        t.TelephoneId,t.TelephoneNo,t.TelephoneType,am.AuthorisedId,am.AuthorityRoleId,am.StartDate,a.Enabled,
        a.Password,ar.Role,a.UserName

        FROM person p
        INNER JOIN lives l ON p.PersonId = l.PersonId
        INNER JOIN place pl ON pl.PlaceId = l.PlaceId
        INNER JOIN uses u ON p.PersonId = u.PersonId
        INNER JOIN telephone t ON t.TelephoneId=u.TelephoneId
        INNER JOIN authorisedmembership am ON am.PersonId=p.PersonId
        INNER JOIN authorised a ON a.AuthorisedId=am.AuthorisedId
        INNER JOIN authorityrole ar ON ar.AuthorityRoleId=am.AuthorityRoleId
        INNER JOIN employment e ON e.PersonId = p.PersonId
        WHERE p.FinishDate is NULL AND e.FinishDate is NULL AND am.FinishDate is NULL AND
        p.PersonId = :PersonId");

          $stmt->bindParam(':PersonId', $PersonId);
          $stmt->execute();
          return $stmt;
}
//updating role and sDate
function updateRole($AuthorityRoleId,$PersonId,$StartDate)
{
    try{
  $stmt =getCnx()->prepare("UPDATE authorisedmembership set   AuthorityRoleId =  :AuthorityRoleId,
         StartDate = :StartDate WHERE PersonId = :PersonId");
  
  $stmt->bindParam(':AuthorityRoleId', $AuthorityRoleId);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->bindParam(':PersonId', $PersonId);
  $stmt->execute();
  } catch(PDOException $e){
    echo $e;
  }
  
}
//updating role and sDate
function suspend($AuthorisedId,$Enabled)
{
    try{
  $stmt =getCnx()->prepare("UPDATE authorised set   Enabled =  :Enabled WHERE AuthorisedId = :AuthorisedId");
  
  $stmt->bindParam(':AuthorisedId', $AuthorisedId);
  $stmt->bindParam(':Enabled', $Enabled);
  $stmt->execute();
  } catch(PDOException $e){
    echo $e;
  }
  
}
 function terminateStaff($finishDate,$PersonId)
    {
      $stmt = getCnx()->prepare("UPDATE employment SET FinishDate = :finishDate WHERE PersonId = :PersonId");

      $stmt->bindParam(':finishDate', $finishDate);
      $stmt->bindParam(':PersonId', $PersonId);

      $stmt->execute();
      return $stmt;

    }

  function getUserByUserName($username, $password)
  {
    //echo "SELECT AuthorisedId FROM Authorised WHERE UserName = '$username' AND Password = '$password'";
    try {
      $stmt = getCnx()->prepare("SELECT a.AuthorisedId, ar.Role FROM authorised a
        INNER JOIN authorisedmembership am ON a.AuthorisedId = am.AuthorisedId 
        INNER JOIN authorityrole ar ON am.AuthorityRoleId = ar.AuthorityRoleId
        WHERE UserName = :username AND Password = :password");

      $stmt->bindParam(':username', $username);
      $stmt->bindParam(':password', $password);
      $stmt->execute();
      return $stmt;
    } catch (PDOException $e){
      echo $e;
    }
    // return null;
  }


  function checkUserByUserNameAndPassword($username, $password)
  {
    // echo "SELECT AuthorisedId FROM Authorised WHERE UserName = '$username' AND Password = '$password'";
    try {
      $stmt = getCnx()->prepare("SELECT AuthorisedId FROM authorised WHERE UserName = :username AND Password = :password");

      $stmt->bindParam(':username', $username);
      $stmt->bindParam(':password', $password);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        return true;
      }
    } catch (PDOException $e){
      echo $e;
    }
    return false;
  }
    function getCurrentUserImageByUserName($username)
  {

    try {
      $stmt = getCnx()->prepare("SELECT pI.ImageName FROM personimage pI
                         
                          INNER JOIN person p on p.PersonId =  pI.PersonId
                          JOIN authorisedmembership am on am.PersonId = p.PersonId
                          INNER JOIN authorised a on a.AuthorisedId = am.AuthorisedId
                           WHERE a.UserName = :username");

      $stmt->bindParam(':username', $username);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['ImageName']))
        return $row["ImageName"];
      }
      return "userImage.jpg";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }

  // Select All Images 
  //   function getCurrentUserImagesByUserName($username)
  // {
  //    $stmt=getCnx()->prepare("SELECT pI.ImageName FROM personimage pI
                         
  //                         INNER JOIN person p on p.PersonId =  pI.PersonId
  //                         JOIN authorisedmembership am on am.PersonId = p.PersonId
  //                         INNER JOIN authorised a on a.AuthorisedId = am.AuthorisedId
  //                          WHERE a.UserName = :username");

  //         $stmt->bindParam(':username', $username);
  //         $stmt->execute();
  //         return $stmt;
  // }

  function getCurrentUserNameByUserName($username)
  {
    
    try {
      $stmt = getCnx()->prepare("SELECT p.FirstName,p.LastName FROM authorised a
        INNER JOIN authorisedmembership am ON a.AuthorisedId = am.AuthorisedId 
        INNER JOIN person p ON am.PersonId = p.PersonId WHERE a.UserName = :username");

      $stmt->bindParam(':username', $username);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        $Fullname = $row["FirstName"].' '.$row["LastName"];
        return $Fullname;
      }
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }
// get user Id
   function getCurrentUserIdByUserName($username)
  {
    
    try {
      $stmt = getCnx()->prepare("SELECT p.PersonId,p.LastName FROM authorised a
        INNER JOIN authorisedmembership am ON a.AuthorisedId = am.AuthorisedId 
        INNER JOIN person p ON am.PersonId = p.PersonId WHERE a.UserName = :username");

      $stmt->bindParam(':username', $username);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        $Fullname = $row["PersonId"];
        return $Fullname;
      }
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }
// get user role and permations
    function getCurrentUserPermisionIdByUserName($username)
  {
    
    try {
      $stmt = getCnx()->prepare("SELECT ar.Role FROM authorityrole ar
                                 INNER JOIN authorisedmembership am ON am.AuthorityRoleId = ar.AuthorityRoleId
                                 INNER JOIN authorised a on a.AuthorisedId = am.AuthorisedId
                                 INNER JOIN person p ON am.PersonId = p.PersonId WHERE a.UserName = :username");

      $stmt->bindParam(':username', $username);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        $userRole = $row["Role"];
        return $userRole;
      }
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }
  // get user Email
     function getCurrentUserEmailByUserName($username)
  {
    
    try {
      $stmt = getCnx()->prepare("SELECT p.Email,p.LastName FROM authorised a
        INNER JOIN authorisedmembership am ON a.AuthorisedId = am.AuthorisedId 
        INNER JOIN person p ON am.PersonId = p.PersonId WHERE a.UserName = :username");

      $stmt->bindParam(':username', $username);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        $Fullname = $row["Email"];
        return $Fullname;
      }
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }

  // get person tele phone 
       function getCurrentUserPhoneByUserName($pId)
  {
    
    try {
      $stmt = getCnx()->prepare("SELECT T.TelephoneNo FROM uses u
        INNER JOIN telephone T ON u.TelephoneId =  T.TelephoneId
        INNER JOIN person p ON u.PersonId = p.PersonId WHERE p.PersonId = :pId");

      $stmt->bindParam(':pId', $pId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        $Fullname = $row["TelephoneNo"];
        return $Fullname;
      }
    } catch (PDOException $e){
      echo $e;
    }
    return "No Phone Number";
  }
  // get user Address
       function getCurrentUserAddressByUserName($pId)
  {
    
    try {
      $stmt = getCnx()->prepare("SELECT pl.Address FROM lives l
        INNER JOIN place pl ON l.PlaceId =  pl.PlaceId
        INNER JOIN person p ON l.PersonId = p.PersonId WHERE p.PersonId = :pId");

      $stmt->bindParam(':pId', $pId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        $Fullname = $row["Address"];
        return $Fullname;
      }
    } catch (PDOException $e){
      echo $e;
    }
    return "No Phone Number";
  }

 function updatePass($username, $password)
  {
    // echo "UPDATE authorised  set Password = '$password' WHERE UserName = '$username'";
    try {
      $stmt = getCnx()->prepare("UPDATE authorised  set Password = :password WHERE UserName = :username");

      $stmt->bindParam(':username', $username);
      $stmt->bindParam(':password', $password);
      $stmt->execute();
    }
    catch (PDOException $e){
      echo $e;
    }
}

function  addownership($personId,$VehicleId,$StartDate){
  $stmt=getCnx()->prepare("INSERT INTO ownership(PersonId,VehicleId,StartDate)
        VALUES (:personId, :VehicleId,:StartDate)");

        $stmt->bindParam(':personId', $personId);
        $stmt->bindParam(':VehicleId',$VehicleId);
        $stmt->bindParam(':StartDate', $StartDate);

        $stmt->execute();
}
function getOwnerDetailById($PersonId)
{
  $stmt=getCnx()->prepare("SELECT P.PersonId, P.FirstName, P.LastName,P.Gender,P.Email,T.TelephoneNo,V.VehicleId,V.Model,V.Plate FROM person P 
                            INNER JOIN ownership O ON O.PersonId = P.PersonId
                            INNER JOIN vehicle V ON V.VehicleId = O.VehicleId
                            INNER JOIN uses U ON U.PersonId = P.PersonId
                            INNER JOIN telephone T ON T.TelephoneId = U.TelephoneId
                            WHERE P.PersonId =:PersonId");

          $stmt->bindParam(':PersonId', $PersonId);
          $stmt->execute();
          return $stmt;
}

function getStaffDetailsByEmployeeId($employeeId)
{
  try {
    $stmt = getCnx()->prepare("SELECT p.* FROM person p
      INNER JOIN employment e ON p.PersonId = e.PersonId WHERE e.FinishDate IS NULL
      AND e.EmploymentId = :eId");

    $stmt->bindParam(':eId', $employeeId);
    $stmt->execute();
    return $stmt;
  }
  catch (PDOException $e){
    echo $e;
  }
}

?>