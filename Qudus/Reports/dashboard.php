<?php require_once '../Include/head.php';?>

<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="main-body">


							<div class="page-wrapper">


								<div class="page-body">
									<div class="row">
                         <?php require_once 'reportClass.php'; 
$date = date('Y-m-d');
$fDay=date('Y-m-01'); ?>

										<div class="col-xl-3 col-md-6">
											<a href="selesReport.php">
												<div class="card bg-c-lite-green update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white"> <?php echo '$'.getAllSallesAmount($fDay,$date);?></h4>
																<h6 class="text-white m-b-0">All Sales</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-1" height="50" style="display: block; width: 91px; height: 50px;" width="91"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div> 
											</a>
										</div>

										<div class="col-xl-3 col-md-6">
											<a href="purchaseReport.php">
												<div class="card bg-c-lite-green update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white"><?php echo '$'. AllPurchases($fDay,$date);?> </h4>
																<h6 class="text-white m-b-0">All purchases</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-1" height="50" style="display: block; width: 91px; height: 50px;" width="91"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div>
											</a>
										</div>






										<div class="col-xl-3 col-md-6">
											<a href="expensesReport.php">
												<div class="card bg-c-pink update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white" ><?php echo '$'. getAllExpensesAmount($fDay,$date); ?> </h4>
																<h6 class="text-white m-b-0">All Expenses</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-3" height="50" width="91" style="display: block; width: 91px; height: 50px;"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div>
											</a>
										</div>


										<div class="col-xl-3 col-md-6">
											<a href="customerReport.php">
												<div class="card bg-c-lite-green update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white" id="customerReport">500</h4>
																<h6 class="text-white m-b-0">All Customers</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-4" height="50" width="91" style="display: block; width: 91px; height: 50px;"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div>
											</a>
										</div>




										<div class="col-xl-3 col-md-6">
											<a href="sallaryReporList.php">
												<div class="card bg-c-lite-green update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white" ><?php echo '$'.getAllSallery($fDay,$date); ?> </h4>
																<h6 class="text-white m-b-0">All Salary</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-4" height="50" width="91" style="display: block; width: 91px; height: 50px;"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div>
											</a>
										</div>

									<!--
										* Abdinasir's report
									-->


									<div class="col-xl-3 col-md-6">
											<a href="CurentMaterialList.php">
												<div class="card bg-c-lite-green update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white" ><?php echo CurentStoreMaterial();?> </h4>
																<h6 class="text-white m-b-0">Curent Material Store</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-4" height="50" width="91" style="display: block; width: 91px; height: 50px;"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div>
											</a>
										</div>



										<div class="col-xl-3 col-md-6">
											<a href="CurentProductList.php">
												<div class="card bg-c-lite-green update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white" ><?php echo CurentProductStore();?> </h4>
																<h6 class="text-white m-b-0">Curent Product Store</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-4" height="50" width="91" style="display: block; width: 91px; height: 50px;"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div>
											</a>
										</div>

										<div class="col-xl-3 col-md-6">
											<a href="EmployeeList.php">
												<div class="card bg-c-lite-green update-card">
													<div class="card-block">
														<div class="row align-items-end">
															<div class="col-8">
																<h4 class="text-white" ><?php echo getAllEmployee();?> </h4>
																<h6 class="text-white m-b-0">All Employee</h6>
															</div>
															<div class="col-4 text-right"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; top: 0px; left: 0px; bottom: 0px; right: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
																<canvas id="update-chart-4" height="50" width="91" style="display: block; width: 91px; height: 50px;"></canvas>
															</div>
														</div>
													</div>
													<div class="card-footer">
														<!-- <p class="text-white m-b-0"><i class="feather icon-clock text-white f-14 m-r-10"></i>update : 2:15 am</p> -->
													</div>
												</div>
											</a>
										</div>
									
									<!--
										* END  Abdinasir's report
									-->


								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!--  -->

			<script type="text/javascript">
				$(document).ready(function() {
						/**********************
							Outomatic charge 
							***********************/ 
							var autocharge = "";
							$.ajax({
								url: "../employee/autocharge.php",
								method: "POST",
								data: {autocharge:autocharge},
								success: function (data) {
									
								}
							});
						//dhamaadka outo charge  

						getAllExpenses();

						function getAllExpenses() {
							var getExpenses = "";
							$.ajax({
								url: "report.php/getExpenses",
								method: "POST",
								data: {getExpenses:getExpenses},
								success: function (data) {
									$("#expennseAmount").text(data);
								}
							});
						}

						getAllCustomers();
						function getAllCustomers() {
							var getCustomers = "";
							$.ajax({
								url: "report.php/getCustomers",
								method: "POST",
								data: {getCustomers:getCustomers},
								success: function (data) {
									$("#customerReport").text(data);
								}
							});
						}


						getAllSalles();
						function getAllSalles() {
							var getSalesTotal = "";
							$.ajax({
								url: "report.php/getSelles",
								method: "POST",
								data: {getSalesTotal:getSalesTotal},
								success: function (data) {
									$("#salesID").text(data);
								}
							});
						}

						getAllSallery();
						function getAllSallery() {
							var getTotal = "";
							$.ajax({
								url: "report.php/getSellary",
								method: "POST",
								data: {getTotal:getTotal},
								success: function (data) {
									$("#salleryID").text(data);
								}
							});
						}


						getAllSPurchases();
						function getAllSPurchases() {
							var getPurchasesTotal = "";
							$.ajax({
								url: "report.php/getPurchases",
								method: "POST",
								data: {getPurchasesTotal:getPurchasesTotal},
								success: function (data) {
									$("#purchaseID").text(data);
								}
							});
						}

					});
				</script>

				<?php require_once '../Include/script.php'; ?>
			</body>
			</html>
			<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
			<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
			<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

			<script type="text/javascript">
				$(document).ready(function() {
					$('#lists').DataTable();
				});
			</script>
