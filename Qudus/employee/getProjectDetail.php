<?php
// include Database connection file

 require_once'projectClass.php';
// check request
if(isset($_POST))
{
    // get User ID
    $pId = $_POST['hiddenId'];

    // Get User Details
   $result = getProjectDetail($pId);

    $response = array();
    if($result->rowcount()>0){
      while($row=$result->fetch())
      {
            $response = $row;
        }
    }
    else
    {
        $response['status'] = 200;
        $response['message'] = "Data not found!";
    }
    // display JSON data
    echo json_encode($response);
}
else
{
    $response['status'] = 200;
    $response['message'] = "Invalid Request!";
}

?>