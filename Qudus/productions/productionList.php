<?php
//session_start();
//unset($_SESSION['shopping_cart']);
?>
<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>

<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Production List</h4>
														<span>Qudus <code>Constraction</code> Company</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">

													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
										</div>

										<div class="card-block">
											
											<div>
												<a href="index.php"><button style="margin:10px" class="btn btn-success btn-round waves-effect md-trigger float-right"><i class="icofont icofont-plus-alt"> </i>New Production</button></a>
											</br></br></br>
											</div>
											<div class="dt-responsive table-responsive">
												<table id="Purchases" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>NO</th>
															<th>Product Name</th>
															<th>Quantity</th>
															<th>Date</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<?php
													require_once 'productionClass.php';
													$result=getProductionList();
													$i=0;
													while ($row=$result->fetch()) {
														$i++;
														echo '
														<tbody>
														<tr>
														<td>'.$i.'</td>
														<td>'.$row['Name'].'</td>
														<td>'.$row['qty'].'</td>
														<td>'.date("d/m/Y",strtotime($row['StartDate'])).'</td>';
														echo '<td><button class="btn btn-info" onClick="" ><a href="../Reports/CurentProductList.php" ><i class="fa fa-bin">See Store</a></button>
														</td>
														</tr>';
													}
													?>
												</tbody>
											</table>
										</div>

										<!-- Detail Model -->
										<?php require_once 'Models.php'; ?>
										<!-- End Detail Model -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once '../Include/script.php'; ?>
</body>
</html>
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script type="text/javascript" src="../files/assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="../files/assets/pages/j-pro/js/jquery-cloneya.min.js"></script>
<script type="text/javascript">

									$(document).ready( function () {
										// $('#Purchases').DataTable();

										// $('#fondationErr').hide();

									});
									function seeDetail(PurchaseId) {

						            $.post("getProductionDetails.php", {
						                PurchaseId: PurchaseId,
						            },function (data, status) {
						                // alert(data)
						                $('#Showtheresult').html(data);
						                $("#purchasesDetials").modal("show");
						            });
								}

                          function Remove(PurchaseId) {

						            $.post("removePurchase.php", {
						                PurchaseId: PurchaseId,
						            },function (data, status) {
						                // alert(data)
						                // $('#Showtheresult').html(data);
						                // $("#purchasesDetials").modal("show");
										window.location.replace('purchaseList.php');
						            });
								}



                            </script>
