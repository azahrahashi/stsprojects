<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Supplier</h4>
														<span>This Is The <code>Supplier</code> Managment Page.</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<br>
										<div class="col-md-12">
											<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#asupplier"><i class="fa fa-plus"></i> Add</button>
										</div>
										<div class="card-block">
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Company Name</th>
															<th>Contect Person</th>
															<th>Phone Number</th>
															<th>Address</th>
															<th>create by</th>
															<th>Startdate</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<tbody>

													</tbody>
												</table>
											</div>

											<!-- Add Stagetype Model -->

											<div class="modal fade" id="asupplier" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<form method="post" id="user_form" enctype="multipart/form-data">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title">Add Supplier</h4>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="CompanyName" name="CompanyName" class="form-control" placeholder="Company Name">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="ContectPerson" name="ContectPerson" class="form-control" placeholder="Contect Person">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="Number" id="PhoneNumber" name="PhoneNumber" class="form-control" placeholder="Phone Number">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="Address" name="Address" class="form-control" placeholder="Address">
																		<input type="hidden" id="createdBy" name="createdBy" class="form-control" placeholder="created By" value="<?php echo getCurrentUserNameByUserName($_SESSION[getSessionName()]);?>">
																	</div>
																</div>
																<div class="form-group row">
																	<br>
																	<div class="col-sm-12">
																		<input type="date" id="StartDate" name="StartDate" class="form-control" placeholder="Start Date">
																	</div>
																	<br>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary waves-effect waves-light "onclick="addsuppliers()">Save changes</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
											<!-- End Add Stagetype Model -->


											<!-- Update Model -->

											<div class="modal fade" id="Usupplier" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<form method="post" id="user_form" enctype="multipart/form-data">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title"> Update supplier </h4>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="hidden" name="uSupplierId" id="uSupplierId" value="">
																		<input type="text" id="uCompanyName" name="uCompanyName" class="form-control" placeholder="Company Name">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="uContectPerson" name="uContectPerson" class="form-control" placeholder="Contect Person">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="uPhoneNumber" name="uPhoneNumber" class="form-control" placeholder="Phone Number">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="uAddress" name="uAddress" class="form-control" placeholder="Address">
																		<input type="hidden" id="ucreatedBy" name="ucreatedBy" class="form-control" placeholder="created By">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="date" id="uStartDate" name="uStartDate" class="form-control" placeholder="Start Date">
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary waves-effect waves-light " onclick="updatesuppliers()"> Save changes</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										<!-- End Update Model -->


									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>

<script type="text/javascript">

get_supplier();
function get_supplier() {
	$.ajax({
		url: "get_supplier.php",
		method: "GET",
		success: function (data) {
			$("tbody").html("");
			$("tbody").append(data);
		}
	});
}

function addsuppliers() {

	var CompanyName = $("#CompanyName").val();
	var ContectPerson = $("#ContectPerson").val();
	var Phone = $("#PhoneNumber").val();
	var Address = $("#Address").val();
	var createdBy = $("#createdBy").val();
	var StartDate = $("#StartDate").val();

	if (CompanyName == "") {
		$("#CompanyName").after('<p class="error_message">Company name?</p>');
		$("#CompanyName").closest('.form-group').addClass('has-error');
	}
	else if (ContectPerson == "") {
		$(".error_message").remove();
		$("#ContectPerson").after('<p class="error_message">Contact person?</p>');
		$("#ContectPerson").closest('.form-group').addClass('has-error');
	}
	else if (Phone == "") {
		$(".error_message").remove();
		$("#PhoneNumber").after('<p class="error_message">Phone Number?</p>');
		$("#PhoneNumber").closest('.form-group').addClass('has-error');
	}
	else if (Address == "") {
		$(".error_message").remove();
		$("#Address").after('<p class="error_message">Address?</p>');
		$("#Address").closest('.form-group').addClass('has-error');
	}
	else if (StartDate == "") {
		$(".error_message").remove();
		$("#StartDate").after('<p class="error_message">Address?</p>');
		$("#StartDate").closest('.form-group').addClass('has-error');
	}
	else {
		$(".error_message").remove();
		$.post("addSupplier.php", {
			CompanyName:CompanyName,
			ContectPerson:ContectPerson,
			Phone:Phone,
			Address:Address,
			createdBy:createdBy,
			StartDate:StartDate,
			SupplierId:0
		},
		function (data, status) {
			$('#asupplier').modal('hide');
			$("tbody").html("");
			get_supplier();
			$("input").val("");
		});
	}
}

function setsupplier(SupplierId,CompanyName,ContectPerson,Phone,Address,createdBy,StartDate){

	// alert(Phone);

	$("#uSupplierId").val(SupplierId);
	$("#uCompanyName").val(CompanyName);
	$("#uContectPerson").val(ContectPerson);
	$("#uPhoneNumber").val(Phone);
	$("#uAddress").val(Address);
	$("#ucreatedBy").val(createdBy);
	$("#uStartDate").val(StartDate);
	$("#Usupplier").modal("show");

}

function updatesuppliers(){

	var SupplierId = $("#uSupplierId").val();
	var CompanyName = $("#uCompanyName").val();
	var ContectPerson = $("#uContectPerson").val();
	var Phone = $("#uPhoneNumber").val();
	var Address = $("#uAddress").val();
	var createdBy = $("#ucreatedBy").val();
	var StartDate = $("#uStartDate").val();

	if (CompanyName == "") {
		$("#uCompanyName").after('<p class="error_message">Company name?</p>');
		$("#uCompanyName").closest('.form-group').addClass('has-error');
	}
	else if (ContectPerson == "") {
		$(".error_message").remove();
		$("#uContectPerson").after('<p class="error_message">Contact person?</p>');
		$("#uContectPerson").closest('.form-group').addClass('has-error');
	}
	else if (Phone == "") {
		$(".error_message").remove();
		$("#uPhoneNumber").after('<p class="error_message">Phone Number?</p>');
		$("#uPhoneNumber").closest('.form-group').addClass('has-error');
	}
	else if (Address == "") {
		$(".error_message").remove();
		$("#uAddress").after('<p class="error_message">Address?</p>');
		$("#uAddress").closest('.form-group').addClass('has-error');
	}
	else if (StartDate == "") {
		$(".error_message").remove();
		$("#uStartDate").after('<p class="error_message">Address?</p>');
		$("#uStartDate").closest('.form-group').addClass('has-error');
	}
	else {
		$(".error_message").remove();
		$.post("addSupplier.php",{
			CompanyName:CompanyName,
			ContectPerson:ContectPerson,
			Phone:Phone,
			Address:Address,
			createdBy:createdBy,
			StartDate:StartDate,
			SupplierId:SupplierId
		}, function (data, status) {
			$('#Usupplier').modal('hide');
			$("tbody").html("");
			get_supplier();
			$("input").val("");
		});
	}

}

function removeSupplier(SupplierId) {

	$.post("removeSupplier.php",{
		SupplierId:SupplierId
	},
	function (data, status) {
		$("tbody").html("");
		get_supplier();
	});

}
</script>
