<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>

<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Foundations</h4>
														<span>This Is The <code>Foundations</code> Management Page.</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">

													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
										</div>

										<div class="card-block">
											<div>
												<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#aStage"><i class="fa fa-plus"></i> Add</button>
											</div>
											<br><br>
											<br>
											<div class="dt-responsive table-responsive">
												<table id="Stagedata" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>ID</th>
															<th>Name</th>
															<th>Stage Type</th>
															<th>StatrDate</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<?php
													require_once 'StageClass.php';
													$result=getstages();
													while ($row=$result->fetch()) {

														echo '
														<tbody>
														<tr>
														<td>'.$row['Id'].'</td>
														<td>'.$row['Name'].'</td>
														<td>'.$row['StageType'].'</td>
														<td>'.date("d-m-Y",strtotime($row['StartDate'])).'</td>
														<td>';?>
														<button class="btn btn-success"
														onClick="setStages(<?php echo $row['Id'];?>,'<?php echo $row['StartDate'];?>','<?php echo $row['Name'];?>','<?php echo $row['StageType'];?>',<?php echo $row['StageTypeId'];?>)"> Update</button>
														<?php
														echo '<button class="btn btn-warning" onClick="removeSt('.$row['Id'].')" ><i class="fa fa-bin">Remove</button></td>
														</tr>';
													}
													?>
												</tbody>
											</table>
										</div>
										<!-- Add Model -->

										<div class="modal fade" id="aStage" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Add Foundation</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<h5>Foundation</h5>
															<br>
															<label>Enter Foundation Name</label>
															<input type="text" name="Name" id="Name" class="form-control" />
															<br>
															<!-- <label>Start Date</label> -->
															<input type="hidden" name="StartDate" id="StartDate" class="form-control" value="<?php echo date("Y-m-d") ?>" />
															<br>
															<label>Select Foundation Type</label>
															<select class="form-control" id="StageType">
																<option value="">Select Foundation Type</option>
																<?php
																$result = getstagestype();
																if (!empty($result))
																{
																	while ($row=$result->fetch()) {
																		echo '<option value ="' . $row["Id"] . '">' . $row["Name"]. '</option>';
																	}
																}
																?>
															</select>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="newStage()">Save changes</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>

										<!-- End Add Model -->

										<!-- Update Model -->

										<div class="modal fade" id="uStage" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Update Foundation</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<h5>Foundation</h5>
															<br>
															<input type="hidden" id="Id" name="Id" value=""  class="form-control">
															<label>Enter Foundation Name</label>
															<input type="text" name="uName" id="uName" class="form-control" />
															<br>
															<!-- <label>Start Date</label> -->
															<input type="hidden" name="uStartDate" id="uStartDate" class="form-control" value="<?php echo date("Y-m-d") ?>" />
															<br>
															<label>Select Type</label>
															<select class="form-control" id="uStageType">
																 <!-- <option value="0"> Select Fondation Type</option> --> -->
																<?php
																$result = getstagestype();
																if (!empty($result))
																{
																	
																	while ($row=$result->fetch()) {
																		echo '<option value ="' . $row["Id"] . '">' . $row["Name"]. '</option>';
																	}
																}
																?>
															</select>
															<span id="fondationErr" class="text-danger"></span>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="updateStage()">Save changes</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>


										<!-- End Update Model -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

	$(document).ready( function () {
		$('#Stagedata').DataTable();

		$('#fondationErr').hide();



	});



	function newStage() {

		var Name = $("#Name").val();
		var StartDate = $("#StartDate").val();
		var StageType = $("#StageType").val();


		$('.danger').remove();

		if (Name == "") {
			$("#Name").after('<p class="danger"> Name field is required!</p>');
		}
		else if(StageType == ""){
			$("#StageType").after('<p class="danger"> StageType field is required!</p>');
		}else{

			$.post("addstage.php", {
				Name: Name,
				StartDate: StartDate,
				StageType:StageType,
				Id:0
			},
			function (data, status) {
				alert(data);
				window.location.reload();

			});
		}
	}


     function setStages(Id,StartDate,Name,StageType,StageTypeId){

			 // alert(Id+','+Name+','+StartDate+','+StageType+','+StageTypeId);
			 $("#Id").val(Id);
			 $("#uName").val(Name);
			 $("#uStageType").val(StageTypeId);
			 $("#uStartDate").val(StartDate);
			  $("#StageTypesSelect").empty();
			 $("#uStageType").append('<option value="'+StageTypeId+'">'
			 	+StageType+'</option>');
			 $("#uStage").modal("show");

			}


		// update function

		function updateStage(){

			var Id=$("#Id").val();
			var Name=$("#uName").val();
			var StageType=$("#uStageType").val();
			var StartDate=$("#uStartDate").val();

			if(StageType==0)
				$('#fondationErr').text('Required');
		  // alert(Id+','+Name+','+StageType+','+StartDate+',');

		  if (StageType==0) {
		  	$('#fondationErr').show();
		  }
		  else{

		  	$.post("addstage.php",{
		  		Name:Name,
		  		StageType:StageType,
		  		StartDate:StartDate,
		  		Id:Id
		  	}, function (data, status) {
		  		alert("success");
		  		window.location.reload();
		  	});

		  }
		}

            //remove function

            function removeSt(Id) {

            	$.post("removeStage.php",{
            		Id:Id
            	},
            	function (data, status) {
            		alert("success");
            		window.location.reload();
            	});

            }

        </script>

        <style type="text/css">
        	.danger{
        		color:red;
        	}
        </style>
