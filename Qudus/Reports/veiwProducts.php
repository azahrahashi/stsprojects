<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Basic Form Inputs</h4>
														<span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											<div>
												
											</div>
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th style="width: 10%;">Id</th>
															<th style="width: 10%;">product name</th>
															<th style="width: 10%;">Startdate</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>

													 <?php 
													require_once 'reportClass.php';
													$result=VeiwProducts();
													while ($row=$result->fetch()) {

														echo '
														<tbody>
														<tr>
														<td>'.$row['Id'].'</td>
														<td>'.$row['Name'].'</td>
														<td>'.$row['StartDate'].'</td>
														<td>';?>
														
														<?php

													}
													?>
													
												</tbody>
											</table>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>

		   