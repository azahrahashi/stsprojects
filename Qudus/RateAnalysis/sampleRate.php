<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<?php
$StageTypeId=0;
if (isset($_GET['pId']))
	$ProjectId=$_GET['pId'];
?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Sample Rate Form</h4>
														<!-- 	<span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span> -->
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">

											<div class="card">
												<div class="card-header">
													<h5>Sample Rate Analysis</h5>
													<!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
													<div class="card-header-right">
														<i class="icofont icofont-spinner-alt-5"></i>
													</div>
												</div>
												<div class="card-block">
													<h4 class="sub-title">Sample Rate Analysis</h4>
														<div class="form-group row">
															<label class="col-sm-2 col-form-label">Choose Stage</label>
															<div class="form-radio">
																<?php
																require_once '../Classes/stageClass.php';
																$result = getStageTypes();
																if (!empty($result)){
																	while ($row=$result->fetch())  {
																		$StageTypeId =$row['Id'];
																		echo '
																		<div class="radio radio-inline">
																		<label>
																		<input type="radio" name="stageTypes" id="r'.$StageTypeId.'"  onclick="setForm(this,'.$StageTypeId.');">
																		<i class="helper"></i>'.$row['Name'].'
																		</label>
																		</div>
																		';
																	}
																}
																?><span class="text-danger error" id="types">Please Choose One of them</span>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-2 col-form-label">Select Foundation</label>
															<div class="form-radio" id="stageList">
													   </div>
													   <span class="text-danger error" id="stages">Please Choose One of them</span>
												</div>
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Item Description</label>
													<div class="col-sm-8">
														<textarea rows="5" cols="5" class="form-control" placeholder="Default textarea" id="itemDescription"></textarea>
													</div>
													   <span class="col-sm-2 text-danger error" id="itemDesc">Please  fill </span>
													   <input type="hidden" name="" id="projectId" value="<?php echo $ProjectId; ?>">
												</div>
												<div class="form-group row">
													<label class="col-sm-2 col-form-label"> Unit</label>
													<div class="col-sm-8">
														<input type="text" name="" id="unitDesc" class="form-control" value="">
													</div>
													   <span class="col-sm-2 text-danger error" id="unitDescErr">Please  fill </span>
													   <input type="hidden" name="" id="projectId" value="">
												</div>
												<div class="form-group row">
													<label class="col-sm-12 col-form-label text-center " style="font: bold;">Sample Rate Analysis</label>
												</div>
												<div class="col-sm-12 col-xl-12" id="analysisButton" style="overflow-x:auto;">
													<button class="btn btn-grd-success float-right"onclick="newRow();">Add new Row</button>
													<table class="table table-framed table-bordered col-sm-12 col-xl-12">
														<thead>
															<th>Item Description</th>
															<th>Unit</th>
															<th>Qty</th>
															<th>Rate USD</th>
															<th>Amount USD</th>
														</thead>
														<tbody id="descriptionAnalysis" class="col-xl-12">

														</tbody>
													</table>
												</div>
												<div class="form-group row">
													<div class="col-sm-12">
														<button type="submit" class="form-control float-right btn btn-success"  onclick="saveAnalysis()">submit</button>
													</div>
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script>
$( document ).ready(function() {
    $('.error').hide();
});
    var anyl=[];
	var desc,unit,qty,rate,amount,stageType,stage,itemDesc,projectId,totalRate,unitDesc;
	stageType=stage=totalRate=0;
	var i=0;
	function saveAnalysis() {
		for (var j = 0; j <= i-1; j++) {
			desc=$('#desc'+j).val();
			unit=$('#unit'+j).val();
			qty=parseInt($('#qty'+j).val());
			rate=parseFloat($('#rate'+j).val());
			amount=parseFloat($('#amount'+j).val());
		 if(isNaN(amount)){
           amount = 0.00;
       }
		 if(isNaN(rate)){
           rate = 0.00;
       }
		 if(isNaN(qty)){
           qty = 0.00;
       }
		 if(isNaN(totalRate)){
           qty = 0.00;
       }
			 var info = desc + "," + unit + "," + qty + "," + rate+ "," +amount;
			anyl.push(info);
			totalRate=parseFloat((totalRate+amount));
		}
		itemDesc=$('#itemDescription').val();
		projectId=$('#projectId').val();
		unitDesc=$('#unitDesc').val();
		// alert(stage);
		if (stageType==0) {
			// $('#types').show();
			alert("Please pick up a foundation ");
		}
		else if (stage==0) {
			$('#stages').show();
			alert("Please pick up a foundation ");
		}
		else if (itemDesc=="") {
			$('#itemDesc').show();
		}
		else if (unitDesc=="") {
			$('#unitDescErr').show();
		}
		// console.log(anyl);
		// alert(77+','+anyl.length+','+stageType+','+stage+','+itemDesc+','+projectId+','+totalRate);
		else
		{

        $.post("addSampleRate.php", {
          anyl:anyl,
          itemDesc:itemDesc,
          stageType:stageType,
          stage:stage,
          totalRate:totalRate,
          unitDesc:unitDesc,
          projectId:projectId
           }, function (data, status) {
           // alert(data)

                       window.location.replace('../projects/projectList.php');

           });

		}
	}

	function setForm(e,id){
		stageType=id;
		$('#itemDescription').empty();
		$('#stageList').empty();
		if (id==1) {
		$('#itemDescription').val('Excavation for  ------ columns foundation up to ------ m '+"\n"+' depth including sorting out and stacking of useful materials '+"\n"+' and disposing of excavated stuff up to ----- m lead for loose or soft soil ');
		}
		else if(id==2){
			$('#itemDescription').val('Providing and laying controlled cement concrete ----- m '+"\n"+' and curing compelete excluding cost of formwork and reinforcement '+"\n"+' for reinforced work in slabs, landings, sleves balconies lintels, beams,'+"\n"+' girders cantilever up to floor two level ');

		}

		else if(id==3){
			$('#itemDescription').val('Providing HYSD bar reinforcement for RCC work including bending, bending, '+"\n"+' and placing in position up to floor two level ');

		}

			$.post("../ajaxStage/stageListByType.php",{
				typeId: id
			}, function (data, status) {
				console.log(data);
				var j=data.length-1;
				for (var i = 0; i <data.length ; i++) {
					$('#stageList').append(
						''+
						'<div class="radio radio-inline">'+
						'<label>'+
						'<input class="border-checkbox" name="stages" type="radio" id="f'+data[i].Id+'" onclick="setStage('+data[i].Id+');">'+
						'<i class="helper"></i>'+data[i].Name+
						'</label>'+
						'</div>');
				}
				$('.error').hide();

		});
	}
	function setStage(stageId) {
		stage=stageId;
		$('.error').hide();
	}

	function newRow() {
		$('#descriptionAnalysis').append('<tr>'+
			'<td><input id="desc'+i+'" style="width:100%"></input></td>'+
			'<td><input id="unit'+i+'" style="width:25%"></input></td>'+
			'<td><input id="qty'+i+'" onblur="" style="width:25%"></input></td>'+
			'<td><input id="rate'+i+'" onblur="setAmount()" style="width:25%"></input></td>'+
			'<td><input id="amount'+i+'" style="width:25%"></input></td>'+
			'</tr>');

		i++;
	}
	function setAmount() {
		for (var j = 0; j <= i-1; j++) {
			qty=parseInt($('#qty'+j).val());
			rate=parseFloat($('#rate'+j).val());
			$('#amount'+j).val(qty*rate);

		 if(isNaN(amount)){
           amount = 0.00;
       }
		 if(isNaN(rate)){
           rate = 0.00;
       }
		 if(isNaN(qty)){
           qty = 0.00;
       }
		 if(isNaN(totalRate)){
           qty = 0.00;
       }
			 // var info = desc + "," + unit + "," + qty + "," + rate+ "," +amount;
			// anyl.push(info);
			// totalRate=parseFloat((totalRate+amount));
		}
	}
</script>
			<style>
			input {
				border:0px;
				border-bottom:2px solid #999;
			}
		</style>
