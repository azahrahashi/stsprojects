<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Were Houses </h4>
														<span>This Is The <code>WereHouse</code>Management Page.</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<br>
										<div class="col-md-12">
											<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#AWarehouse"><i class="fa fa-plus"></i> Add</button>
										</div>
										<div class="card-block">

											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Warehouse Name</th>
															<th>Warehouse Address</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>

													<tbody>

													</tbody>

													</tbody>
												</table>
											</div>

											<!-- Add Stagetype Model -->

											<div class="modal fade" id="AWarehouse" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<form method="post" id="user_form" enctype="multipart/form-data">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title"> Add Warehouse</h4>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<br>
																	<div class="col-sm-12">
																		<span id="WarehouseNameErr" class="text-danger"></span>
																		<input type="text" id="WarehouseName" name="WarehouseName" class="form-control" placeholder="Warehouse Name">
																	</div>
																	<br><br>
																	<br>
																	<div class="col-sm-12">
																		<input type="text" id="WarehouseAddress" name="WarehouseAddress" class="form-control" placeholder=" Warehouse Address">
																	</div>
																	<br>
																</div>

																<div class="modal-footer">
																	<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																	<button type="button" id="add-warehouse" class="btn btn-primary waves-effect waves-light" onclick="addWarehouse();">Save</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
											<!-- End Add Stagetype Model -->


											<!-- Update Model -->
											<div class="modal fade" id="UWarehouse" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<form method="post" id="user_form" enctype="multipart/form-data">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title"> Upadate Warehouse</h4>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<br>
																	<input type="hidden" name="uWarehouseId" id="uWarehouseId">
																	<div class="col-sm-12">
																		<input type="text" id="uWarehouseName" name="uWarehouseName" class="form-control" placeholder="Warehouse Name">
																	</div>
																	<br><br>
																	<br>
																	<div class="col-sm-12">
																		<input type="text" id="uWarehouseAddress" name="uWarehouseAddress" class="form-control" placeholder=" Warehouse Address">
																	</div>
																	<br>
																</div>

																<div class="modal-footer">
																	<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary waves-effect waves-light "onclick="UpdateWarehouse()">Upsate</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>

											<!-- End Update Model -->


										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php require_once '../Include/script.php'; ?>
	</body>
	</html>

	<script type="text/javascript">

	get_where_house();
	function get_where_house() {
		$.ajax({
			url: "ger_where_house.php",
			method: "GET",
			success: function (data) {
			 $("tbody").html("");
			 $("tbody").append(data);
			}
		});
	}

	function addWarehouse() {

		var WarehouseName = $("#WarehouseName").val();
		var WarehouseAddress = $("#WarehouseAddress").val();

		if (WarehouseName == "") {
			$("#WarehouseName").after('<p class="error_message">Wherehouse name?</p>');
			$("#WarehouseName").closest('.form-group').addClass('has-error');
		}
		else if (WarehouseAddress == "") {
			$(".error_message").remove();
			$("#WarehouseAddress").after('<p class="error_message">Wherehouse Address</p>');
			$("#WarehouseAddress").closest('.form-group').addClass('has-error');
		}
		else {
			$(".error_message").remove();

			$.post("addWarehouse.php", {
				WarehouseName:WarehouseName,
				WarehouseAddress:WarehouseAddress,
				WarehouseId:0
			},
			function (data, status) {
				$('#AWarehouse').modal('hide');
				get_where_house();
				$("tbody").html("");
				$("input").val("");
			});
		}
	}

	function setWarehouse(WarehouseId,WarehouseName,WarehouseAddress){

		$("#uWarehouseId").val(WarehouseId);
		$("#uWarehouseName").val(WarehouseName);
		$("#uWarehouseAddress").val(WarehouseAddress);
		$("#UWarehouse").modal("show");
	}

	function UpdateWarehouse(){

		var WarehouseId=$("#uWarehouseId").val();
		var WarehouseName=$("#uWarehouseName").val();
		var WarehouseAddress=$("#uWarehouseAddress").val();

		$.post("addWarehouse.php",{
			WarehouseName:WarehouseName,
			WarehouseAddress:WarehouseAddress,
			WarehouseId:WarehouseId
		}, function (data, status) {
			$('#UWarehouse').modal('hide');
			get_where_house();
			$("tbody").html("");
			$("input").val("");
		});

	}



	function removeWarehouse(WarehouseId) {

		$.post("removeWarehouse.php",{
			WarehouseId:WarehouseId
		},
		function (data, status) {
			get_where_house();
			$("tbody").html("");
		});

	}
	</script>
