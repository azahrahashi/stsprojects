<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Basic Form Inputs</h4>
														<span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											<div>
												<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#ACustomer"><i class="fa fa-plus"></i> Add</button>
											</div><br><br><br>
											<div class="dt-responsive table-responsive">
												<table id="Customertable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Customer Name</th>
															<th>Gender</th>
															<th>Telephone</th>
															<th>Address</th>
															<th>Email</th>
															<th>StartDate</th>
															<th style="width: 10%;" >Action</th>
														</tr>
													</thead>

													<tbody>
														
													</tbody>
													
												</tbody>
											</table>
										</div>

										<!-- Add Stagetype Model -->

								<div class="modal fade" id="ACustomer" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Add Customer</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="Name" name="Name" class="form-control" placeholder="Name..">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<select name="Gender" id="Gender" class="form-control">
																		<option value="">Gender</option>
																		<option value="Male">Male</option>
																		<option value="Female">Female</option>
																	</select>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="Telephone" name="Telephone" class="form-control" placeholder="Telephone..">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="text" id="Address" name="Address" class="form-control" placeholder="Address..">
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="Email" name="Email" class="form-control" placeholder="Email..">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="date" id="StartDate" name="StartDate" class="form-control" placeholder="StartDate..">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light "onclick="addCustomer()">Save Customer</button>
															</div>
														</div>
													</div>
												</form>
											</div> 
										</div>
										<!-- End Add Stagetype Model -->


										<!-- Update Model -->
									<div class="modal fade" id="UCustomer" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Update Customer</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="hidden" id="uCustomerId" name="uCustomerIds" class="form-control" placeholder="Name..">
																	<input type="text" id="uName" name="uName" class="form-control" placeholder="Name..">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<select name="uGender" id="uGender" class="form-control">
																		<option value="">Gender</option>
																		<option value="Male">Male</option>
																		<option value="Female">Female</option>
																	</select>
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="uTelephone" name="uTelephone" class="form-control" placeholder="Telephone..">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="text" id="uAddress" name="uAddress" class="form-control" placeholder="Address..">
																</div>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="uEmail" name="uEmail" class="form-control" placeholder="Email..">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="date" id="uStartDate" name="uStartDate" class="form-control" placeholder="StartDate..">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light "onclick="updateCustomer()">Update Customer</button>
															</div>
														</div>
													</div>
												</form>
											</div> 
										</div>
										<!-- End Update Model -->


									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script type="text/javascript">

	getAllCustomers();
	function getAllCustomers() {
		$.ajax({
			url: "Getcustomer.php",
			method: "GET",
			success: function (data) {
				// alert(data);
				$("tbody").html("");
				$("#Customertable").append(data);
			}
		});
	}
	function addCustomer() {
		var Name = $("#Name").val();
		var Gender = $("#Gender").val();
		var Telephone = $("#Telephone").val();
		var Address = $("#Address").val();
		var Email = $("#Email").val();
		var StartDate = $("#StartDate").val();
		var CustomerId = 0;

		if (Name == "") {
			alert("Empty is not allowed");
		}
		else if(Gender == ""){
			alert("Empty is not allowed");
		}
		else if(Telephone == ""){
			alert("Empty is not allowed");
		}
		else if(Address == ""){
			alert("Empty is not allowed");
		}
		else if(Email == ""){
			alert("Empty is not allowed");
		}
		else if(StartDate == ""){
			alert("Empty is not allowed");
		}
		else{
			$.ajax({
				url: "addCustomer.php",
				method: "POST",
				data: {Name:Name,Gender:Gender,Telephone:Telephone,Address:Address,Email:Email,StartDate:StartDate,CustomerId: CustomerId},
				success: function (data) {
					getAllCustomers();
					$('#ACustomer').modal('hide');
				}
			});

		}



	}		

	function setCustomer(CustomerId,Name,Gender,Telephone,Address,Email,StartDate) {

		  // alert(CustomerId);

		$("#uCustomerId").val(CustomerId);
		$("#uName").val(Name);
		$("#uTelephone").val(Telephone);
		$("#uAddress").val(Address);
		$("#uEmail").val(Email);
		if (Gender == "Female") {
			$('#uGender').html('<option value="'+Gender+'">Female</option>'
				+'<option value="Male">Male</option>'
				);
		}
		else if (Gender == "Male") {
			$('#uGender').html('<option value="'+Gender+'">Male</option>'
				+'<option value="Female">Female</option>'
				);
		}
		$("#uStartDate").val(StartDate);
		$("#UCustomer").modal("show");
	}

	function updateCustomer(){

		var CustomerId=$("#uCustomerId").val();
		var Name=$("#uName").val();
		var Gender = $('#uGender').val();
		var Telephone = $('#uTelephone').val();
		var Address=$("#uAddress").val();
		var Email=$("#uEmail").val();
		var StartDate=$("#uStartDate").val();

		$.post("addCustomer.php",{
			CustomerId:CustomerId,
			Name:Name,
			Gender:Gender,
			Telephone:Telephone,
			Address:Address,
			Email:Email,
			StartDate:StartDate
		}, function (data, status) {
			// alert(data);
			$("tbody").html("");
			getAllCustomers();
			$('#UCustomer').modal('hide');
		});

	}

	function removeCustomer(CustomerId) {

		$.post("removeCustomer.php",{
			CustomerId:CustomerId
		},
		function (data, status) {
			$("tbody").html("");
			getAllCustomers();
});

	}


</script>

