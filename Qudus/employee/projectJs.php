<script type="text/javascript">
				$(document).ready(function() {
					$('#error').hide();
				});
				// project registration script
				function saveproject(){
					   // get values from Input
			     var pId =$("#pId").val();
			     var pName =$("#pName").val();
			     var pOwner =$("#pOwner").val();
			     var pDetail =$("#pDetail").val();
			    
			    if(pName==''){
			       $('#error').show();
			            $('#pName').focus();
			     }
			     if(pOwner==''){
			        $('#error').show();
			            $('#pOwner').focus();
			     }			    
			   
			    else{
			      $('#error').hide();           
			        $.post("addProject.php", {
			            pId:pId,
			            pName: pName,
			            pOwner: pOwner,
			         	 pDetail: pDetail

			        }, function (data, status) {    

			     // alert(data);			        

			             swal("You have saved Successfully!", {
				              icon: "success",
				            }).then(function(){ 
				                   location.reload();
				                   }
				                );
			            // close the popup
			         $("#default-Modal").modal("hide");

			            // read records again
			            // clear fields from the popup
			            $("#pName").val("");
			            $("#pOwner").val("");
			            $("#pOwner").val("");
			            $("#pDetail").val("");
			           
			        });
			        }				
				}
				/**********************************
            get Project  Detail
            ***********************************/ 
            function getProjectDetail(hiddenId) {
             $("#pId").val(hiddenId);
               var hiddenId = hiddenId;
         // alert(hiddenId);
          //   // Add User ID to the hidden field for furture usage
            $.post("getProjectDetail.php", {
                    hiddenId: hiddenId
                },
                function (data, status) {
                // alert(data);    
                            // PARSE json data
                    var user = JSON.parse(data);
                    // Assing existing values to the modal popup fields      
                    $("#pId").val(user.ProjectId);
                    $("#pName").val(user.Name);
                    $("#pOwner").val(user.Owner);            
                    $("#pDetail").val(user.Describtion);
                  
                   }
            );
          //   // Open modal popup
              $("#default-Modal").modal("show");

        }

                /*******************************
			    Remove Project by updateing finishDate
				********************************/
    function RemoveProject(hiddenId){
        var hiddenId = hiddenId;
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
         $.post("RemoveProject.php", {
                hiddenId: hiddenId
               }, function (data, status) {
          swal("Poof! Your imaginary file has been deleted!", {
              icon: "success",
            }).then(function(){ 
                   location.reload();
                   }
                );
            });;

          
          } else {
            swal("Your imaginary file is safe!");
          }
        });


    }
			</script>
			
			<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>