
<div class="modal fade" id="AddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
    
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Employe</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
              <form method="post">
                <div class="content">

                  <div class="row form-group">
                    <div class="col-md-6">
                      <label class="col-form-label" for="inputDefault">FName</label>
                      <input class="form-control" id="FName" type="text" placeholder="FName">
                    </div>

                     <div class="col-md-6">
                      <label class="col-form-label" for="inputDefault">LName</label>
                       <input class="form-control" id="LName" type="text" placeholder="LName">
                    </div>
                    
                     <div class="col-md-6">
                      <label class="col-form-label" for="inputDefault">Gender</label>
                      <select class="form-control" id="Gender">
                        <option value="0"> Choose Gender</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                      </select>
                    </div>

                     <div class="col-md-6">
                      <label class="col-form-label" for="inputDefault">Telephone</label>
                       <input class="form-control" id="Tele" type="text" placeholder="Tele Home">
                    </div>

                    <div class="col-md-6">
                      <label class="col-form-label" for="inputDefault">Address</label>
                      <input class="form-control" id="Address" type="text" placeholder="Address">
                    </div>

                     <div class="col-md-6">
                      <label class="col-form-label" for="inputDefault">Email</label>
                       <input class="form-control" id="Email" type="text" placeholder="Email">
                    </div>
                </div>
                     

                  </div>

                  </div>
                <div class="modal-footer ">
              <button type="button" name="submit" class="btn btn-info pull-right" onclick="addCustomer();">Add Customer</button>
                  <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                </div>
                </form>
        </div>
      </div>
    </div>
</div>


