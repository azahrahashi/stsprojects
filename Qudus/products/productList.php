<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">

					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Products</h4>
														<span>This Is The <code>Products</code> Managment Page.</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">

											<div>
												<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#Aproduct"><i class="fa fa-plus"></i> Add</button>
											</div>
											<br><br>
											<br>
											<div class="dt-responsive table-responsive">
												<table id="lists" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Product Name</th>
															<th>Unit</th>
															<th>UnitPrice</th>
															<th>SellPrice</th>
															<th>StockLevel</th>
															<th>StartDate</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>

													<tbody>

													</tbody>
												</table>
											</div>

											<!-- Add product Model -->

											<div class="modal fade" id="Aproduct" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<form method="post" id="user_form" enctype="multipart/form-data">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title">Add Product</h4>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="Name" name="Name" class="form-control" placeholder="Name..">
																	</div>
																	<br>
																	<br>
																	<div class="col-sm-12">
																		<input type="text" id="Unit" name="Unit" class="form-control" placeholder="Unit..">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="UnitPrice" name="UnitPrice" class="form-control" placeholder="UnitPrice..">
																	</div>
																	<br>
																	<br>
																	<div class="col-sm-12">
																		<input type="text" id="SalePrice" name="SalePrice" class="form-control" placeholder="SalePrice..">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="StockLevel" name="StockLevel" class="form-control" placeholder="StockLevel..">
																	</div>
																	<br>
																	<br>
																	<div class="col-sm-12">
																		<input type="date" id="StartDate" name="StartDate" class="form-control" placeholder="StartDate..">
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary waves-effect waves-light "onclick="addProduct()">Save Product</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
											<!-- End Add Stagetype Model -->


											<!-- Update Model -->

											<div class="modal fade" id="Uproduct" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<form method="post" id="user_form" enctype="multipart/form-data">
														<div class="modal-content">
															<div class="modal-header">
																<h4 class="modal-title">Edit Product</h4>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																	<span aria-hidden="true">&times;</span>
																</button>
															</div>
															<div class="modal-body">
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="hidden" name="Id" id="Id">
																		<input type="text" id="uName" name="uName" class="form-control" placeholder="Name..">
																	</div>
																	<br>
																	<br>
																	<div class="col-sm-12">
																		<input type="text" id="uUnit" name="uUnit" class="form-control" placeholder="Unit..">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="uUnitPrice" name="uUnitPrice" class="form-control" placeholder="UnitPrice..">
																	</div>
																	<br>
																	<br>
																	<div class="col-sm-12">
																		<input type="text" id="uSalePrice" name="uSalePrice" class="form-control" placeholder="SalePrice..">
																	</div>
																</div>
																<div class="form-group row">
																	<div class="col-sm-12">
																		<input type="text" id="uStockLevel" name="uStockLevel" class="form-control" placeholder="StockLevel..">
																	</div>
																	<br>
																	<br>
																	<div class="col-sm-12">
																		<input type="date" id="uStartDate" name="uStartDate" class="form-control" placeholder="StartDate..">
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																	<button type="button" class="btn btn-primary waves-effect waves-light "onclick="updateProducts()">Edit Product</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
											<!-- End Update Model -->


										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php require_once '../Include/script.php'; ?>
	</body>
	</html>

	<script type="text/javascript">
	getstAllprducts();
	function getstAllprducts() {
		$.ajax({
			url: "GetProduct.php",
			method: "GET",
			success: function (data) {
				// alert(data);
				$("tbody").html("");
				$("tbody").append(data);
			}
		});
	}

	function addProduct() {
		var Name = $("#Name").val();
		var Unit = $("#Unit").val();
		var UnitPrice = $("#UnitPrice").val();
		var SalePrice = $("#SalePrice").val();
		var StockLevel = $("#StockLevel").val();
		var StartDate = $("#StartDate").val();

		if (Name == "") {
			$("#Name").after('<p class="error_message">Product name?</p>');
			$("#Name").closest('.form-group').addClass('has-error');
		}
		else if(Unit == ""){
			$(".error_message").remove();
			$("#Unit").after('<p class="error_message">Product unit?</p>');
			$("#Unit").closest('.form-group').addClass('has-error');
		}
		else if(UnitPrice == ""){
			$(".error_message").remove();
			$("#UnitPrice").after('<p class="error_message">Unit price?</p>');
			$("#UnitPrice").closest('.form-group').addClass('has-error');
		}
		else if(SalePrice == ""){
			$(".error_message").remove();
			$("#SalePrice").after('<p class="error_message">Sales price?</p>');
			$("#SalePrice").closest('.form-group').addClass('has-error');
		}
		else if(StockLevel == ""){
			$(".error_message").remove();
			$("#StockLevel").after('<p class="error_message">Stock Level?</p>');
			$("#StockLevel").closest('.form-group').addClass('has-error');
		}
		else if(StartDate == ""){
			$(".error_message").remove();
			$("#StartDate").after('<p class="error_message">Start date?</p>');
			$("#StartDate").closest('.form-group').addClass('has-error');
		}
		else{
			$(".error_message").remove();
			$.post("addProducts.php", {
				Name: Name,
				Unit:Unit,
				UnitPrice:UnitPrice,
				SalePrice:SalePrice,
				StockLevel:StockLevel,
				StartDate:StartDate,
				Id:0
			},
			function (data, status) {
				$('#Aproduct').modal('hide');
				$("tbody").html("");
				getstAllprducts();
				$("input").val("");
			});
		}

	}

	function setproduct(Id,Name,Unit,UnitPrice,SalePrice,StockLevel,StartDate){

		$("#Id").val(Id);
		$("#uName").val(Name);
		$("#uUnit").val(Unit);
		$("#uUnitPrice").val(UnitPrice);
		$("#uSalePrice").val(SalePrice);
		$("#uStockLevel").val(StockLevel);
		$("#uStartDate").val(StartDate);
		$("#Uproduct").modal("show");

	}
	function updateProducts(){

		var Id=$("#Id").val();
		var Name=$("#uName").val();
		var Unit=$("#uUnit").val();
		var UnitPrice=$("#uUnitPrice").val();
		var SalePrice=$("#uSalePrice").val();
		var StockLevel=$("#uStockLevel").val();
		var StartDate=$("#uStartDate").val();

		if (Name == "") {
			$("#uName").after('<p class="error_message">Product name?</p>');
			$("#uName").closest('.form-group').addClass('has-error');
		}
		else if(Unit == ""){
			$(".error_message").remove();
			$("#uUnit").after('<p class="error_message">Product unit?</p>');
			$("#uUnit").closest('.form-group').addClass('has-error');
		}
		else if(UnitPrice == ""){
			$(".error_message").remove();
			$("#uUnitPrice").after('<p class="error_message">Unit price?</p>');
			$("#uUnitPrice").closest('.form-group').addClass('has-error');
		}
		else if(SalePrice == ""){
			$(".error_message").remove();
			$("#uSalePrice").after('<p class="error_message">Sales price?</p>');
			$("#uSalePrice").closest('.form-group').addClass('has-error');
		}
		else if(StockLevel == ""){
			$(".error_message").remove();
			$("#uStockLevel").after('<p class="error_message">Stock Level?</p>');
			$("#uStockLevel").closest('.form-group').addClass('has-error');
		}
		else if(StartDate == ""){
			$(".error_message").remove();
			$("#uStartDate").after('<p class="error_message">Start date?</p>');
			$("#uStartDate").closest('.form-group').addClass('has-error');
		}
		else {
			$(".error_message").remove();
			$.post("addProducts.php",{
				Name:Name,
				Unit:Unit,
				UnitPrice:UnitPrice,
				SalePrice:SalePrice,
				StockLevel:StockLevel,
				StartDate:StartDate,
				Id:Id
			}, function (data, status) {
				$('#Uproduct').modal('hide');
				$("tbody").html("");
				getstAllprducts();
				$("input").val("");
			});
		}
	}

	function removeProducts(Id) {

		$.post("RemoveProducts.php",{
			Id:Id
		},
		function (data, status) {
			$("tbody").html("");
			getstAllprducts();
		});

	}
	</script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
	<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		$('#lists').DataTable();
	});
	</script>
