
<div class="modal fade" id="AddModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form method="post" id="user_form" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add Customer</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-sm-12">
              <input type="text" id="Name" name="Name" class="form-control" placeholder="Name..">
            </div>
            <br>
            <br>
            <div class="col-sm-12">
              <select name="Gender" id="Gender" class="form-control">
                <option value="">Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-12">
              <input type="text" id="Telephone" name="Telephone" class="form-control" placeholder="Telephone..">
            </div>
            <br>
            <br>
            <div class="col-sm-12">
              <input type="text" id="Address" name="Address" class="form-control" placeholder="Address..">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-12">
              <input type="text" id="Email" name="Email" class="form-control" placeholder="Email..">
            </div>
            <br>
            <br>
            <div class="col-sm-12">
              <input type="date" id="StartDate" name="StartDate" class="form-control" placeholder="StartDate..">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary waves-effect waves-light "onclick="addCustomer()">Save Customer</button>
          </div>
        </div>
      </div>
    </form>
  </div> 
</div>

<script type="text/javascript">
 function addCustomer() {
  var Name = $("#Name").val();
  var Gender = $("#Gender").val();
  var Telephone = $("#Telephone").val();
  var Address = $("#Address").val();
  var Email = $("#Email").val();
  var StartDate = $("#StartDate").val();
  var CustomerId = 0;

  if (Name == "") {
    alert("Empty is not allowed");
  }
  else if(Gender == ""){
    alert("Empty is not allowed");
  }
  else if(Telephone == ""){
    alert("Empty is not allowed");
  }
  else if(Address == ""){
    alert("Empty is not allowed");
  }
  else if(Email == ""){
    alert("Empty is not allowed");
  }
  else if(StartDate == ""){
    alert("Empty is not allowed");
  }
  else{
    $.ajax({
      url: "../customer/addCustomer.php",
      method: "POST",
      data: {Name:Name,Gender:Gender,Telephone:Telephone,Address:Address,Email:Email,StartDate:StartDate,CustomerId: CustomerId},
      success: function (data) {
        location.reload();
        $('#ACustomer').modal('hide');

      }
    });

  }



}   

</script>