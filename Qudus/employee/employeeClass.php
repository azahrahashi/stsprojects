<?php
require_once'../Connection/dbconection.php';

/**********************************
Add New Employeee
************************************/


function UpdateEmployee($firstName,$lastName,$Gender,$email){

  $stmt= getCnx()->prepare("INSERT INTO person(FirstName,LastName,Gender,Email,StartDate)
    VALUES (:firstName, :lastName, :Gender,:email,NOW())");

  $stmt->bindParam(':firstName', $firstName);
  $stmt->bindParam(':lastName', $lastName);
  $stmt->bindParam(':Gender', $Gender);
  $stmt->bindParam(':email', $email);

  $stmt->execute();

}

function addPerson($firstName,$lastName,$Gender,$email)
{
  $stmt= getCnx()->prepare("INSERT INTO person(FirstName,LastName,Gender,Email,StartDate)
    VALUES (:firstName, :lastName, :Gender,:email,NOW())");

  $stmt->bindParam(':firstName', $firstName);
  $stmt->bindParam(':lastName', $lastName);
  $stmt->bindParam(':Gender', $Gender);
  $stmt->bindParam(':email', $email);

  $stmt->execute();

  return getCnx()->lastInsertId();
}
function addPersonImage($pId,$pImage,$StartDate)
{
  $stmt= getCnx()->prepare("INSERT INTO personimage(PersonId,ImageName,StartDate)
    VALUES (:pId, :pImage,:StartDate)");
  $stmt->bindParam(':pId', $pId);
  $stmt->bindParam(':pImage', $pImage);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->execute();
}

//Inserting Place
function addPlace($address){
  $stmt= getCnx()->prepare("INSERT INTO place(Address)
    VALUES (:address)");

  $stmt->bindParam(':address', $address);
  $stmt->execute();
  return getCnx()->lastInsertId();
}

function addLives($PersonId,$PlaceId){
  $stmt=getCnx()->prepare(
    "INSERT INTO lives(PersonId,PlaceId,StartDate)
    VALUES (:PersonId, :PlaceId,NOW())");

  $stmt->bindParam(':PersonId', $PersonId);
  $stmt->bindParam(':PlaceId', $PlaceId);


  $stmt->execute();
}

function addTelephone($phone){
  $stmt=getCnx()->prepare(
    "INSERT INTO telephone(TelephoneNo)
    VALUES (:phone)");

  $stmt->bindParam(':phone', $phone);

  $stmt->execute();
  return getCnx()->lastInsertId();
}
/*************************************
  get person image by person Id
**************************************/ 

    function getPersonImage($personId)
  {

    try {
      $stmt = getCnx()->prepare("SELECT ImageName from personimage WHERE PersonId = :personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['ImageName']))
        return $row["ImageName"];
      }
      return "userImage.jpg";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }
  /******************************
  get Person Name By Person Id
  ********************************/

    function getPersonName($personId)
  {
    try {
      $stmt = getCnx()->prepare("SELECT FirstName, LastName from person 
                                  WHERE PersonId=:personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['FirstName'].''.$row['LastName']))
        return $row["FirstName"].''.$row["LastName"];
      }
      return "No Name";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }

    /******************************
  get Person Role By Person Id
  ********************************/

    function getPersonRole($personId)
  {
    try {
      $stmt = getCnx()->prepare("SELECT ar.Role FROM authorityrole ar
                                INNER JOIN authorisedmembership am on am.AuthorityRoleId = ar.AuthorityRoleId
                                INNER JOIN person p ON p.PersonId = am.PersonId
                                WHERE am.PersonId=:personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['Role']))
        return $row["Role"];
      }
      return "No Role";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }
   /******************************
  get Person gender By Person Id
  ********************************/

    function getGender($personId)
  {
    try {
      $stmt = getCnx()->prepare("SELECT  Gender from person 
                                  WHERE PersonId=:personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['Gender']))
        return $row["Gender"];
      }
      return "No Name";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  } 

     /******************************
  get Person Email By Person Id
  ********************************/

    function getPersonEmail($personId)
  {
    try {
      $stmt = getCnx()->prepare("SELECT  Email from person 
                                  WHERE PersonId=:personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['Email']))
        return $row["Email"];
      }
      return "No Email";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  }   
  /******************************
  get Person Registred Date By Person Id
  ********************************/

    function getPersonRegistredDate($personId)
  {
    try {
      $stmt = getCnx()->prepare("SELECT StartDate from person 
                                  WHERE PersonId=:personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['StartDate']))
        return $row["StartDate"];
      }
      return "No date ";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  } 

  /******************************
  get Person Address By Person Id
  ********************************/

    function getPersonAddress($personId)
  {
    try {
      $stmt = getCnx()->prepare("SELECT PL.Address FROM place PL
                                INNER JOIN lives l on l.PlaceId = pl.PlaceId
                                INNER JOIN person p on p.PersonId = l.PersonId 
                                WHERE p.PersonId=:personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['Address']))
        return $row["Address"];
      }
      return "No Address ";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  } 
    /******************************
  get Person Mobile By Person Id
  ********************************/

    function getPesonMobile($personId)
  {
    try {
      $stmt = getCnx()->prepare("SELECT  t.TelephoneNo FROM telephone t
                                  INNER JOIN uses u on u.UsesId = t.TelephoneId
                                  INNER JOIN person p on p.PersonId = u.PersonId 
                                WHERE p.PersonId=:personId");

      $stmt->bindParam(':personId', $personId);
      $stmt->execute();
      while ($row = $stmt->fetch()){
        if (!empty($row['TelephoneNo']))
        return $row["TelephoneNo"];
      }
      return "No TelephoneNo ";
    } catch (PDOException $e){
      echo $e;
    }
    return "Unknown";
  } 
  /***********************************
        get Staff Salary Detail
*************************************/ 
// function getStaffSalary($id)

function  addUses($PersonId,$TelephoneId,$StartDate){
  $stmt=getCnx()->prepare(
    "INSERT INTO uses(PersonId,TelephoneId,StartDate)
    VALUES (:PersonId, :TelephoneId, :StartDate )");

  $stmt->bindParam(':PersonId', $PersonId);
  $stmt->bindParam(':TelephoneId', $TelephoneId);
  $stmt->bindParam(':StartDate', $StartDate);

  $stmt->execute();
}

function  addSalary($PersonId,$salary){
  $stmt=getCnx()->prepare(
    "INSERT INTO salary(empID,Amount)
    VALUES (:PersonId, :salary)");

  $stmt->bindParam(':PersonId', $PersonId);
  $stmt->bindParam(':salary', $salary);


  $stmt->execute();
}

        /************************************
        get Employee List
        **************************************/
        function getEmployeeLis()
        {
          $stmt =getCnx()->query("SELECT P.PersonId,p.FirstName,p.LastName,p.Gender,t.TelephoneNo,ar.Role,pl.Address,s.Amount FROM person p
            INNER JOIN salary s on s.empID = p.PersonId
            INNER JOIN uses u on u.PersonId = p.PersonId
            INNER JOIN telephone t on t.TelephoneId = u.TelephoneId
            INNER JOIN lives l on l.PersonId = p.PersonId
            INNER JOIN place pl on pl.PlaceId = l.PlaceId
            INNER JOIN authorisedmembership a on a.PersonId = p.PersonId
            INNER JOIN authorityrole ar on ar.AuthorityRoleId = a.AuthorityRoleId
            INNER JOIN employment e on e.PersonId = p.PersonId WHERE p.FinishDate IS NULL");

          $stmt->execute();
          return $stmt;
        }

        function addEmployee($PersonId,$StartDate){
          $stmt= getCnx()->prepare(
            "INSERT INTO employment(PersonId,StartDate)
            VALUES (:PersonId,  :StartDate)");

          $stmt->bindParam(':PersonId', $PersonId);

          $stmt->bindParam(':StartDate',$StartDate);

          $stmt->execute();
          return getCnx()->lastInsertId();
        }

        function addAthorised($username,$password,$CreatedBy){

          $stmt=getCnx()->prepare("INSERT INTO authorised(UserName, Password,CreatedBy,CreatedDate,Enabled)
            VALUES (:UserName, :Password, :CreatedBy,NOW(),:Enabled)");

          $stmt->bindParam(':CreatedBy', $CreatedBy);
          $stmt->bindValue(':Enabled', "1");
          $stmt->bindParam(':UserName',$username );
          $stmt->bindValue(':Password', $password);

          $stmt->execute();
          return getCnx()->lastInsertId();
        }

        function addAthorisedMembership($PersonId,$athorisedId,$pRole,$StartDate)
        {
          $stmt= getCnx()->prepare("INSERT INTO authorisedmembership(personId,AuthorisedId,AuthorityRoleId,StartDate)
            VALUES (:personId, :AuthorisedId, :AuthorityRoleId,:StartDate)");

          $stmt->bindParam(':personId', $PersonId);
          $stmt->bindParam(':AuthorisedId', $athorisedId);
          $stmt->bindParam(':AuthorityRoleId', $pRole);
          $stmt->bindParam(':StartDate', $StartDate);

          $stmt->execute();

        }
            /************************************
            get Employee Role
            **************************************/
            function getEmployeeRole()
            {
              $stmt = getCnx()->query("SELECT * FROM authorityrole");

              $stmt->execute();
              return $stmt;
            }
            /*************************************
            get person image by person Id
            **************************************/

            // function getPersonImage($personId)
            // {

            //   try {
            //     $stmt = getCnx()->prepare("SELECT ImageName from personimage WHERE PersonId = :personId");

            //     $stmt->bindParam(':personId', $personId);
            //     $stmt->execute();
            //     while ($row = $stmt->fetch()){
            //       if (!empty($row['ImageName']))

            //         return $row["ImageName"];

            //     }
            //     return "101363.jpg";
            //   } catch (PDOException $e){
            //     echo $e;
            //   }
            //   return "Unknown";
            // }


            function LastName($personId)
            {
              try {
                $stmt = getCnx()->prepare("SELECT LastName from person
                  WHERE PersonId=:personId");

                $stmt->bindParam(':personId', $personId);
                $stmt->execute();
                while ($row = $stmt->fetch()){
                  if (!empty($row['LastName']))
                    return $row["LastName"];
                }
                return "No Name";
              } catch (PDOException $e){
                echo $e;
              }
              return "Unknown";
            }

             /******************************
            get Person FirstName By Person Id
            ********************************/
            function FirstName($personId)
            {
              try {
                $stmt = getCnx()->prepare("SELECT FirstName from person
                  WHERE PersonId=:personId");

                $stmt->bindParam(':personId', $personId);
                $stmt->execute();
                while ($row = $stmt->fetch()){
                  if (!empty($row['FirstName']))
                    return $row["FirstName"];
                }
                return "No Name";
              } catch (PDOException $e){
                echo $e;
              }
              return "Unknown";
            }
              /***********************************
          get Staff Salary Detail
          *************************************/
          function getStaffSalary($id)

          {
            $stmt = getCnx()->prepare("SELECT p.FirstName,p.LastName,a.Role,s.Amount FROM person p
              INNER JOIN authorisedmembership am on am.PersonId = p.PersonId
              INNER JOIN authorityrole a on a.AuthorityRoleId = am.AuthorityRoleId
              INNER JOIN salary s on s.empID = p.PersonId WHERE p.PersonId = :id");

            $stmt->bindParam(':id', $id);

            $stmt->execute();
            return $stmt;
          }
            /**********************************
            Pay Salary
            ************************************/
            function paySalary($empId,$amount,$salaryType,$createdBy,$createdDate)
            {
              $cnx = getCnx();
              $stmt = $cnx->prepare("INSERT INTO salarydetails(empId,Amount,SalaryType,createdBy,status,createdDate)

                VALUES (:empId,:amount,:salaryType,:createdBy,1,:createdDate)");

              $stmt->bindParam(':empId', $empId);
              $stmt->bindParam(':amount',$amount);
              $stmt->bindParam(':salaryType', $salaryType);
              $stmt->bindParam(':createdBy', $createdBy);
              $stmt->bindParam(':createdDate', $createdDate);
              $stmt->execute();

            }
            /************************************
            get staff salary  List
            **************************************/
            function getsalaryList($PersonId)
            {
              $stmt =getCnx()->prepare("SELECT * FROM salarydetails  WHERE FinishDate IS NULL AND empId = :id");
              $stmt->bindParam(':id', $PersonId);

              $stmt->execute();
              return $stmt;
            }
            /************************************
            get staff salary  and charge salary
            **************************************/
            function getsalary()
            {
              $stmt =getCnx()->query("SELECT * FROM salary  WHERE FinishDate IS NULL");
              $stmt->execute();
              return $stmt;
            }
            /************************************
            check salary if already charged
            **************************************/
            function checkSalary()
            {
              $stmt =getCnx()->query("SELECT createdDate  FROM salarydetails  WHERE FinishDate IS NULL");
              $stmt->execute();
              return $stmt;
            }
            /**********************************
            Pay Salary
            ************************************/
            function autochargeSalary($empID,$Amount,$salaryType,$createdBy,$createdDate)
            {
              $cnx = getCnx();
              $stmt = $cnx->prepare("INSERT INTO salarydetails(empId,Amount,SalaryType,createdBy,status,createdDate)

                VALUES (:empID,:Amount,:salaryType,:createdBy,0,:createdDate)");

              $stmt->bindParam(':empID', $empID);
              $stmt->bindParam(':Amount',$Amount);
              $stmt->bindParam(':salaryType', $salaryType);
              $stmt->bindParam(':createdBy', $createdBy);
              $stmt->bindParam(':createdDate', $createdDate);
              $stmt->execute();

            }

            /**********************************
                  update person
            ************************************/

                  function updateperson($PersonId,$fName,$lName,$Email,$Gender,$Registerdate)
                  {
                    $cnx = getCnx();
                    $stmt = $cnx->prepare("UPDATE person SET FirstName =:FirstName, LastName =:LastName, Gender=:Gender, Email =:Email, StartDate =:StartDate WHERE PersonId = :PersonId");

                    $stmt->bindParam(':PersonId', $PersonId);
                    $stmt->bindParam(':FirstName',$fName);
                    $stmt->bindParam(':LastName', $lName);
                    $stmt->bindParam(':Email', $Email);
                    $stmt->bindParam(':Gender', $Gender);
                    $stmt->bindParam(':StartDate', $Registerdate);
                    $stmt->execute();

                  }

            /**********************************
                  update Place
            ************************************/

                  function updateplaceByID($PersonId, $Address){

                    $cnx = getCnx();
                    $stmt = $cnx->prepare("UPDATE place AS p
                      INNER JOIN lives AS lives ON p.PlaceId = lives.PersonId
                      SET p.Address = :Address
                      WHERE 
                      lives.PersonId = :PersonId");

                    $stmt->bindParam(':PersonId', $PersonId);
                    $stmt->bindParam(':Address',$Address);
                    $stmt->execute();


                  }

            /**********************************
                  update telephone
            ************************************/

                  function updatephoneByID($PersonId, $Mobile){

                    $cnx = getCnx();
                    $stmt = $cnx->prepare("UPDATE telephone AS T
                      INNER JOIN uses AS uses ON uses.TelephoneId = t.TelephoneId
                      SET T.TelephoneNo = :mobile
                      WHERE 
                      uses.PersonId = :PersonId");

                    $stmt->bindParam(':PersonId', $PersonId);
                    $stmt->bindParam(':mobile',$Mobile);
                    $stmt->execute();

                  }


                  function terminateEmployee($PersonId){

                    $stmt =getCnx()->prepare("UPDATE person SET  FinishDate=NOW() WHERE  PersonId = :PersonId");

                    $stmt->bindParam(':PersonId', $PersonId);
                    $stmt->execute();
                  }

                  ?>
