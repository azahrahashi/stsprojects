<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Customer Sales Report</h4>
														<span>Please customize the report below </span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>No</th>
															<th>Date</th>
															<th>Refrence No</th>
															<th>Tiller </th>
															<th>Grand Total</th>
                              								<th>Paid</th>
                              								<th>Blance</th>
                              								<th>Status</th>
                              								<th width="5%">Action</th>
														</tr>
													</thead>

													<tbody>
														<?php
														require_once 'reportClass.php';
															   $blance =0;
															   $Status ="";
														    if (isset($_GET['pId'])) {
														    	$customerId=$_GET['pId'];
														      $result=getcustomerSal($customerId);
														      $i = 0;
														    	while ($row=$result->fetch()) {
														        $SellsID = $row['SellsID'];
														       
														        $tAmoun = $row['TotalAmount'];
														        $tPaid = $row['TotalPaid'];
														        $blance=$tAmoun-$tPaid ;
														        // waxaan ku diyaarinaa statuska 
														        // 0 waxey la mid tahay paid  1 due 2 return
														        if ($row['Status']==0) {
														        	$Status = 'Paid';
														        }
														        elseif ($row['Status']==1) {
														        	$Status = 'Due';
														        }
														          elseif ($row['Status']==2) {
														        	$Status = 'Return';
														        }


														        $i++;
														    		echo '
														    		<tr>
														    		<td>'; echo $i; echo'</td>
														    		<td>'.$row['SellesDate'].'</td>
														    		<td>SALE/'. date("Y m", strtotime($row['SellesDate'])).' ('.$row['SellsID'].')</td>    	
														    		<td>'.$row['createdBy'].'</td>
														        <td>'.getCurrenceSign().''.$row['TotalAmount'].'.00</td>
														        <td>'.getCurrenceSign().' '.$row['TotalPaid'].'.00</td>
														        <td>'.getCurrenceSign().' '.$blance.'.00</td>
														        <td> '.$Status.'</td>

														        <td>  <a href="#" onclick="ViewSale('.$SellsID.')" class="btn btn-info btn-icon" ><i class="icofont icofont-eye-alt"></i></a></td>';?><?php
														    	}
														    }

														?>
													</tbody>

												</tbody>
											</table>
										</div>
										<!-- Model Print  -->

											<div class="modal fade" id="large-Modal" tabindex="-1" role="dialog">
													<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
													<div class="modal-header">
													<h4 class="modal-title">Modal title</h4>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
													</button>
													</div>
													<div class="modal-body">
											<div class="card">
<div class="row invoice-contact">
<div class="col-md-8">
<div class="invoice-box row">
<div class="col-sm-12">
	
<table class="table table-responsive invoice-table table-borderless">
<tbody>
<tr>
<td><img src="../files/assets/images/logo-blue.png" class="m-b-10" alt=""></td>
</tr>
<tr>
<td>Compney Name</td>
</tr>
<tr>
<td>123 6th St. Melbourne, FL 32904 West Chicago, IL 60185</td>
</tr>
<tr>
<td><a href="mailto:demo@gmail.com" target="_top">demo@gmail.com</a>
</td>
</tr>
<tr>
<td>+91 919-91-91-919</td>
</tr>

 </tbody>
</table>
</div>
</div>
</div>
<div class="col-md-4">
</div>
</div>
<div class="card-block">
<div class="row invoive-info">
<div class="col-md-4 col-xs-12 invoice-client-info">
<h6>Client Information :</h6>
<h6 class="m-0">Josephin Villa</h6>
<p class="m-0 m-t-10">123 6th St. Melbourne, FL 32904 West Chicago, IL 60185</p>
<p class="m-0">(1234) - 567891</p>
<p>demo@xyz.com</p>
</div>
<div class="col-md-4 col-sm-6">
<h6>Order Information :</h6>
<table class="table table-responsive invoice-table invoice-order table-borderless">
<tbody>
<tr>
<th>Date :</th>
<td>November 14</td>
</tr>
<tr>
<th>Status :</th>
<td>
<span class="label label-warning">Pending</span>
</td>
</tr>
<tr>
<th>Id :</th>
<td>
#145698
</td>
</tr>
</tbody>
</table>
</div>
<div class="col-md-4 col-sm-6">
<h6 class="m-b-20">Invoice Number <span>#12398521473</span></h6>
<h6 class="text-uppercase text-primary">Total Due :
<span>$900.00</span>
</h6>
</div>
</div>
 <div class="row">
<div class="col-sm-12">
<div class="table-responsive">
<table class="table  invoice-detail-table">
<thead>
<tr class="thead-default">
<th width="20%">Description</th>
<th>Quantity</th>
<th>Amount</th>
<th>Total</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<p>lorem ipsum dolor sit amet, </p>
</td>
<td>6</td>
<td>$200.00</td>
<td>$1200.00</td>
</tr>
<tr>
<td>
<p>lorem ipsum dolor sit amet, </p>
</td>
<td>7</td>
<td>$100.00</td>
<td>$700.00</td>
</tr>
<tr>
<td>
<p>lorem ipsum dolor sit amet, </p>
</td>
<td>5</td>
<td>$150.00</td>
<td>$750.00</td>
</tr>
</tbody>
</table>
 </div>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<table class="table table-responsive invoice-table invoice-total">
<tbody>
<tr>
<th>Sub Total :</th>
<td>$4725.00</td>
</tr>
<tr>
<th>Taxes (10%) :</th>
<td>$57.00</td>
</tr>
<tr>
<th>Discount (5%) :</th>
<td>$45.00</td>
</tr>
<tr class="text-info">
<td>
<hr>
<h5 class="text-primary">Total :</h5>
</td>
<td>
<hr>
<h5 class="text-primary">$4827.00</h5>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="row">
<div class="col-sm-12">
<h6>Terms And Condition :</h6>
<p>lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
</div>
</div>
</div>
</div>
													</div>
													<div class="modal-footer">
													<div class="buttons" style="width: 100%">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <a href="http://shop.globalsts.net/admin/sales/add_payment/16" class="tip btn btn-primary" title="" data-toggle="modal" data-target="#myModal2" data-original-title="Add Payment">
                                <i class="fa fa-dollar"></i>
                                <span class="hidden-sm hidden-xs">Payment</span>
                            </a><div class="tooltip fade top in" style="top: -32px; left: 28px; display: block;"><div class="tooltip-arrow"></div><div class="tooltip-inner">Add Payment</div></div>
                        </div>
                        <div class="btn-group">
                            <a href="http://shop.globalsts.net/admin/sales/add_delivery/16" class="tip btn btn-primary" title="" data-toggle="modal" data-target="#myModal2" data-original-title="Add Delivery">
                                <i class="fa fa-truck"></i>
                                <span class="hidden-sm hidden-xs">Delivery</span>
                            </a>
                        </div>
                                                <div class="btn-group">
                            <a href="http://shop.globalsts.net/admin/sales/email/16" data-toggle="modal" data-target="#myModal2" class="tip btn btn-primary" title="" data-original-title="Email">
                                <i class="fa fa-envelope-o"></i>
                                <span class="hidden-sm hidden-xs">Email</span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="http://shop.globalsts.net/admin/sales/pdf/16" class="tip btn btn-primary" title="" data-original-title="Download as PDF">
                                <i class="fa fa-download"></i>
                                <span class="hidden-sm hidden-xs">PDF</span>
                            </a>
                        </div>
                                                <div class="btn-group">
                            <a href="http://shop.globalsts.net/admin/sales/edit/16" class="tip btn btn-warning sledit" title="" data-original-title="Edit">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-sm hidden-xs">Edit</span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="#" class="tip btn btn-danger bpo" title="" data-content="<div style='width:150px;'><p>Are you sure?</p><a class='btn btn-danger' href='http://shop.globalsts.net/admin/sales/delete/16'>Yes I'm sure</a> <button class='btn bpo-close'>No</button></div>" data-html="true" data-placement="top" data-original-title="<b>Delete Sale</b>">
                                <i class="fa fa-trash-o"></i>
                                <span class="hidden-sm hidden-xs">Delete</span>
                            </a>
                        </div>
                          <div class="btn-group">
                            <a href="#" class="tip btn btn-danger bpo" title="" data-content="<div style='width:150px;'><p>Are you sure?</p><a class='btn btn-danger' href='http://shop.globalsts.net/admin/sales/delete/16'>Yes I'm sure</a> <button class='btn bpo-close'>No</button></div>" data-html="true" data-placement="top" data-original-title="<b>Delete Sale</b>">
                                <i class="fa fa-trash-o"></i>
                                <span class="hidden-sm hidden-xs">Delete</span>
                            </a>
                        </div>
                                            </div>
                </div>
													</div>
													 </div>
													</div>
													</div>
										<!-- Dhamaadka model print  -->




									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>


<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('#lists').DataTable();
});

function ViewSale(e){

var sID = e;
$("#large-Modal").modal("show");
}
</script>
