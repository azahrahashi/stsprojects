<?php

require_once '../Connection/dbconection.php';



    function addstageType($Name,$StartDate)
      {
        $stmt= getCnx()->prepare("INSERT INTO stagetypes (Name,StartDate)
              VALUES (:Name, :StartDate)");

              $stmt->bindParam(':Name', $Name);
              $stmt->bindParam(':StartDate', $StartDate);

              $stmt->execute();

           // return getCnx()->lastInsertId();
      }

// Get All StageTypes Funtion
    function GetAllStagesType(){

        $stmt=getCnx()->query("SELECT * FROM stagetypes
          WHERE FinishDate IS NULL");

                return $stmt;
      }



     function updateStageType($Id,$Name,$StartDate)
      {
        $stmt =getCnx()->prepare("UPDATE stagetypes SET  Name=:Name,StartDate=:StartDate WHERE  Id = :Id");

      	  $stmt->bindParam(':Id', $Id);
      	  $stmt->bindParam(':Name', $Name);
      	  $stmt->bindParam(':StartDate', $StartDate);
      	  $stmt->execute();
      }


       function terminateStageType($Id)
      {
        $stmt =getCnx()->prepare("UPDATE stagetypes SET  FinishDate=NOW() WHERE  Id = :Id");

          $stmt->bindParam(':Id', $Id);
          $stmt->execute();
      }


 ?>
