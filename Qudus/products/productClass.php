<?php 

require_once '../Connection/dbconection.php';
          // Get All Products Function
    function getstAllprducts(){

        $stmt=getCnx()->query("SELECT * FROM products 
          WHERE FinishDate IS NULL");

                return $stmt;
      }
              
                 // Add Function
    function addProduct($Name,$Unit,$UnitPrice,$SalePrice,$StockLevel,$StartDate)
      {
        $stmt= getCnx()->prepare("INSERT INTO products(Name,Unit,UnitPrice,SalePrice,StockLevel,StartDate)
              VALUES ( :Name, :Unit, :UnitPrice, :SalePrice, :StockLevel, :StartDate)");

              $stmt->bindParam(':Name', $Name);
              $stmt->bindParam(':Unit', $Unit);
              $stmt->bindParam(':UnitPrice', $UnitPrice);
              $stmt->bindParam(':SalePrice', $SalePrice);
              $stmt->bindParam(':StockLevel', $StockLevel);
              $stmt->bindParam(':StartDate', $StartDate);
              $stmt->execute();

           // return getCnx()->lastInsertId();
      }

             // Update Funtion
   function updateProducts($Id,$Name,$Unit,$UnitPrice,$SalePrice,$StockLevel,$StartDate)
      {
        $stmt =getCnx()->prepare(" UPDATE products SET   Name=:Name, Unit=:Unit, UnitPrice=:UnitPrice, SalePrice=:SalePrice, StockLevel=:StockLevel, StartDate=:StartDate WHERE  Id = :Id");

      	  $stmt->bindParam(':Id', $Id);
      	  $stmt->bindParam(':Name', $Name);
          $stmt->bindParam(':Unit', $Unit);
          $stmt->bindParam(':UnitPrice', $UnitPrice);
          $stmt->bindParam(':SalePrice', $SalePrice);
          $stmt->bindParam(':StockLevel', $StockLevel);
          $stmt->bindParam(':StartDate', $StartDate);
      	  $stmt->execute();
      }
         
      function terminateProducts($Id)
      {
        $stmt =getCnx()->prepare("UPDATE products SET  FinishDate=NOW() WHERE  Id = :Id");

          $stmt->bindParam(':Id', $Id);
          $stmt->execute();
      }




 ?>