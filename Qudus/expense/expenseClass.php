<?php 

require_once '../Connection/dbconection.php';
          // Get All salary Function

function getAllexpense(){

  $stmt=getCnx()->query("SELECT * FROM expenses WHERE FinishaDate IS NULL  ORDER BY expenses.ExpensesId DESC ");
  return $stmt;
}

                 // Add Function
function addExpense($Amount,$Reason,$Status,$StartDate)
{
  $stmt= getCnx()->prepare("INSERT INTO expenses (Amount,Reason,Status,StartDate)
    VALUES ( :Amount, :Reason, :Status, :StartDate)");
   
  $stmt->bindParam(':Amount', $Amount);
  $stmt->bindParam(':Reason', $Reason);
  $stmt->bindParam(':Status', $Status);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->execute();

}

             // Update Funtion
function updateExpense($ExpensesId,$Amount,$Reason,$Status,$StartDate)
{

  $stmt =getCnx()->prepare(" UPDATE expenses SET  Amount=:Amount, Reason=:Reason, Status=:Status, StartDate=:StartDate WHERE  ExpensesId = :ExpensesId");

  $stmt->bindParam(':ExpensesId', $ExpensesId);
  $stmt->bindParam(':Amount', $Amount);
  $stmt->bindParam(':Reason', $Reason);
  $stmt->bindParam(':Status', $Status);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->execute();
}

function terminateExpense($ExpensesId)
{
  $stmt =getCnx()->prepare("UPDATE expenses SET  FinishaDate=NOW() WHERE  ExpensesId = :ExpensesId");

  $stmt->bindParam(':ExpensesId', $ExpensesId);
  $stmt->execute();
}




?>