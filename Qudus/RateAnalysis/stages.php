<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Basic Form Inputs</h4>
														<!-- 	<span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span> -->
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">
														<li class="breadcrumb-item">
															<a href="index.html"> <i class="feather icon-home"></i> </a>
														</li>
														<li class="breadcrumb-item"><a href="#!">Form-Components</a>
														</li>
														<li class="breadcrumb-item"><a href="#!">Form-Elements-Advance</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div>
										<div class="card">
											<div class="card-header">
												<h5>Stage Types</h5>
											</div>
											<div class="card-block">
												<div class="row">
													<div class="col-sm-12 col-md-12 col-xl-12 m-b-30 col-md-offset-4">
														<h4 class="sub-title">Stage Types </h4>
														<div class="form-radio">
															<?php
															require_once '../Classes/stageClass.php';
															$result = getStageTypes();
															if (!empty($result)){
																while ($row=$result->fetch())  {
																	$id =$row['Id'];
																	echo '
																	<div class="radio radio-inline">
																	<label>
																	<input type="radio" name="radio" id="r'.$id.'"  onclick="setForm(this,'.$id.');">
																	<i class="helper"></i>'.$row['Name'].'
																	</label>
																	</div>
																	';
																}
															}
															?>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="card" id="rateAnalysis">
											<div class="card-header">
												<h5 >Stages</h5>
												<!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
											</div>
											<div class="card-block">
												<div class="row">
													<div class="col-sm-12 col-xl-12 m-b-30">
														<h4 class="sub-title" id="boxHeader"></h4>
														<div class="border-checkbox-section" id="stageList">

														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12 col-xl-12 m-b-30 " style="overflow-x:auto;">
														<table class="table table-framed table-bordered col-sm-12 col-xl-8  col-xl-offset-2 m-b-30">
															<thead>
																<th>Item Description</th>
																<th>Unit</th>
																<th>Rate USD</th>
															</thead>
															<tbody id="description">

															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="card-block">
												<div class="row">
													<div class="col-sm-12 col-xl-12 m-b-30">
														<h4 class="sub-title uppercase">sample rate analysis</h4>
														<div class="border-checkbox-section" id="">

														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-12 col-xl-12" id="analysisButton" style="overflow-x:auto;">
														<button class="btn btn-grd-success float-right"onclick="newRow();">Add new Row</button>
														<table class="table table-framed table-bordered col-sm-12 col-xl-12">
															<thead>
																<th>Item Description</th>
																<th>Unit</th>
																<th>Qty</th>
																<th>Rate USD</th>
																<th>Amount USD</th>
															</thead>
															<tbody id="descriptionAnalysis" class="col-xl-12">

															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<button onclick="save();">Save</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php require_once '../Include/script.php'; ?>
</body>
</html><script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#rateAnalysis').hide();
	});
	var cb='';
	function save() {
		if($('#r1').is(':checked'))
			alert(999+','+$('#r1').val());
	}

	function setForm(e,id){
		$('#boxHeader').empty();
		$('#description').empty();
		$('#stageList').empty();

		var e =$(e).val();
		if (id==1) {
			$('#boxHeader').text('EXCAVATION');
			$('#description').append('<td>Excavation for <input style="width:5%"> columns foundation up to <input style="width:5%"> m</br> depth including sorting out and stacking of useful</br>materials and disposing of excavated stuff up to <input style="width:5%"> m</br> lead for loose or soft soil </td>'+
				'<td><input></td>'+
				'<td><input></td>');

			$.post("../ajaxStage/stageListByType.php",{
				typeId: id
			}, function (data, status) {
				console.log(data);
				var j=data.length-1;
				for (var i = 0; i <data.length ; i++) {
					$('#stageList').append(
						'<div class="border-checkbox-group border-checkbox-group-info">'+
						'<input class="border-checkbox" type="checkbox" id="cb'+i+'">'+
						'<label class="border-checkbox-label" for="cb'+i+'">'+data[i].Name+'</label>'+
						'</div>');
				}
					if ($('#cb'+j).is(':checked')) {
						// cb=data[j].Name;
						cb='alert';
						j--;
					}

			});
		}
		else if (id==2) {
			$('#boxHeader').text('CONCRETE RCC');
			$('#description').append('<td>Providing and laying controlled cement concrete <input style="width:5%"> m</br> and curing compelete excluding cost of formwork and </br>reinforcement for reinforced work in slabs, landings,</br> sleves balconies lintels, beams, girders cantilever up to </br>floor two level </td>'+
				'<td><input></td>'+
				'<td><input></td>');

			$.post("../ajaxStage/stageListByType.php",{
				typeId: id
			}, function (data, status) {
				console.log(data);
				for (var i = 0; i <data.length ; i++) {
					$('#stageList').append(
						'<div class="border-checkbox-group border-checkbox-group-info">'+
						'<input class="border-checkbox" type="checkbox" id="cb'+i+'">'+
						'<label class="border-checkbox-label" for="cb'+i+'">'+data[i].Name+'</label>'+
						'</div>');

				}

			});
		}

		$('#rateAnalysis').show();
	}
	function newRow() {
		$('#descriptionAnalysis').append('<tr>'+
			'<td><input ></input></td>'+
			'<td><input style="width:50%"></input></td>'+
			'<td><input style="width:50%"></input></td>'+
			'<td><input style="width:50%"></input></td>'+
			'<td><input style="width:50%"></input></td>'+
			'</tr>');
	}
				// function newMatRow() {
				// 	$('#descriptionAnalysis').append('<tr>'+
				// 		'<td><select>');
				// 	$.post("../ajaxStage/stageListByType.php",{
				// 		typeId: 1
				// 	}, function (data, status) {
				// 		console.log(data);
				// 		for (var i = 0; i <data.length ; i++) {
				// 			$('#descriptionAnalysis').append('<option value="'+data[i].Id+'">'+data[i].Name+'</option>');
				// 		}
				// 		$('#descriptionAnalysis').append(
				// 			'</select></td>'+
				// 			'<td><input></input></td>'+
				// 			'<td><input></input></td>'+
				// 			'<td><input></input></td>'+
				// 			'<td><input></input></td>'+
				// 			'</tr>');
				// 	});
				// }
			</script>
			<style>
			input {
				border:0px;
				border-bottom:2px solid #999;
			}
		</style>