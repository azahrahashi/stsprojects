<?php 

require_once'../Connection/dbconection.php';

  function getStageTypes()
  {
     $stmt =getCnx()->prepare("SELECT st.* FROM stagetypes st WHERE st.FinishDate is null");
     $stmt->execute();
     return $stmt;
  }
  function getSamrateanalys($ProjectId)
  {
     $stmt =getCnx()->prepare("SELECT st.*,ss.Name as stagetype,s.Name as stage FROM samrateanalys st 
      INNER JOIN stagetypes ss ON ss.Id=st.StageTypeId
      INNER JOIN stages s ON s.Id=st.StageId
      WHERE st.ProjectId=$ProjectId AND st.FinishDate is null");
     $stmt->execute();
     return $stmt;
  }
  function getSamrateanalysDet($Id)
  {
     $stmt =getCnx()->prepare("SELECT st.* FROM samrateanalysdetails st 
      INNER JOIN samrateanalys ss ON ss.Id=st.samrateanalysId
      WHERE st.samrateanalysId=$Id AND st.FinishDate is null");
     $stmt->execute();
     return $stmt;
  }
  function getStagesByTypeId($id)
  {
     $stmt =getCnx()->prepare("SELECT s.* FROM stages s WHERE s.FinishDate is null AND s.StageTypeId = :id");
     
     $stmt->bindParam(':id', $id);
     $stmt->execute();
     return $stmt;
  }
  function addSampleRate($ProjectId,$stageType,$stage,$itemDesc,$unitDesc,$totalRate)
  {
    
     $cnx = getCnx();
       $stmt = $cnx->prepare("INSERT INTO samrateanalys(ProjectId,StageTypeId,StageId,ItemDescription,Unit,ReateUSD,StartDate)
            VALUES (:ProjectId,:stageType,:stage,:itemDesc,:unitDesc,:totalRate,NOW())");
            $stmt->bindParam(':ProjectId', $ProjectId);
            $stmt->bindParam(':stageType', $stageType);
            $stmt->bindParam(':stage', $stage);
            $stmt->bindParam(':itemDesc', $itemDesc);
            $stmt->bindParam(':unitDesc', $unitDesc);
            $stmt->bindParam(':totalRate', $totalRate);

            $stmt->execute();
             return getCnx()->lastInsertId();
  }


function addSampleRateDetail($SampleRateId,$Description,$Unit,$Qty,$ReateUSD,$AmountUSD)
{
     $cnx = getCnx();
       $stmt = $cnx->prepare("INSERT INTO samrateanalysdetails(samrateanalysId,Description,Unit,Qty,ReateUSD,AmountUSD,StartDate)
            VALUES (:SampleRateId,:Description,:Unit,:Qty,:ReateUSD,:AmountUSD,NOW())");
            $stmt->bindParam(':SampleRateId', $SampleRateId);
            $stmt->bindParam(':Description', $Description);
            $stmt->bindParam(':Unit', $Unit);
            $stmt->bindParam(':Qty', $Qty);
            $stmt->bindParam(':ReateUSD', $ReateUSD);
            $stmt->bindParam(':AmountUSD', $AmountUSD);

            $stmt->execute();
}

 ?>