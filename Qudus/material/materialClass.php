<?php
require_once'../Connection/dbconection.php';

    /**********************************
            Add New Project            
    ************************************/
function    addMaterial($mName,$mUnit,$mUnitPrice,$mStockLevel,$mstartDate)
{
    $cnx = getCnx();
    $stmt = $cnx->prepare("INSERT INTO material(Name,Unit,UnitPrice,StockLevel,StartDate) 
            VALUES (:mName,:mUnit,:mUnitPrice,:mStockLevel,:mstartDate)");

       
        $stmt->bindParam(':mName', $mName);
        $stmt->bindParam(':mUnit', $mUnit);
        $stmt->bindParam(':mUnitPrice', $mUnitPrice);
        $stmt->bindParam(':mStockLevel', $mStockLevel);
        $stmt->bindParam(':mstartDate',$mstartDate);
        $stmt->execute();

}
    /**************************************
            Update Project 
    ***************************************/ 
  function    updatematerialId($Id,$mName,$mUnit,$mUnitPrice,$mStockLevel,$mstartDate){
        $stmt = getCnx()->prepare("UPDATE material SET Name = :mName ,Unit=:mUnit,UnitPrice=:mUnitPrice,StockLevel=:mStockLevel,  StartDate  = :mstartDate WHERE  Id = :Id");

        $stmt->bindParam(':Id', $Id);
        $stmt->bindParam(':mName', $mName);
        $stmt->bindParam(':mUnit', $mUnit);
        $stmt->bindParam(':mUnitPrice', $mUnitPrice);
        $stmt->bindParam(':mStockLevel', $mStockLevel);
        $stmt->bindParam(':mstartDate',$mstartDate);
        
        $stmt->execute();
        return $stmt;


  }
     /*********************************
           Terminate Income
     **********************************/
  function terminateMaterial($Id)
    {
        $stmt = getCnx()->prepare("UPDATE material SET FinishDate = NOW()  WHERE Id = :Id");
        $stmt->bindParam(':Id', $Id);
        $stmt->execute();
        return $stmt;

    } 
     /***********************************
         get Income Detail
     *************************************/ 
function getMaterialDetail($Id)

{
       $stmt = getCnx()->prepare("SELECT * FROM material WHERE   Id = :Id");
       $stmt->bindParam(':Id', $Id);
        $stmt->execute();
        return $stmt;
}

    /************************************
         get Suppliers List
    **************************************/ 
  function getMaterialList()
  {
     $stmt =getCnx()->query("SELECT * FROM material WHERE  FinishDate IS NULL");
     $stmt->execute();
     return $stmt;
  }

?>