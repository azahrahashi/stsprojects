<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

<div class="pcoded-content">
<div class="pcoded-inner-content">
<div class="main-body">
<div class="page-wrapper">
	<div class="page-header">
	
	</div>
	<div class="card">
		<div class="card-header">
			<h5>Add Employee</h5>
			
		</div>
		<div class="card">
			
			<div class="card-block">
				<fieldset>
					<?php
            $fName =  $lName = $gender = $email = $phone = $pRole= $salary = $address = $pImage= "";
            $fNameErr = $lNameErr = $emailErr = $pRole = $addressErr =$salaryErr= $pImageErr ="" ;
         	$error = false;
              if ($_SERVER["REQUEST_METHOD"] == "POST")
              {
                     require_once'employeeClass.php';
                      require_once '../common/regExp.php';
                      require_once '../common/validation.php';// require validation file then validate all form input


                 $fName = validateField($_POST['fName'], rTwoWords(), $fNameErr, $error, "Only Latin characters", true);
                 $lName = validateField($_POST['lName'], rTwoWords(), $lNameErr, $error, "Only Latin characters", true);
                 $pRole = validateField($_POST['pRole'],  rTwoWords(), $pRoleErr, $error, "Choose Branch", true);
                 $email = test_input($_POST["email"]);
                 $salary = test_input($_POST["salary"]);
                 $address = test_input($_POST["address"]);
                 $gender = test_input($_POST["gender"]);
                  $phone = test_input($_POST["phone"]);
                  // Image Validations
                  $imgFile = $_FILES['pImage']['name'];
                  $tmp_dir = $_FILES['pImage']['tmp_name'];
                  $imgSize = $_FILES['pImage']['size'];
                  if(empty($imgFile)){
                  //$pImageErr = "Please Select Image File.";
                 //$error = true;
                }else{
                  $upload_dir = 'employeeImage/'; // upload directory
                  $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
                  // valid image extensions
                  $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
                  // rename uploading image
                  $pImage = rand(1000,1000000).".".$imgExt;
                  // allow valid image file formats
                  if(in_array($imgExt, $valid_extensions)){
                    // Check file size '5MB'
                    if($imgSize < 5000000){
                      move_uploaded_file($tmp_dir,$upload_dir.$pImage);
                    }
                else{
                  $pImageErr = "Sorry, your file is too large.";
                  $error = true;
                  }
                    }
                  else{
                    $pImageErr = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $error = true;
                   }
                   }

              }
              if ($_SERVER["REQUEST_METHOD"] == "POST" && $error == false)
              {  
			    try{  
			              $StartDate = date("Y/m/d");
			              $CreatedBy = "Abdinoor";
			              $cnx= getCnx();
			              $cnx->beginTransaction();
			              $PersonId= addPerson($fName,$lName,$gender,$email);
			              addPersonImage($PersonId,$pImage,$StartDate);                                        
			              $PlaceId=addPlace($address);
			              addLives($PersonId,$PlaceId);
			              $TelephoneId= addTelephone($phone);
			              addUses($PersonId,$TelephoneId,$StartDate);
			              addSalary($PersonId,$salary);
			              $EmploymentId =addEmployee($PersonId,$StartDate); 
			              $username ="STS00".$PersonId;
			              $password = 123456;
			              $athorisedId = addAthorised($username,$password,$CreatedBy);
			              addAthorisedMembership( $PersonId,$athorisedId,$pRole,$StartDate);

			        
			        
			          $cnx->commit();
			      } catch (PDOException $e) {
			        $cnx->rollBack();
			        echo "there is SQL Problame $e";
			      }
               ?>
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" />
              <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
              <script>

                swal({
                  title:"Good job!",
                  text:"You have saved Successfully!",
                  type:"success"},
                  function () {
                    window.location.replace("employeeList.php");
                  }
                );
              //header("location:ProjectList.php");
              </script>
                  <?php
              }


              else
              {
              ?>
				 <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" enctype="multipart/form-data" class="well form-horizontal">
				<div class="row">
				           <div class="form-group col-md-6">
								<label class="col-sm-2 col-form-label">First Name</label>
								<div class="col-sm-10">
								<input type="text" class="form-control" name="fName" placeholder="First Name">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="col-sm-2 col-form-label">Last Name</label>
								<div class="col-sm-10">
								<input type="text" class="form-control" name="lName" placeholder="Last Name">
								</div>
							</div> 
							<div class="form-group col-md-6">
								<label class="col-sm-2 col-form-label">Gender</label>
								<div class="col-sm-10">
									<label>
										<input type="radio" name="gender" value="Male" checked="checked">
										<i class="helper"> </i> Male 
										</label>

										<label>
										<input type="radio" name="gender" value="Female">
										<i class="helper"> </i> Female 
									</label>
								</div>
							</div> 
							<div class="form-group col-md-6">
								<label class="col-sm-2 col-form-label">email</label>
								<div class="col-sm-10">
								<input type="email" class="form-control" name="email" placeholder="email">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="col-sm-2 col-form-label">Telephone</label>
								<div class="col-sm-10">
								<input type="text" class="form-control" name="phone" placeholder="Telephone">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="col-sm-2 col-form-label">Address</label>
								<div class="col-sm-10">
								<input type="text" class="form-control" name="address" placeholder="Address">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label class="col-sm-2 col-form-label">Photo</label>
								<div class="col-sm-10">
								<input type="file" class="form-control"  name="pImage">
								<span class="error"><?php echo $pImageErr;?></span>
								</div>
							</div> 

							<div class="form-group col-md-6">
								<div class="row">
									<div class="form-group col-md-6">
										<label class="col-sm-12 col-form-label">Designation</label>
										<div class="col-sm-10">
										 <select class="custom-select form-control col-12" name="pRole">
										 	  <?php
					                          echo '<option value=""> Select Category</option>' ;
					                          require_once'employeeClass.php';
					                          $result = getEmployeeRole();
					                          if (!empty($result))
					                          {
					                            
					                            while ($row=$result->fetch()) {
					                           echo '<option value = ' . $row["AuthorityRoleId"] .'>' . $row["Role"] . '</option>';
					                          }
					                          }
					                        ?>			                    
					                    </select>
										</div>
									</div> 
									<div class="form-group col-md-6">
										<label class="col-sm-12 col-form-label">Salary</label>
											<div class="col-sm-10">
												<input type="number" class="form-control" name="salary" placeholder="0.00">
												<span class="error"><?php echo $salaryErr;?></span>
											</div>
									</div>

								</div>
							</div> 
								
							
			  				<div class="modal-footer float-right">
								<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
								<input type="submit" class="btn btn-success btn-round">
								</div>
                         

                    
                  
				
				</div>
				 </form>
				  <?php } ?>
  </fieldset>
			</div>
		</div>
	</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php require_once '../Include/script.php'; ?>
			</body>
			</html>
			<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
			