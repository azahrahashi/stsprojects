<?php 

require_once '../Connection/dbconection.php';
          // Get All salary Function

function getAllCustomerName(){
  $stmt=getCnx()->query("SELECT customers.Name FROM `customers` WHERE FinishDate is NULL");
  return $stmt;
}
function getAllCustomers(){
  $stmt=getCnx()->query("SELECT * FROM customers WHERE FinishDate IS NULL");
  return $stmt;
}

                 // Add Function
function addCustomer($Name,$Gender,$Telephone,$Address,$Email,$StartDate)
{
  $stmt= getCnx()->prepare("INSERT INTO customers (Name,Gender,Telephone,Address,Email,StartDate)
    VALUES ( :Name, :Gender, :Telephone, :Address, :Email, :StartDate)");
   
  $stmt->bindParam(':Name', $Name);
  $stmt->bindParam(':Gender', $Gender);
  $stmt->bindParam(':Telephone', $Telephone);
  $stmt->bindParam(':Address', $Address);
  $stmt->bindParam(':Email', $Email);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->execute();

}

             // Update Funtion
function updateCustomer($CustomerId,$Name,$Gender,$Telephone,$Address,$Email,$StartDate)
{

  $stmt =getCnx()->prepare(" UPDATE customers SET  Name=:Name, Gender=:Gender, Telephone=:Telephone, Address=:Address, Email=:Email, StartDate=:StartDate  WHERE  CustomerId = :CustomerId");

  $stmt->bindParam(':CustomerId', $CustomerId);
  $stmt->bindParam(':Name', $Name);
  $stmt->bindParam(':Gender', $Gender);
  $stmt->bindParam(':Telephone', $Telephone);
  $stmt->bindParam(':Address', $Address);
  $stmt->bindParam(':Email', $Email);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->execute();
}

function terminateCustomer($CustomerId)
{
  $stmt =getCnx()->prepare("UPDATE customers SET  FinishDate=NOW() WHERE  CustomerId = :CustomerId");

  $stmt->bindParam(':CustomerId', $CustomerId);
  $stmt->execute();
}




?>