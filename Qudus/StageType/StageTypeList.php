<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Foundation Types</h4>
														<span>This Is The <code>Foundation Types</code> Managment Page.</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											<div>
												<button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#aStageType"><i class="fa fa-plus"></i> Add</button>
											</div>
											<br><br>
											<br>
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Name</th>
															<th>Startdate</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<?php
													require_once 'StageTypeClass.php';
													$result=GetAllStagesType();
													while ($row=$result->fetch()) {
														echo '
														<tr>
														<td>'.$row['Id'].'</td>
														<td>'.$row['Name'].'</td>
														<td>'.date("d-m-Y",strtotime($row['StartDate'])).'</td>
														<td>';?>
														<button class="btn btn-success"
														onClick="setStageType(<?php echo $row['Id'];?>,'<?php echo $row['Name'];?>','<?php echo $row['StartDate'];?>')"> Update </button>
														<?php
														echo '<button class="btn btn-warning" onClick="RemoveStageType('.$row['Id'].')" ><i class="fa fa-trush">Remove</button></td>
														</tr>';
													}
													?>
													</tbody>
													</table>
												</div>

												<!-- Add Stagetype Model -->

											<div class="modal fade" id="aStageType" tabindex="-1" role="dialog">
													<div class="modal-dialog" role="document">
														<form method="post" id="user_form" enctype="multipart/form-data">
															<div class="modal-content">
																<div class="modal-header">
																	<h4 class="modal-title">Add Foundation Type</h4>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">&times;</span>
																	</button>
																</div>
																<div class="modal-body">
																	<h5>Foundation Type</h5>
															<br><!--
															<input type="hidden" id="Id" name="Id" value=""  class="form-control"> -->
															<label>Enter Foundation Name</label>
															<input type="text" name="StagetypeName" id="StagetypeName" class="form-control" />
															<br>
															<!-- <label>Date</label> -->
															<input type="hidden" name="StagetypeDate" id="StagetypeDate" class="form-control" value="<?php echo date("Y-m-d") ?>" />
															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="addStageType()">Save changes</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										 </div>
										<!-- End Add Stagetype Model -->


										<!-- Update Model -->

										<div class="modal fade" id="uStageType" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
													 <div class="modal-header">
														<h4 class="modal-title"> Update Foundation Type</h4>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<h5> Foundation Type</h5>
															<br>
															<input type="hidden" id="Id" name="Id" value=""  class="form-control">
															<label>Enter Type Name</label>
															<input type="text" name="uStagetypeName" id="uStagetypeName" class="form-control" />
															<br>
															<!-- <label>Date</label> -->
															<input type="date" name="uStagetypeDate" id="uStagetypeDate" class="form-control" value="<?php echo date("Y-m-d") ?>" />
															<br>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="updateStageType()">Save changes</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- End Update Model -->


									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
			<?php require_once '../Include/script.php'; ?>
		</body>
		</html>

		<script type="text/javascript">

			             function addStageType() {

								var Name = $("#StagetypeName").val();
								var StartDate = $("#StagetypeDate").val();
								// alert(+Name+','+StageType+','+StartDate);
											$('.danger').remove();

								if (Name == "") {
									$("#StagetypeName").after('<p class="danger"> Name field is required!</p>');
								}
								else{

								 $.post("addStageType.php", {
									  	Name: Name,
									  	StartDate: StartDate,
									  	Id:0
									  },
									  function (data, status) {
		                                  // alert("Successfully Registered");
		                                    window.location.reload();
		                                	});
                                         }
										    }

			             function setStageType(Id,Name,StartDate){

								   // alert(Id+','+Name+','+StartDate);
								 $("#Id").val(Id);
								 $("#uStagetypeName").val(Name);
								 $("#uStagetypeDate").val(StartDate);
								 $("#uStageType").modal("show");

								}
					     function updateStageType(){

								var Id=$("#Id").val();
								var Name=$("#uStagetypeName").val();
								var StartDate=$("#uStagetypeDate").val();

							    // alert(Id+','+Name+',,'+StartDate+',');


							  	$.post("addStageType.php",{
							  		Name:Name,
							  		StartDate:StartDate,
							  		Id:Id
							  	}, function (data, status) {
							  		// alert("Successfully Updated");
							  		window.location.reload();
							  	});

							  }

					    function RemoveStageType(Id) {

                                	$.post("RemoveStageType.php",{
                                		Id:Id
                                	},
                                	function (data, status) {
                                		// alert("Successfully Removed");
                                		window.location.reload();
                                	});

                                }

							</script>

							 <style type="text/css">
                            	.danger{
                            		color:red;
                            	}
                            </style>

