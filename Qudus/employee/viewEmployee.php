<?php require_once 'employeeClass.php'; ?>
<?php
if (isset($_POST['updateEm'])) {
	if($_POST['PersonId']>0){
		$fName=$_POST['fName'];
		$lName = $_POST['lName'];
		$Email=$_POST['Email'];
		$Gender=$_POST['Gender'];
		$Mobile=$_POST['Mobile'];
		$Registerdate=$_POST['Registerdate'];
		$Address=$_POST['Address'];
		$PersonId = $_POST['PersonId'];
		
		updateperson($PersonId,$fName,$lName,$Email,$Gender,$Registerdate);
		updateplaceByID($PersonId, $Address);
		updatephoneByID($PersonId, $Mobile);
		header('Location: viewEmployee?pId='.$PersonId.'');
	}
}
?>


<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>
					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<?php
									$PersonId =0;

									require_once 'employeeClass.php';
									//send projectId
									if (isset($_GET['pId'])){
										$PersonId=$_GET['pId'];
									}

									?>


									<div class="row">
										<div class="col-lg-12">
											<div class="cover-profile">
												<div class="profile-bg-img">
													<img class="profile-bg-img img-fluid" src="../files/assets/images/user-profile/bg-img1.jpg" alt="bg-img">
													<div class="card-block user-info">
														<div class="col-md-12">
															<div class="media-left">
																<a href="#" class="profile-image">
																	<img class="user-img img-radius" src="employeeImage/<?php   echo getPersonImage($PersonId);?>" width="80px;" alt="user-img">
																</a>
															</div>
															<div class="media-body row">
																<div class="col-lg-12">
																	<div class="user-title">
																		<h2><?php echo getPersonName($PersonId);?></h2>
																		<span class="text-white"><?php   echo getPersonImage($PersonId);?></span>
																	</div>
																</div>
																<div>
																	<div class="pull-right cover-btn">
																		<!-- <button type="button" class="btn btn-primary m-r-10 m-b-5"><i class="icofont icofont-plus"></i> Follow</button> -->
																		<button type="button" class="btn btn-primary"><i class="icofont icofont-ui-messaging"></i> Message</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<!--employeeprofile  -->
									<div class="row">
										<div class="col-lg-12">

											<div class="tab-header card">
												<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist" id="mytab">
													<li class="nav-item">
														<a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
														<div class="slide"></div>
													</li>
													<li class="nav-item">
														<a class="nav-link" data-toggle="tab" href="#binfo" role="tab">Salary</a>
														<div class="slide"></div>
													</li>
													<li class="nav-item">
														<a class="nav-link" data-toggle="tab" href="#contacts" role="tab">Notice & Massages</a>
														<div class="slide"></div>
													</li>
													<!-- <li class="nav-item">
													<a class="nav-link" data-toggle="tab" href="#review" role="tab">Reviews</a>
													<div class="slide"></div>
												</li> -->
											</ul>
										</div>


										<div class="tab-content">
											<!-- halkaan waxaa ka ilaawan doono person info  -->
											<div class="tab-pane active" id="personal" role="tabpanel">
												<div class="card">
													<div class="card-header">
														<h5 class="card-header-text">Employee Profile</h5>
														<button id="edit-btn" type="button" class="btn btn-sm btn-primary waves-effect waves-light f-right">
															<i class="icofont icofont-edit"></i>
														</button>
													</div>
													<div class="card-block">
														<div class="view-info">
															<div class="row">
																<div class="col-lg-12">
																	<div class="general-info">
																		<div class="row">
																			<div class="col-lg-12 col-xl-6">
																				<div class="table-responsive">
																					<table class="table m-0">
																						<tbody>
																							<tr>
																								<th scope="row">Full Name</th>
																								<td><?php echo getPersonName($PersonId);?></td>
																							</tr>
																							<tr>
																								<th scope="row">Gender</th>
																								<td><?php echo getGender($PersonId);?></td>
																							</tr>
																							<tr>
																								<th scope="row">Register Date</th>
																								<td><?php echo getPersonRegistredDate($PersonId);?></td>
																							</tr>


																						</tbody>
																					</table>
																				</div>
																			</div>

																			<div class="col-lg-12 col-xl-6">
																				<div class="table-responsive">
																					<table class="table">
																						<tbody>
																							<tr>
																								<th scope="row">Email</th>
																								<td><a href="#!"><?php echo getPersonEmail($PersonId);?></a></td>
																							</tr>
																							<tr>
																								<th scope="row">Mobile Number</th>
																								<td><?php echo getPesonMobile($PersonId);?></td>
																							</tr>
																							<tr>
																								<th scope="row">Address</th>
																								<td><?php echo getPersonAddress($PersonId);?></td>
																							</tr>
																						</tbody>
																					</table>
																				</div>
																			</div>

																		</div>

																	</div>

																</div>

															</div>

														</div>
														<!-- halkaan waxaan ku soo saari doonaa forka Edit Employeee  -->
														<form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST"
															enctype="multipart/form-data" class="well form-horizontal">
															<div class="edit-info" style="display: none;">
																<div class="row">
																	<div class="col-lg-12">
																		<div class="general-info">
																			<div class="row">
																				<div class="col-lg-12">
																					<table class="table">
																						<tbody>
																							<tr>
																								<td>
																									<label for="">First name</label>
																									<div class="input-group">
																										<input type="hidden" name="PersonId" value="<?php echo $PersonId;?>">
																										
																										<input type="text" class="form-control" name="fName" placeholder="First Name" value="<?php echo FirstName($PersonId);?>">
																									</div>
																								</td>
																								<td>
																									<label for="">Last name</label>
																									<div class="input-group">
																										<input type="text" class="form-control" name="lName" placeholder="First Name" value="<?php echo LastName($PersonId);?>">
																									</div>
																								</td>
																								
																							</tr>
																							<tr>
																								<td>
																									<label for="">Email</label>
																									<div class="input-group">
																										<input type="text" class="form-control" name="Email" placeholder="First Name" value="<?php echo getPersonEmail($PersonId);?>">
																									</div>
																								</td>
																							</tr>
																							<tr>
																								<td>
																									<label for="">Gender</label>
																									<div class="input-group">
																										<select class="form-control" name="Gender" >
																											<option value="0">Select Gender</option>
																											<?php
																											$gender = getGender($PersonId);
																											if ($gender == "Male") {
																												echo "<option value=".getGender($PersonId)." selected>".getGender($PersonId)."</option>";
																												echo "<option value='Female'>Female</option>";
																											}
																											if ($gender == "Female") {
																												echo "<option value=".getGender($PersonId)." selected>".getGender($PersonId)."</option>";
																												echo "<option value='Male'>Male</option>";
																											}
																											?>
																										</select>
																									</div>
																								</td>
																								<td>
																									<label for="">Phone Number</label>
																									<div class="input-group">
																										<input type="text" class="form-control" name="Mobile" placeholder="First Name" value="<?php echo getPesonMobile($PersonId);?>">
																									</div>
																								</td>
																							</tr>

																							<tr>
																								<td>
																									<label for="">Register Date</label>
																									<div class="input-group">
																										<input type="date" class="form-control" name="Registerdate" placeholder="First Name" value="<?php echo getPersonRegistredDate($PersonId);?>">
																									</div>
																								</td>
																								<td>
																									<label for="">Address</label>
																									<div class="input-group">
																										<input type="text" class="form-control" name="Address" placeholder="First Name" value="<?php echo getPersonAddress($PersonId);?>">
																									</div>
																								</td>
																							</tr>
																						</tbody>
																					</table>
																				</div>

																			</div>

																			<div class="text-center">
																				<input type="submit" name="updateEm" value="Update" class="btn btn-primary waves-effect waves-light m-r-20">
																				<a href="#!" id="edit-cancel" class="btn btn-default waves-effect">Cancel</a>
																			</div>
																		</div>

																	</div>

																</div>

															</div>

														</form>
														<!-- dhamaadka hiden forka edit employee -->
													</div>
												</div>
											</div>
											<!-- dhamaadka person information  -->

											<!-- halkaan waxaa lagu soo saari doonaa employee salary table  -->
											<div class="tab-pane" id="binfo" role="tabpanel">

											<div class="card">
											<div class="card-header">
											<h5 class="card-header-text">User Services</h5>
											</div>
											<div class="card-block">
				<div class="dt-responsive table-responsive">
					<table id="lists" class="table table-striped table-bordered nowrap">
						<thead>							
							<tr class="s fontSize22">
								<td class="center">Ref</td>
								<td class="center">Date</td>
								<td class="center">Paid</td>
								<td class="center">Charged</td>
								<td class="center">Reason</td>
								<td class="center">Balance</td>
								<td class="center">Action</td>
								
							</tr>						

						</thead>
						<tbody>
					<?php
					            require_once "../Common/commonValues.php";//require Member Class file
			                    require_once'employeeClass.php';
			                    $balance = 0;
		                         $result = getsalaryList($PersonId);
			                        if (!empty($result)){
				                    while ($row = $result->fetch())
			                   	 {
			                   	  $status = $row['status'];
			                   	  $salaryId = $row['salaryId'];
			                   	  $amount = $row['Amount'];
						      echo'<tr>
						   		    <td>'. $row['salaryId'] .'</td>
									<td class="table-plus">'. date("M Y", strtotime($row['createdDate'])).'</td>';

								
					                 if($status == 0){
					                        $balance = $balance + $amount;
					                       
					                        echo  "<td> ". getCurrenceSign() . "" . $amount ."</td>";
					                        echo  "<td> 0.0</td>";
					                         echo  "<td>". $row['SalaryType'] ."</td>";
					                          }
					                          else if($status == 1){
					                            $balance = $balance - $amount;
					                              
					                               echo  "<td>0.0</td>";
					                               echo  "<td style='color:red;'> ". getCurrenceSign() . "" . $amount ."</td>";
					                                echo  "<td >". $row['SalaryType'] ."</td>";                      
					                         } 
					                                           
					                         if($balance >= 0){
					                          $bStyle = "green";
					                        }
					                        else{
					                          $bStyle = "red";
					                        }
					                          
					                          echo "

					                 <td class='blance' style=' text-align:center; color:white; background-color:".$bStyle."' > ". getCurrenceSign() . "" .$balance ."</td>

					                  <td>
					               <button id='edit-btn' type='button'
					                class='btn btn-sm btn-primary waves-effect waves-light f-right' onclick='updateSalary(".$salaryId.")'>
									<i class='icofont icofont-edit'></i>
									</button></td>
					             
					                
					            </tr>";
						            }
						          }
						      ?> 
						</tbody>
						<tfoot>
							<tr class="s fontSize22">
								<td class="center">Ref</td>
								<td class="center">Date</td>
								<td class="center">Paid</td>
								<td class="center">Charged</td>
								<td class="center">Reason</td>
								<td class="center">Balance</td>
								<td class="center" width="5%">Action</td>
								
							</tr>
					</table>
				</div>
			</div>
											</div>
											</div><!-- dhamaadka employee salary  -->








												<div class="tab-pane" id="contacts" role="tabpanel">
													<div class="card">
														<div class="card-header">
															<h5 class="card-header-text">Message & Notification</h5>
														</div>
														<div class="card-block">
															<div class="row">
																<!-- halkaan u isticmaal Php code  -->




															</div>
														</div>
													</div>


												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>

				</div>
				<?php require_once '../Include/script.php'; ?>
			</body>
			</html>
			<script type="text/javascript">
				function updateSalary(e) {
					var salaryId = e;
					alert(salaryId);
					// body...
				}

			</script>
