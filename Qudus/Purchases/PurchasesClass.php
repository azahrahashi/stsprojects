<?php 

require_once '../Connection/dbconection.php';
          // Get All Types Funtion
    function getstages(){

        $stmt=getCnx()->query("SELECT st.Name as StageType,s.* FROM stages s 
        INNER JOIN stagetypes st ON s.StageTypeId=st.Id
          WHERE s.FinishDate IS NULL AND st.FinishDate IS NULL");

                return $stmt;
      }
               // Get Types Funtion
     function getPurchases(){

        $stmt=getCnx()->query(" SELECT p.*,pt.PaymentMethodId,s.SupplierId,pt.PaymentMethod as Method,s.CompanyName FROM purchase p 
          INNER JOIN paymentmethod pt ON p.PaymentMethodId=pt.PaymentMethodId
          INNER JOIN supplier s ON p.SupplierId =s.SupplierId
          WHERE p.FinishDate IS NULL AND pt.FinishDate IS NULL ORDER BY p.PurchaseId DESC");
                return $stmt;
      }

function getPurchaseDetailsById($PurchaseId){
    $stmt= getCnx()->prepare("SELECT pd.*,m.Name as material FROM purchasedetail pd 
      INNER JOIN purchase p ON p.PurchaseId = pd.PurchaseId 
      INNER JOIN material m ON pd.MaterialId = m.Id
      WHERE pd.PurchaseId = :PurchaseId AND pd.FinishDate IS NULL");

    $stmt->bindParam(':PurchaseId', $PurchaseId);
    $stmt->execute();
    return $stmt;
}

    function getMaterials(){

        $stmt=getCnx()->query("SELECT * FROM material 
          WHERE FinishDate IS NULL");

                return $stmt;
      }
    function getMaterialById($Id){

        $stmt=getCnx()->query("SELECT * FROM material 
          WHERE FinishDate IS NULL AND Id=$Id");

                return $stmt;
      }

                 // Add Function
    function addPurchase($supplier,$subtotal,$discount,$totalAmount,$PaymentMethodId,$pdate,$receivedBy,$location,$status)
      {
        $stmt= getCnx()->prepare("INSERT INTO purchase(SupplierId,Amount,Discount,TotalAmount,PaymentMethodId,PurchaseDate,ReceivedBy,whereHouse,Status,StartDate)
              VALUES (:supplier, :subtotal, :discount,:totalAmount, :PaymentMethodId, :pdate, :receivedBy, :location, :status, NOW())");

              $stmt->bindParam(':supplier', $supplier);
              $stmt->bindParam(':subtotal', $subtotal);
              $stmt->bindParam(':discount', $discount);
              $stmt->bindParam(':totalAmount', $totalAmount);
              $stmt->bindParam(':PaymentMethodId', $PaymentMethodId);
              $stmt->bindParam(':pdate', $pdate);
              $stmt->bindParam(':receivedBy', $receivedBy);
              $stmt->bindParam(':location', $location);
              $stmt->bindParam(':status', $status);


              $stmt->execute();

           return getCnx()->lastInsertId();
      }
       function addExpenses($pId,$Amount,$Reason,$Status,$StartDate)
      {
        $stmt= getCnx()->prepare("INSERT INTO expenses(purchaseId,Amount,Reason,Status,StartDate)
              VALUES (:pId, :Amount, :Reason,:Status,:StartDate)");

              $stmt->bindParam(':pId', $pId);
              $stmt->bindParam(':Amount', $Amount);
              $stmt->bindParam(':Reason', $Reason);
              $stmt->bindParam(':Status', $Status);
              $stmt->bindParam(':StartDate', $StartDate);

              $stmt->execute();

           return getCnx()->lastInsertId();
      }
    function addMaterialStore($MaterialId,$pId,$Qty,$status,$StartDate)
      {
        $stmt= getCnx()->prepare("INSERT INTO materialstore(MaterialId,PurchaseId,Qty,Status,StartDate)
              VALUES (:MaterialId,:pId,:Qty,:status,:StartDate)");

              $stmt->bindParam(':MaterialId', $MaterialId);
              $stmt->bindParam(':pId', $pId); 
              $stmt->bindParam(':Qty', $Qty);
              $stmt->bindParam(':status', $status);
              $stmt->bindParam(':StartDate', $StartDate);


              $stmt->execute();

           // return getCnx()->lastInsertId();
      }
          function addPurchaseDetail($pId,$MaterialId,$Qty,$UnitPrice,$total,$status)
      {
        $stmt= getCnx()->prepare("INSERT INTO purchasedetail(PurchaseId,MaterialId,Qty,UnitPrice,Total,Status)
              VALUES (:pId, :MaterialId, :Qty,:UnitPrice, :total, :status)");

              $stmt->bindParam(':pId', $pId);
              $stmt->bindParam(':MaterialId', $MaterialId);
              $stmt->bindParam(':Qty', $Qty);
              $stmt->bindParam(':UnitPrice', $UnitPrice);
              $stmt->bindParam(':total', $total);
              $stmt->bindParam(':status', $status);


              $stmt->execute();

           // return getCnx()->lastInsertId();
      }

function updatePurchase($supplier,$subtotal,$discount,$totalAmount,$PaymentMethodId,$pdate,$receivedBy,$location,$status,$PurchaseId)
{
        $stmt =getCnx()->prepare(" UPDATE purchase SET   SupplierId=:supplier, PurchaseDate=:pdate, Amount=:subtotal, Discount=:discount, TotalAmount=:totalAmount, PaymentMethodId=:PaymentMethodId, whereHouse=:location, ReceivedBy=:receivedBy WHERE  PurchaseId = :PurchaseId");
          $stmt->bindParam(':supplier', $supplier);
          $stmt->bindParam(':pdate', $pdate);
          $stmt->bindParam(':subtotal', $subtotal);
          $stmt->bindParam(':discount', $discount);
          $stmt->bindParam(':totalAmount', $totalAmount);
          $stmt->bindParam(':PaymentMethodId', $PaymentMethodId);
          $stmt->bindParam(':receivedBy', $receivedBy);
          $stmt->bindParam(':location', $location);
          $stmt->bindParam(':PurchaseId', $PurchaseId);
          $stmt->execute();
}
  function updateExpenses($pId,$Amount,$Reason,$status,$StartDate)
      {
        $stmt= getCnx()->prepare("UPDATE expenses SET purchaseId=:pId,Amount=:Amount,Reason=:Reason,Status=:status,StartDate=:StartDate WHERE purchaseId=:pId
              ");

              $stmt->bindParam(':Amount', $Amount);
              $stmt->bindParam(':pId', $pId);  
              $stmt->bindParam(':Reason', $Reason);
               $stmt->bindParam(':status', $status);
              $stmt->bindParam(':StartDate', $StartDate);


              $stmt->execute();
      }
function getProductByPurchaseId($pId,$MaterialId)
{
  
    $stmt= getCnx()->prepare("SELECT `PurchaseDetaiId` FROM `purchasedetail` WHERE `PurchaseId`=:pId AND `MaterialId`=:MaterialId");
    $stmt->bindParam(':pId', $pId);
    $stmt->bindParam(':MaterialId', $MaterialId);
    $stmt->execute();
    if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    
      return $row['PurchaseDetaiId'];
    }
      return $row[0];
  }
}
 function updatePurchaseDetail($pId,$MaterialId,$Qty,$UnitPrice,$total,$status)
  { 
    if (getProductByPurchaseId($pId,$MaterialId)>0) {
      
    $stmt= getCnx()->prepare("UPDATE purchasedetail SET PurchaseId=:pId,MaterialId=:MaterialId,Qty=:Qty,UnitPrice=:UnitPrice,Total=:total,Status=:status,FinishDate=NULL WHERE MaterialId=:MaterialId AND PurchaseId=:pId");

    }else{
    $stmt= getCnx()->prepare("INSERT INTO purchasedetail(PurchaseId,MaterialId,Qty,UnitPrice,Total,Status)
          VALUES (:pId, :MaterialId, :Qty,:UnitPrice, :total,:status)");
    }
          $stmt->bindParam(':pId', $pId);
          $stmt->bindParam(':MaterialId', $MaterialId);
          $stmt->bindParam(':Qty', $Qty);
          $stmt->bindParam(':UnitPrice', $UnitPrice);
          $stmt->bindParam(':total', $total);
          $stmt->bindParam(':status', $status);
          $stmt->execute();
    
  }

  function updateMaterialStore($MaterialId,$pId,$Qty,$status,$StartDate)
      {
        $stmt= getCnx()->prepare("UPDATE materialstore SET MaterialId=:MaterialId,PurchaseId=:pId,Qty=:Qty,Status=:status,StartDate=:StartDate WHERE PurchaseId=:pId
              ");

              $stmt->bindParam(':MaterialId', $MaterialId);
              $stmt->bindParam(':pId', $pId);  
              $stmt->bindParam(':Qty', $Qty);
               $stmt->bindParam(':status', $status);
              $stmt->bindParam(':StartDate', $StartDate);


              $stmt->execute();
      }

         /*       Terminate Purchase Details
        **************************************/ 
function terminatePurchaseDetail($PurchaseID,$MaterialId)
  {
    $stmt =getCnx()->prepare("UPDATE purchasedetail SET  FinishDate=NOW() WHERE  PurchaseId = :PurchaseID AND MaterialId=:MaterialId");

      $stmt->bindParam(':PurchaseID', $PurchaseID);
      $stmt->bindParam(':MaterialId', $MaterialId);
      $stmt->execute();

  }
         
      function terminatePushace($PurchaseId)
      {
        $stmt =getCnx()->prepare("UPDATE purchase SET  FinishDate=NOW() WHERE  PurchaseId = :PurchaseId");

          $stmt->bindParam(':PurchaseId', $PurchaseId);
          $stmt->execute();
      }



function get_Purchase_By_ID($PurchaseId){
  $stmt=getCnx()->prepare("SELECT M.Id as MId, M.Name AS materialName, PD.Qty,PD.UnitPrice,PD.Total,P.Discount,P.TotalAmount FROM purchasedetail PD
INNER JOIN material M ON M.Id = PD.MaterialId 
INNER JOIN purchase P ON P.PurchaseId = PD.PurchaseId
 where P.PurchaseId=:PurchaseId  AND PD.FinishDate IS NULL ");

  $stmt->bindParam(':PurchaseId',$PurchaseId);
  $stmt->execute();
  return $stmt;

}
 function getSupplierById($PurchaseId)
   {
      $stmt= getCnx()->prepare("SELECT s.SupplierId FROM supplier s
        INNER JOIN purchase p ON p.SupplierId =s.SupplierId
                    where p.PurchaseId=:PurchaseId");
        $stmt->bindParam(':PurchaseId', $PurchaseId);
          $stmt->execute();
          if (!empty($stmt)) {
         while ($row=$stmt->fetch()) {
           return $row['SupplierId'];
            
        }         
     }
          return 0;
   }
 function getWherehouseById($PurchaseId)
   {
      $stmt= getCnx()->prepare("SELECT s.WarehouseId FROM warehouse s
        INNER JOIN purchase p ON p.whereHouse =s.WarehouseId
                    where p.PurchaseId=:PurchaseId");
        $stmt->bindParam(':PurchaseId', $PurchaseId);
          $stmt->execute();
          if (!empty($stmt)) {
         while ($row=$stmt->fetch()) {
           return $row['WarehouseId'];
            
        }         
     }
          return 0;
   }
 function getOrderStatusById($PurchaseId)
   {
      $stmt= getCnx()->prepare("SELECT s.StatusId FROM orderstatus s
        INNER JOIN purchase p ON p.Status =s.StatusId
                    where p.PurchaseId=:PurchaseId");
        $stmt->bindParam(':PurchaseId', $PurchaseId);
          $stmt->execute();
          if (!empty($stmt)) {
         while ($row=$stmt->fetch()) {
           return $row['StatusId'];
            
        }         
     }
          return 0;
   }
 function getPaymentMethodById($PurchaseId)
   {
      $stmt= getCnx()->prepare("SELECT s.PaymentMethodId FROM paymentmethod s
        INNER JOIN purchase p ON p.PaymentMethodId =s.PaymentMethodId
                    where p.PurchaseId=:PurchaseId");
        $stmt->bindParam(':PurchaseId', $PurchaseId);
          $stmt->execute();
          if (!empty($stmt)) {
         while ($row=$stmt->fetch()) {
           return $row['PaymentMethodId'];
            
        }         
     }
          return 0;
   }
   function getLocations(){

  $stmt=getCnx()->query("SELECT * FROM warehouse 
    WHERE FinishDate IS NULL");

  return $stmt;
}
   function getStatus(){

  $stmt=getCnx()->query("SELECT * FROM orderstatus 
    WHERE FinishDate IS NULL");

  return $stmt;
}
 ?>