<?php 

require_once '../Connection/dbconection.php';
          // Get All Products Function
function getstAllsuppliers(){

  $stmt=getCnx()->query("SELECT * FROM supplier 
    WHERE FinishDate IS NULL");

  return $stmt;
}

                 // Add Function
function addsuppliers($CompanyName,$ContectPerson,$Phone,$Address,$createdBy,$StartDate)
{
  $stmt= getCnx()->prepare("INSERT INTO supplier(CompanyName,ContectPerson,Phone,Address,createdBy,StartDate)
    VALUES ( :CompanyName, :ContectPerson, :Phone, :Address, :createdBy, :StartDate)");

  $stmt->bindParam(':CompanyName', $CompanyName);
  $stmt->bindParam(':ContectPerson', $ContectPerson);
  $stmt->bindParam(':Phone', $Phone);
  $stmt->bindParam(':Address', $Address);
  $stmt->bindParam(':createdBy', $createdBy);
  $stmt->bindParam(':StartDate', $StartDate);

  $stmt->execute();

}

             // Update Funtion
function updatesuppliers($SupplierId,$CompanyName,$ContectPerson,$Phone,$Address,$createdBy,$StartDate)
{

  $stmt =getCnx()->prepare(" UPDATE supplier SET  CompanyName=:CompanyName, ContectPerson=:ContectPerson, Phone=:Phone, Address=:Address, createdBy=:createdBy, StartDate=:StartDate  WHERE  SupplierId = :SupplierId");

  $stmt->bindParam(':SupplierId', $SupplierId);
  $stmt->bindParam(':CompanyName', $CompanyName);
  $stmt->bindParam(':ContectPerson', $ContectPerson);
  $stmt->bindParam(':Phone', $Phone);
  $stmt->bindParam(':Address', $Address);
  $stmt->bindParam(':createdBy', $createdBy);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->execute();
}

function terminateSupplier($SupplierId)
{
  $stmt =getCnx()->prepare("UPDATE supplier SET  FinishDate=NOW() WHERE  SupplierId = :SupplierId");

  $stmt->bindParam(':SupplierId', $SupplierId);
  $stmt->execute();
}




?>