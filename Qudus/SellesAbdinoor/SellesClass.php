<?php
require_once'../Connection/dbconection.php';


    /************************************
         get Customer List
    **************************************/ 
  function getAllCustomer()
  {
     $stmt =getCnx()->query("SELECT * FROM customers");
     $stmt->execute();
     return $stmt;
  }
    /************************************
         get Product List
    **************************************/ 
  function getProduct(){

        $stmt=getCnx()->query("SELECT * FROM products 
          WHERE FinishDate IS NULL");

                return $stmt;
      }

      /**********************************
        get product By Id 
      ***********************************/ 
    function getProductDetail($id)

    {
           $stmt = getCnx()->prepare("SELECT * FROM products
                                                                       
                                      WHERE   Id = :id");
           $stmt->bindParam(':id', $id);
          
            $stmt->execute();
            return $stmt;
    }
    /************************************
         get Product List
    **************************************/ 
  function GetAllProduct()
  {
     $stmt =getCnx()->query("SELECT * FROM Products");
     $stmt->execute();
     return $stmt;
  }


    /**********************************
            Add New Project            
    ************************************/
function    addMaterial($mName,$mstartDate)
{
    $cnx = getCnx();
    $stmt = $cnx->prepare("INSERT INTO material(Name,StartDate) 
            VALUES (:mName,:mstartDate)");

       
        $stmt->bindParam(':mName', $mName);
        $stmt->bindParam(':mstartDate',$mstartDate);
        $stmt->execute();

}
    /**************************************
            Update Project 
    ***************************************/ 
  function    updatematerialId($mId,$mName,$mstartDate){
        $stmt = getCnx()->prepare("UPDATE material SET Name = :mName ,  StartDate  = :mstartDate WHERE  Id = :mId");

        $stmt->bindParam(':mId', $mId);
        $stmt->bindParam(':mName', $mName);
        $stmt->bindParam(':mstartDate',$mstartDate);
        
        $stmt->execute();
        return $stmt;


  }
     /*********************************
           Terminate Income
     **********************************/
  function terminateMaterial($mId)
    {
        $stmt = getCnx()->prepare("UPDATE material SET FinishDate = NOW()  WHERE Id = :mId");
        $stmt->bindParam(':mId', $mId);
        $stmt->execute();
        return $stmt;

    } 
     /***********************************
         get Income Detail
     *************************************/ 
function getMaterialDetail($mId)

{
       $stmt = getCnx()->prepare("SELECT * FROM material WHERE   Id = :mId");
       $stmt->bindParam(':mId', $mId);
        $stmt->execute();
        return $stmt;
}

    /************************************
         get Suppliers List
    **************************************/ 
  function getProjectList()
  {
     $stmt =getCnx()->query("SELECT * FROM material WHERE  FinishDate IS NULL");
     $stmt->execute();
     return $stmt;
  }

?>