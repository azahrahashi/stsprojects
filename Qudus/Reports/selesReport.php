<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Seles Report</h4>
														<span>This is The<code>Salles</code> Report</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											<div>
												<form>
													<div class="row">
														<div class="col-3">
															<input type="date" class="form-control" id="date" placeholder="First name">
														</div>
														<div class="col-3">
															<input type="date" class="form-control" id="toDate" placeholder="Last name">
														</div>
														<div class="col-3">
															<button type="button" class="btn btn-outline-secondary" id="search">Search</button>
														</div>
													</div>
												</form>
											</div>
											<br/>
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Amount</th>
															<th>Discount</th>
															<th>Total Paid</th>
															<th>Sales Date</th>
															<th>Total Amount</th>
															<th>Blance</th>
														</tr>
													</thead>

													<tbody>

													</tbody>

												</tbody>
											</table>
										</div>

										<!-- Add Stagetype Model -->

										<div class="modal fade" id="Asalary" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Add Salary</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="form-group row">
																<br>
																<div class="col-sm-12">
																	<input type="text" id="Amount" name="Amount" class="form-control" placeholder="Enter Amount">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<select name="SalaryType" id="SalaryType" class="form-control">
																		<option value="0">Select Salary</option>
																		<option value="Advance">advance</option>
																		<option value="Wage">wage</option>
																		<option value="Salary">normal salary</option>
																	</select>
																</div>
																<br>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="Paidby" name="Paidby" class="form-control" placeholder="Paid by">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="text" id="receivedby" name="receivedby" class="form-control" placeholder="received by">
																</div>
																<br>
															</div>
															<div class="form-group row">
																<br>
																<div class="col-sm-12">
																	<input type="date" id="Date" name="Date" class="form-control" placeholder="Date..">
																</div>
																<br>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="addsalary()">Add</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- End Add Stagetype Model -->


										<!-- Update Model -->

										<div class="modal fade" id="Usalary" tabindex="-1" role="dialog">
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Update Salary</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="form-group row">
																<br>
																<div class="col-sm-12">
																	<input type="text" name="usalaryId" id="usalaryId">
																	<input type="text" id="uAmount" name="uAmount" class="form-control" placeholder="Enter Amount">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<select name="uSalaryType" id="uSalaryType" class="form-control">
																		<option value="0">Select Salary</option>
																		<option value="Advance">Advance</option>
																		<option value="Wage">Wage</option>
																		<option value="Salary">Salary</option>

																	</select>
																</div>
																<br>
															</div>
															<div class="form-group row">
																<div class="col-sm-12">
																	<input type="text" id="uPaidby" name="uPaidby" class="form-control" placeholder="Paid by">
																</div>
																<br>
																<br>
																<div class="col-sm-12">
																	<input type="text" id="ureceivedby" name="ureceivedby" class="form-control" placeholder="received by">
																</div>
																<br>
															</div>
															<div class="form-group row">
																<br>
																<div class="col-sm-12">
																	<input type="date" id="uDate" name="uDate" class="form-control" placeholder="Date..">
																</div>
																<br>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light "onclick="updatesalary()">Update</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>

										<!-- End Update Model -->


									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script type="text/javascript">


	$("#search").click(function() {
		dateTodate();
	});

	function dateTodate() {
		var Salesdate = $("#date").val();
		var SalestoDate = $("#toDate").val();
		$.ajax({
			url: "report.php/getSallesByDate",
			method: "POST",
			data:{Salesdate:Salesdate, SalestoDate:SalestoDate},
			success: function (data) {
				$("tbody").html("");
				$("tbody").append(data);
			}
		});
	}

	getSelles();
	function getSelles() {
		var SallesDetails = "";
		$.ajax({
			url: "report.php/getSallesList",
			method: "POST",
			data:{SallesDetails:SallesDetails},
			success: function (data) {
				$("tbody").append(data);
			}
		});
	}
	function addsalary() {
		var Amount = $("#Amount").val();
		var SalaryType = $("#SalaryType").val();
		var Paidby = $("#Paidby").val();
		var receivedby = $("#receivedby").val();
		var date = $("#Date").val();
		var salaryId = 0;

		if (SalaryType == "") {
			alert("Empty is not allowed");
		}
		else if(Amount == ""){
			alert("Empty is not allowed");
		}
		else if(Paidby == ""){
			alert("Empty is not allowed");
		}
		else if(receivedby == ""){
			alert("Empty is not allowed");
		}
		else if(date == ""){
			alert("Empty is not allowed");
		}
		else{
			$.ajax({
				url: "addSalary.php",
				method: "POST",
				data: {Amount:Amount,SalaryType:SalaryType,Paidby:Paidby,receivedby:receivedby,date:date, salaryId: salaryId},
				success: function (data) {
					getSellery();
					$('#Asalary').modal('hide');
				}
			});

		}
	}

	function setsalary(salaryId,Amount,SalaryType,Paidby,receivedby,Date) {

		$("#usalaryId").val(salaryId);
		$("#uAmount").val(Amount);
		if (SalaryType == 'Advance') {
			$('#uSalaryType').html('<option value="">'+SalaryType+'</option>'
				+'<option value="Wage">Wage</option>'
				+'<option value="Salary">Salary</option>'
				);
		}
		else if (SalaryType == 'Wage') {
			$('#uSalaryType').html('<option value="">'+SalaryType+'</option>'
				+'<option value="Advance">Advance</option>'
				+'<option value="Salary">Salary</option>'
				);
		}
		else if (SalaryType == 'Salary') {
			$('#uSalaryType').html('<option value="">'+SalaryType+'</option>'
				+'<option value="Advance">Advance</option>'
				+'<option value="Wage">Wage</option>'
				);
		}

		$("#uPaidby").val(Paidby);
		$("#ureceivedby").val(receivedby)
		$("#uDate").val(Date);
		$("#Usalary").modal("show");
	}

	function updatesalary(){

		var salaryId=$("#usalaryId").val();
		var Amount=$("#uAmount").val();
		var SalaryType = $('#uSalaryType').val();
		var Paidby=$("#uPaidby").val();
		var receivedby=$("#ureceivedby").val();
		var Date=$("#uDate").val();

		$.post("addSalary.php",{
			Amount:Amount,
			SalaryType:SalaryType,
			Paidby:Paidby,
			receivedby:receivedby,
			Date:Date,
			salaryId:salaryId
		}, function (data, status) {
			$("tbody").html("");
			getSellery();
			$('#Usalary').modal('hide');
		});

	}


	function removeSalary(salaryId) {

		$.post("removeSalary.php",{
			salaryId:salaryId
		},
		function (data, status) {
			$("tbody").html("");
			getSellery();
		});

	}



</script>

<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#lists').DataTable();
	});
</script>
