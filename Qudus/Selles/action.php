<?php 
session_start();
   // session_destroy();
   // $_SESSION = array();

   $total = 0;
   $items = 0;
   $order_table = '';
   $message = '';
if (isset($_POST["product_id"])) 
{
      // $item_array;
   if ($_POST['action'] == "add") 
   {
      if (isset($_SESSION["shopping_cart"])) 
      {
            // echo count($_SESSION['shopping_cart']);
            // print_r($_SESSION);
         $is_available = 0;
         foreach ($_SESSION['shopping_cart'] as $keys => $values) 
         {
            if ($_SESSION['shopping_cart'][$keys]['product_id'] == $_POST['product_id']) 
            {
               $is_available++;
               if ($_POST['flag']==0) {
                  $_SESSION['shopping_cart'][$keys]['product_qty'] = $_SESSION['shopping_cart'][$keys]['product_qty'] + $_POST['product_qty'];
               }
               if ($_POST['flag']==1) {
                  $_SESSION['shopping_cart'][$keys]['product_qty'] = $_POST['product_qty'];
               }
               // if ($_POST['flag']==2) {
               //    $_SESSION['shopping_cart'][$keys]['product_qty'] =(($_SESSION['shopping_cart'][$keys]['product_qty']) + 1);
               // }
               
            }
         }
         if ($is_available < 1) 
         {
            $item_array  = array(
               'product_id' => $_POST['product_id'],
               'product_name' => $_POST['product_name'],
               'product_price' => $_POST['product_price'],
               'product_qty' => $_POST['product_qty']
            );
            $_SESSION['shopping_cart'][] = $item_array;
         }  
      }
      else
      { 
         $item_array  = array(
            'product_id' => $_POST['product_id'],
            'product_name' => $_POST['product_name'],
            'product_price' => $_POST['product_price'],
            'product_qty' => $_POST['product_qty']
         );
         $_SESSION['shopping_cart'][] = $item_array;
      }

   }
   if ($_POST['action'] == "remove") 
   {
         foreach ($_SESSION['shopping_cart'] as $keys => $values) 
         {
            if ($values['product_id'] == $_POST['product_id']) 
            {
                  unset($_SESSION['shopping_cart'][$keys]);
            }
         }
   }
      $order_table .='
      <table class="table table-bordered">
      <tr>
      <th width="40%">Product Name</th>

      <th width="10%">Quantity</th>
      <th width="20%">Price</th>
      <th  width="15%">Total</th>
      <th width="5%"><i class="fa fa-trash-o"></i></th>
      </tr>';
      if (!empty($_SESSION['shopping_cart'])) 
      {
         foreach ($_SESSION['shopping_cart'] as $keys => $values) {
            $order_table.='
            <tr>
            <td>'.$values["product_name"].'</td>
            <td align="right"> <input id="prQty'.$values["product_id"].'" onblur="setSelles('.$values["product_id"].',1);" value="'.$values["product_qty"].'"></td>
            <td align="right">$ '.$values["product_price"].'</td>
            <td>'.number_format($values["product_qty"] * $values["product_price"],2).'</td>
            <td><button name="delete" class="delete" id="'.$values["product_id"].'"><i class="fa fa-times tip pointer posdel"></i></button></td>
            </tr>
            ';
            $total = $total + ($values["product_qty"] * $values["product_price"]);
            $items = $items +$values["product_qty"];
         }
         $order_table.='
         <tr>
         <td colspan="3" align="right">Total</td>
         <td align="right">'.number_format($total,2).'</td>
         <td></td>
         </tr>
         ';
      }
      $order_table .='</table>';
      $output  = array(
         'order_table' => $order_table,
         'cart_item' => $items,
         'total' => $total,
         'shop' =>$_SESSION['shopping_cart']
      );
// header("Content-Type: application/json; charset=UTF-8");
      echo json_encode($output);
}



?>