<?php
require_once'../Connection/dbconection.php';


   /************************************
         get Customer List
    **************************************/ 
  function getAllCustomer()
  {
     $stmt =getCnx()->query("SELECT * FROM customers");
     $stmt->execute();
     return $stmt;
  }
    /************************************
         get Product List
    **************************************/ 
function getProduct(){
  $stmt=getCnx()->query("SELECT * FROM products WHERE FinishDate IS NULL ORDER BY `products`.`Id` DESC");
          return $stmt;
}

    /************************************
         Add Customer List
    **************************************/ 
      /*function addCustomer($FName,$LName,$Gender,$Telephone,$Address,$Email){
          $stmt=getCnx()->prepare("INSERT INTO customers ()
            ");
         }*/


       /************************************
             Add Selles List
        **************************************/ 
function addSelles($Customer,$date,$subtotal,$discount,$totalAmount,$PaymentMethodId,$receivedBy)
  {
    $stmt= getCnx()->prepare("INSERT INTO selles(CustomerId,SellesDate,Amount,Descount,TotalAmount,PaymentMethodId,TotalPaid,ReceivedBy)
          VALUES (:Customer,:date, :subtotal, :discount,:totalAmount, :PaymentMethodId,:totalAmount, :receivedBy)");

          $stmt->bindParam(':Customer', $Customer);
          $stmt->bindParam(':date', $date);
          $stmt->bindParam(':subtotal', $subtotal);
          $stmt->bindParam(':discount', $discount);
          $stmt->bindParam(':totalAmount', $totalAmount);
          $stmt->bindParam(':PaymentMethodId', $PaymentMethodId);
          $stmt->bindParam(':receivedBy', $receivedBy);
         
          $stmt->execute();

       return getCnx()->lastInsertId();
  }

      function addSellesDetail($sId,$ProductId,$Qty,$UnitPrice,$total,$status)
  {
    $stmt= getCnx()->prepare("INSERT INTO sellsdetail(SellsId,ProductId,Qty,Price,Total,Status)
          VALUES (:sId, :ProductId, :Qty,:UnitPrice, :total,:status)");

          $stmt->bindParam(':sId', $sId);
          $stmt->bindParam(':ProductId', $ProductId);
          $stmt->bindParam(':Qty', $Qty);
          $stmt->bindParam(':UnitPrice', $UnitPrice);
          $stmt->bindParam(':total', $total);
          $stmt->bindParam(':status', $status);
          $stmt->execute();
    
  }
  function addProductstock($ProductId,$sId,$Qty,$UnitPrice,$SellPrice,$status,$StartDate)
      {
        $stmt= getCnx()->prepare("INSERT INTO productstock(ProductId,SellsID,Qty,price,SellPrice,Status,StartDate)
              VALUES (:ProductId,:sId,:Qty,:UnitPrice,:SellPrice,:status,:StartDate)");

              $stmt->bindParam(':ProductId', $ProductId);
              $stmt->bindParam(':sId', $sId);  
              $stmt->bindParam(':Qty', $Qty);
              $stmt->bindParam(':UnitPrice', $UnitPrice);
              $stmt->bindParam(':SellPrice', $SellPrice);
               $stmt->bindParam(':status', $status);
              $stmt->bindParam(':StartDate', $StartDate);


              $stmt->execute();
      }
         /************************************
             Get Selles List
        **************************************/ 
function getSelles(){

    $stmt=getCnx()->query("SELECT `SellsID`, `Name` as Name, `TotalAmount`,`Amount`, `Descount`, `PaymentMethod` FROM `customers` INNER JOIN selles on customers.CustomerId=selles.CustomerId INNER JOIN paymentmethod on selles.PaymentMethodId=paymentmethod.PaymentMethodId WHERE selles.FinishDate IS NULL ORDER BY `selles`.`SellsID` DESC");
            return $stmt;
  }


         /************************************
             Terminate Selles
        **************************************/ 
function terminateSelles($SellsID)
  {
    $stmt =getCnx()->prepare("UPDATE selles SET  FinishDate=NOW() WHERE  SellsID = :SellsID");

      $stmt->bindParam(':SellsID', $SellsID);
      $stmt->execute();

  }       /************************************
             Terminate Selles Details
        **************************************/ 
function terminateSalesDetail($SellsID,$product_id)
  {
    $stmt =getCnx()->prepare("UPDATE sellsdetail SET  FinishDate=NOW() WHERE  SellsID = :SellsID AND ProductId=:product_id");

      $stmt->bindParam(':SellsID', $SellsID);
      $stmt->bindParam(':product_id', $product_id);
      $stmt->execute();

  }
   /************************************
             Terminate Selles
   **************************************/ 

function getSellesDetailsById($SellsID){
    $stmt= getCnx()->prepare("SELECT `SellsDetaiId`, `Name`, `Qty`, `Price`, `Total` FROM `sellsdetail` 
      INNER JOIN selles s on sellsdetail.SellsId=s.SellsID  
      INNER JOIN products p on p.Id=sellsdetail.ProductId
      WHERE sellsdetail.SellsID = :SellsID AND sellsdetail.FinishDate IS NULL");
    $stmt->bindParam(':SellsID', $SellsID);
    $stmt->execute();
    return $stmt;
}

   /************************************
             get_selles_By_ID
   **************************************/ 
              function getCustomerById($SellsID)
   {
      $stmt= getCnx()->prepare("SELECT c.CustomerId FROM customers c
        INNER JOIN selles S ON S.CustomerId =c.CustomerId
                    where S.SellsID=:SellsID");
        $stmt->bindParam(':SellsID', $SellsID);
          $stmt->execute();
          if (!empty($stmt)) {
         while ($row=$stmt->fetch()) {
           return $row['CustomerId'];
            
        }         
     }
          return 0;
   }
function get_selles_By_ID($SellsID){
  $stmt=getCnx()->prepare("SELECT P.Id as pId, P.Name AS productName, SD.Qty,SD.Price,SD.Total,S.Descount,S.TotalAmount FROM sellsdetail SD
INNER JOIN products P ON P.Id = SD.ProductId 
INNER JOIN selles S ON S.SellsID = SD.SellsId
 where S.SellsID=:SellsID   AND SD.FinishDate IS NULL ");
  $stmt->bindParam(':SellsID',$SellsID);
  $stmt->execute();
  return $stmt;

}
function getUnitPriceBypId($Id)
{
    $stmt= getCnx()->prepare("SELECT `UnitPrice` FROM `products` WHERE `Id`=:Id");
    $stmt->bindParam(':Id', $Id);
    $stmt->execute();
    if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    
      return $row['UnitPrice'];
    }
      return $row[0];
  }
}
function updateSales($Customer,$date,$subtotal,$discount,$totalAmount,$PaymentMethodId,$receivedBy,$SellsID)
{
        $stmt =getCnx()->prepare(" UPDATE selles SET   CustomerId=:Customer, SellesDate=:date, Amount=:subtotal, Descount=:discount, TotalAmount=:totalAmount, PaymentMethodId=:PaymentMethodId, TotalPaid=:totalAmount, ReceivedBy=:receivedBy WHERE  SellsID = :SellsID");
          $stmt->bindParam(':Customer', $Customer);
          $stmt->bindParam(':date', $date);
          $stmt->bindParam(':subtotal', $subtotal);
          $stmt->bindParam(':discount', $discount);
          $stmt->bindParam(':totalAmount', $totalAmount);
          $stmt->bindParam(':PaymentMethodId', $PaymentMethodId);
          $stmt->bindParam(':receivedBy', $receivedBy);
          $stmt->bindParam(':SellsID', $SellsID);
          $stmt->execute();
}
function getProductBySalesId($sId,$ProductId)
{
  
    $stmt= getCnx()->prepare("SELECT `SellsDetaiId` FROM `sellsdetail` WHERE `SellsId`=:sId AND `ProductId`=:ProductId");
    $stmt->bindParam(':sId', $sId);
    $stmt->bindParam(':ProductId', $ProductId);
    $stmt->execute();
    if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    
      return $row['SellsDetaiId'];
    }
      return $row[0];
  }
}
 function updateSellesDetail($sId,$ProductId,$Qty,$UnitPrice,$total,$status)
  { 
    if (getProductBySalesId($sId,$ProductId)>0) {
      
    $stmt= getCnx()->prepare("UPDATE sellsdetail SET SellsId=:sId,ProductId=:ProductId,Qty=:Qty,Price=:UnitPrice,Total=:total,Status=:status,FinishDate=NULL WHERE  ProductId=:ProductId AND SellsId=:sId");

    }else{
    $stmt= getCnx()->prepare("INSERT INTO sellsdetail(SellsId,ProductId,Qty,Price,Total,Status)
          VALUES (:sId, :ProductId, :Qty,:UnitPrice, :total,:status)");
    }
          $stmt->bindParam(':sId', $sId);
          $stmt->bindParam(':ProductId', $ProductId);
          $stmt->bindParam(':Qty', $Qty);
          $stmt->bindParam(':UnitPrice', $UnitPrice);
          $stmt->bindParam(':total', $total);
          $stmt->bindParam(':status', $status);
          $stmt->execute();
    
  }

  function updateProductstock($ProductId,$sId,$Qty,$UnitPrice,$SellPrice,$status,$StartDate)
      {
        $stmt= getCnx()->prepare("UPDATE productstock SET ProductId=:ProductId,SellsID=:sId,Qty=:Qty,price=:UnitPrice,SellPrice=:SellPrice,Status=:status,StartDate=:StartDate WHERE SellsID=:sId
              ");

              $stmt->bindParam(':ProductId', $ProductId);
              $stmt->bindParam(':sId', $sId);  
              $stmt->bindParam(':Qty', $Qty);
              $stmt->bindParam(':UnitPrice', $UnitPrice);
              $stmt->bindParam(':SellPrice', $SellPrice);
               $stmt->bindParam(':status', $status);
              $stmt->bindParam(':StartDate', $StartDate);


              $stmt->execute();
      }
?>