<?php
require_once '../Connection/dbconection.php';

function getOilChangesReport($startDate,$endDate)
{
	$stmt= getCnx()->prepare("SELECT COUNT(s.ServiceId) AS Totaloil FROM service s WHERE s.ServiceDate BETWEEN :startDate AND :endDate");

        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);

        $stmt->execute();
        $Totalwash = 0;
        if($stmt->rowcount()>0){
      		while($row=$stmt->fetch())
      		{
      			$Totaloil = $row["Totaloil"];

      		}
      	}
        return $Totaloil;
  }
function getOilChanges($startDate,$endDate){
  $stmt= getCnx()->prepare("SELECT v.VehicleId,v.Plate,s.SubTotal,s.ServiceId,s.Discount,s.Total,pm.PaymentMethodId,pm.PaymentMethodName,s.CreatedBy
    FROM vehicle v
    INNER JOIN service s ON v.VehicleId=s.VehicleId
    INNER JOIN paymentmethod pm ON pm.PaymentMethodId = s.PaymentMethodId
    WHERE s.ServiceDate BETWEEN  :startDate AND :endDate");

        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return $stmt;
}

function getWashReport($startDate,$endDate)
{
	//for wash table
	$stmt= getCnx()->prepare("SELECT COUNT(w.WashServiceId) AS Totalwash FROM washservice w WHERE w.ServiceDate BETWEEN :startDate AND :endDate ");

        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);

        $stmt->execute();
        $Totalwash = 0;
        if($stmt->rowcount()>0){
      		while($row=$stmt->fetch())
      		{
      			$Totalwash = $row["Totalwash"];

      		}
      	}
        return $Totalwash;
}
function getWashes($startDate,$endDate)
{
   $stmt= getCnx()->prepare("SELECT v.VehicleId,v.Plate,w.SubTotal,w.Discount,w.Total,pm.PaymentMethodId,pm.PaymentMethodName,w.CreatedBy
    FROM vehicle v
    INNER JOIN washservice w ON v.VehicleId=w.VehicleId
    INNER JOIN paymentmethod pm ON pm.PaymentMethodId = w.PaymentMethodId
    WHERE w.ServiceDate BETWEEN :startDate AND :endDate");

        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return $stmt;
  }


function getWashesReportByPeriod($startDate,$endDate)
{
   $stmt= getCnx()->prepare("SELECT v.*,w.*, pm.PaymentMethodName
    FROM vehicle v
    INNER JOIN washservice w ON v.VehicleId=w.VehicleId
    INNER JOIN servicestatus st ON w.ServiceStatusId=st.ServiceStatusId
    INNER JOIN paymentmethod pm ON pm.PaymentMethodId = w.PaymentMethodId
    WHERE w.FinishDate IS NULL
    AND st.ServiceStatusType  <> 'Cancel'
    AND w.ServiceDate BETWEEN :startDate AND :endDate");

        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return $stmt;
  }

function getTotalIncomeService($startDate,$endDate)
{
	$stmt= getCnx()->prepare("SELECT sum(s.Total) AS TotalIncome FROM service s  WHERE s.ServiceDate BETWEEN :startDate AND :endDate");

        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return $stmt;
}
function getTotalIncomeWash($startDate,$endDate)
{
	//for wash table

	$stmt= getCnx()->prepare("SELECT sum(w.Total) AS TotalIncome FROM washservice w  WHERE w.ServiceDate BETWEEN :startDate AND :endDate");

        $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return $stmt;
}

function getTotalIncomeServicesAndWashes($startDate,$endDate)
{
	$res=getTotalIncomeService($startDate,$endDate);
	if($res->rowcount()>0){
		while($row=$res->fetch())
		{
			$oilIncome = $row["TotalIncome"];

		}
	}
	$reswash=getTotalIncomeWash($startDate,$endDate);
	if($reswash->rowcount()>0){
		while($row=$reswash->fetch())
		{
			$washIncome = $row["TotalIncome"];

		}
	}
	$allTotal=$oilIncome+$washIncome;
	return $allTotal;

}
function getCancelledServices($startDate,$endDate)
{

      $stmt=getCnx()->prepare("SELECT COUNT(ServiceId) as result FROM service
       WHERE ServiceStatusId=0 AND s.ServiceDate BETWEEN :startDate AND :endDate");

	    $stmt->bindParam(':startDate', $startDate);
        $stmt->bindParam(':endDate', $endDate);
        $stmt->execute();
        return $stmt;
}
function washserviceDetails($VehicleId)
{
  $stmt=getCnx()->prepare("SELECT w.*, v.*, pm.PaymentMethodName, ws.Cost,p.FirstName,p.LastName FROM washservice w
  INNER JOIN vehicle v
  ON w.VehicleId=v.VehicleId
  INNER JOIN washservicedetails ws
  ON ws.washServiceId=w.WashServiceId
  INNER JOIN paymentmethod pm
  ON pm.PaymentMethodId=w.PaymentMethodId
  INNER JOIN ownership ow
  ON ow.VehicleId=v.VehicleId
  INNER JOIN person p
  ON p.PersonId=ow.PersonId
  WHERE v.VehicleId=:VehicleId AND w.ServiceStatusId=1");
	$stmt->bindParam(':VehicleId', $VehicleId);
	$stmt->execute();
  return $stmt;
}


function getCarDetailsBy($VehicleId)
{
  $stmt=getCnx()->prepare("SELECT v.*, p.FirstName,p.LastName 
  	FROM vehicle v
  	INNER JOIN ownership ow ON ow.VehicleId=v.VehicleId
  	INNER JOIN person p ON p.PersonId=ow.PersonId
  	WHERE v.VehicleId=:VehicleId ");
	$stmt->bindParam(':VehicleId', $VehicleId);
	$stmt->execute();
  return $stmt;
}

	function isEmployee($CreatedBy){
		if ($CreatedBy=='') {
			$data='';
			$data.='<a href="#"><span type="button" style="color:red;" data-toggle="modal" data-target="#updateEmployeeModal">Update</span></a>';
			return $data;
	//echo "<button onclick="updateVehicles(.$row['StorageId'].)">Update</button>";
		}
		return $CreatedBy;
	}

	function employeePersonalDetails($CreatedBy)
	{
	  $stmt=getCnx()->prepare("SELECT p.*,ar.Role FROM authorised a
			INNER JOIN authorisedmembership am
			ON am.AuthorisedId =a.AuthorisedId
			INNER JOIN authorityrole ar
			ON ar.AuthorityRoleId=am.AuthorityRoleId
			INNER JOIN person p
			ON p.PersonId=am.PersonId
			WHERE a.UserName=:CreatedBy");
		$stmt->bindParam(':CreatedBy', $CreatedBy);
		$stmt->execute();
	  return $stmt;
	}
	function queWash(){
	  $stmt= getCnx()->query("SELECT ws.WashServiceId,v.Make,v.Model,v.Plate,ws.ServiceStatusId,wsd.EmploymentId FROM vehicle v
			INNER JOIN washservice ws ON v.VehicleId = ws.VehicleId
			INNER JOIN washservicedetails wsd ON ws.WashServiceId = wsd.WashServiceId
			INNER JOIN servicestatus st ON st.ServiceStatusId = ws.ServiceStatusId
			WHERE ws.ServiceStatusId =1");


	        $stmt->execute();
	        return $stmt;
	}
	function queOilchange(){
	  $stmt= getCnx()->query("SELECT s.ServiceId,v.Make,v.Model,v.Plate,s.ServiceStatusId,sd.EmploymentId FROM vehicle v
			INNER join service s ON v.VehicleId = s.VehicleId
			INNER JOIN servicedetails sd ON s.ServiceId = sd.ServiceId
			INNER JOIN servicestatus st ON st.ServiceStatusId = s.ServiceStatusId
			WHERE s.ServiceStatusId =1");


	        $stmt->execute();
	        return $stmt;
	}
	function getemployeeList()
	{
	  $result =  getCnx()->query("SELECT em.EmploymentId,p.FirstName,p.LastName FROM employment em
INNER JOIN person p ON p.PersonId = em.PersonId");

	  while ($row=$result->fetch()) {
	    echo '<option value='. $row["EmploymentId"] .' >'. $row["FirstName"] .'' . $row["LastName"] .'</option>';

	  }

	}

	function updateWashService($Wash_Id)
	{
	  try{
	  $stmt =getCnx()->prepare("UPDATE washservice set ServiceStatusId =2
			WHERE WashServiceId =:Id ");

	  $stmt->bindParam(':Id', $Wash_Id);
	  $stmt->execute();
	  } catch(PDOException $e){
	    echo $e;
	  }
	}

	function updateWashServiceDetailsEmployee($Wash_Id,$EmployeeId)
	{
	  try{
	  $stmt =getCnx()->prepare("UPDATE washservicedetails set EmploymentId =:EmployeeId
			WHERE WashServiceId =:Id ");

	  $stmt->bindParam(':Id', $Wash_Id);
		$stmt->bindParam(':EmployeeId', $EmployeeId);
	  $stmt->execute();
	  } catch(PDOException $e){
	    echo $e;
	  }
	}
	function cancelWashService($Wash_Id)
	{
	  try{
	  $stmt =getCnx()->prepare("UPDATE washservice set ServiceStatusId =3
			WHERE WashServiceId =:Id");

	  $stmt->bindParam(':Id', $Wash_Id);
	  $stmt->execute();
	  } catch(PDOException $e){
	    echo $e;
	  }
	}




	function updateOilService($Wash_Id)
	{
	  try{
	  $stmt =getCnx()->prepare("UPDATE service set ServiceStatusId =2
			WHERE ServiceId =:Id ");

	  $stmt->bindParam(':Id', $Wash_Id);
	  $stmt->execute();
	  } catch(PDOException $e){
	    echo $e;
	  }
	}

	function updateOilServiceDetailsEmployee($Wash_Id,$EmployeeId)
	{
	  try{
	  $stmt =getCnx()->prepare("UPDATE servicedetails set EmploymentId =:EmployeeId
			WHERE ServiceId =:Id ");

	  $stmt->bindParam(':Id', $Wash_Id);
		$stmt->bindParam(':EmployeeId', $EmployeeId);
	  $stmt->execute();
	  } catch(PDOException $e){
	    echo $e;
	  }
	}
	function cancelOilService($Wash_Id)
	{
	  try{
	  $stmt =getCnx()->prepare("UPDATE service set ServiceStatusId =3
			WHERE ServiceId =:Id");

	  $stmt->bindParam(':Id', $Wash_Id);
	  $stmt->execute();
	  } catch(PDOException $e){
	    echo $e;
	  }
	}

	/*****************   AMINO   **************************/
	

	function employeeWork($startDate,$endDate)
	{

	      $stmt=getCnx()->prepare("SELECT COUNT(ws.EmploymentId) as employeeWash,p.*,v.Plate,w.SubTotal,
																w.Discount,w.Total,pm.PaymentMethodName FROM washservicedetails ws
																INNER JOIN washservice w
																ON w.WashServiceId=ws.WashServiceId
																INNER JOIN employment e
																ON e.EmploymentId=ws.EmploymentId
																INNER JOIN person p
																ON p.PersonId=e.PersonId
															  INNER JOIN vehicle v
															  ON v.VehicleId=w.VehicleId
															  INNER JOIN paymentmethod pm
															  ON pm.PaymentMethodId=w.PaymentMethodId
																WHERE w.ServiceDate BETWEEN  :startDate AND :endDate
																GROUP BY ws.EmploymentId,v.Plate,w.SubTotal,
																w.Discount,w.Total,pm.PaymentMethodName");

		    $stmt->bindParam(':startDate', $startDate);
	      $stmt->bindParam(':endDate', $endDate);
	      $stmt->execute();
	        return $stmt;
	}

	function washserviceDetailsByEmployee($employeeId)
	{
		$stmt=getCnx()->prepare("SELECT w.*,v.Plate,pm.PaymentMethodName FROM washservice w
														INNER JOIN washservicedetails ws
														ON w.WashServiceId=ws.WashServiceId
														INNER JOIN vehicle v
														on v.VehicleId=w.VehicleId
                            INNER join paymentmethod pm
                            on pm.PaymentMethodId=w.PaymentMethodId
														WHERE ws.EmploymentId=:eId");

	 $stmt->bindParam(':eId', $employeeId);
	 $stmt->execute();
		 return $stmt;
	}
	function oilserviceDetailsByEmployee($employeeId)
	{
		$stmt=getCnx()->prepare("SELECT s.*,v.Plate,pm.PaymentMethodName FROM Service s
														INNER JOIN servicedetails sd
														ON sd.ServiceId=s.ServiceId
														INNER JOIN vehicle v
														on v.VehicleId=s.VehicleId
                            INNER join paymentmethod pm
                            on pm.PaymentMethodId=s.PaymentMethodId
														WHERE sd.EmploymentId=:eId");

	 $stmt->bindParam(':eId', $employeeId);
	 $stmt->execute();
		 return $stmt;
	}

?>
