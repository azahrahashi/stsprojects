<?php
require_once '../Connection/dbconection.php';
function GetEmployeeList(){
$stmt=getCnx()->query("SELECT employment.EmploymentId, person.FirstName,LastName,Gender,Email,person.StartDate FROM employment INNER JOIN Person ON employment.PersonId = person.PersonId WHERE FinishDate IS Null");

  return $stmt;

}

function getAllEmployee(){
 $stmt=getCnx()->query("SELECT count(EmploymentId) as employment  FROM  employment");
 if (!empty($stmt)) {
  while ($row=$stmt->fetch()) {
    if ($row[0] == 0) {
      return 0;
    }
    return $row[0];
  }
}
return 0;
}

function getAllSallery($fDay,$date){
 // echo "SELECT SUM(Amount) as salarydetails  FROM  salarydetails WHERE (createdDate BETWEEN '".$fDay."' AND  '".$date."')";
 $stmt=getCnx()->query("SELECT SUM(Amount) as salarydetails  FROM  salarydetails WHERE (createdDate BETWEEN '".$fDay."' AND  '".$date."')");
 if (!empty($stmt)) {
  while ($row=$stmt->fetch()) {
    if ($row[0] == 0) {
      return 0;
    }
    return $row[0];
  }
}
return 0;
}


function AllPurchases($fDay,$date){

  $stmt=getCnx()->query("SELECT SUM(TotalAmount) as purchase  FROM  purchase WHERE (PurchaseDate BETWEEN '".$fDay."' AND  '".$date."')");
  if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    if ($row[0] == 0) {
      return 0;
    }
    else{
      return $row[0];
    }
  }
}
return 0;
}


function getAllSallesAmount($fDay,$date){

  $stmt=getCnx()->query("SELECT SUM(TotalAmount) as selles  FROM  selles WHERE (SellesDate BETWEEN '".$fDay."' AND '".$date."')");
  if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    if ($row[0] == 0) {
      return 0;
    }
    return $row[0];
  }
}
return 0;
}

function getAllCustomerAmount(){

 $stmt=getCnx()->query("SELECT count(CustomerId) as customer  FROM  customers");
 if (!empty($stmt)) {
  while ($row=$stmt->fetch()) {
    if ($row["customer"] == 0) {
      return 0;
    }
    return $row["customer"];
  }
}
return 0;
}


function getAllExpensesAmount($fDay,$date){

  $stmt=getCnx()->query("SELECT sum(Amount) as Amount  FROM  expenses WHERE (StartDate BETWEEN '".$fDay."' AND  '".$date."')");
  if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    if ($row[0] == 0) {
      return 0;
    }
    return $row[0];
  }
}
return 0;
}


function getSalleryDetail(){

  $stmt=getCnx()->query("SELECT SD.SalaryType,SD.createdBy,SD.Amount,P.FirstName,P.LastName,SD.createdDate
                         FROM salarydetails SD
                         INNER JOIN person P ON P.PersonId = SD.empId
                         WHERE SD.status =1");
  return $stmt;
}


function getCustomerDetail(){

 $stmt=getCnx()->query("SELECT C.CustomerId,C.Name,C.Telephone,C.Email, SUM(S.TotalAmount) AS Amount, SUM(S.TotalPaid) AS PAID
                        FROM customers C
                        INNER JOIN selles S ON S.CustomerId = C.CustomerId
                        WHERE S.FinishDate IS NULL
                        GROUP BY C.CustomerId");

 return $stmt;
}


function getcustomerSal($customerId){

 $stmt=getCnx()->prepare("SELECT s.SellesDate,s.SellsID,s.createdBy,s.TotalAmount,s.TotalPaid,s.Blance,s.Status
                        FROM selles s
                        WHERE s.CustomerId=:customerId AND s.FinishDate is NULL");
                        $stmt->bindParam(':customerId', $customerId);

                              $stmt->execute();
                              return $stmt;
}

function getExpensesDetail(){

  $stmt=getCnx()->query("SELECT * FROM expenses");

  return $stmt;
}

function getPurchaseDetail(){

 $stmt=getCnx()->query("SELECT Amount, Discount, TotalAmount, PurchaseDate, WarehouseName, StartDate, ReceivedBy FROM purchase
  INNER JOIN warehouse ON warehouse.WarehouseId = purchase.whereHouse");

 return $stmt;
}

function getSallesBetweenDate($date, $toDate){
  $stmt=getCnx()->query("SELECT * FROM selles WHERE SellesDate BETWEEN '".$date."' AND '".$toDate."'");
  return $stmt;
}

function getSalleryBetweenDate($date, $toDate){
 $stmt=getCnx()->query("SELECT SD.SalaryType,SD.createdBy,SD.Amount,P.FirstName,P.LastName,SD.createdDate
                         FROM salarydetails SD
                         INNER JOIN person P ON P.PersonId = SD.empId
                         WHERE SD.status =1 AND createdDate BETWEEN '".$date."' AND '".$toDate."'");
 return $stmt;
}


function getCustomerBetweenDate($date, $toDate){
  $stmt=getCnx()->query("SELECT * FROM customers WHERE StartDate BETWEEN '".$date."' AND '".$toDate."'");
  return $stmt;
}


function getExpensesBetweenDate($date, $toDate){
 $stmt=getCnx()->query("SELECT * FROM expenses WHERE StartDate BETWEEN '".$date."' AND '".$toDate."'");
 return $stmt;
}


function getPurchasesBetweenDate($date, $toDate){
  $stmt=getCnx()->query("SELECT Amount, Discount, TotalAmount, PurchaseDate, WarehouseName, StartDate, ReceivedBy FROM purchase
   INNER JOIN warehouse ON warehouse.WarehouseId = purchase.whereHouse WHERE PurchaseDate BETWEEN '".$date."' AND '".$toDate."'");
  return $stmt;
}



function getAllSelles(){

  $stmt=getCnx()->query("SELECT * FROM selles");

  return $stmt;
}



function getAllPendingProjects(){

  $stmt=getCnx()->query("SELECT COUNT(ProjectId) as projects  FROM  projects  WHERE Status = 'pending'");
  if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    return $row['projects'];
  }
}
return 0;
}
function getAllProducts(){

  $stmt=getCnx()->query("SELECT COUNT(Id) AS Name FROM `Products`");
  if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    return $row['Name'];
  }
}
return 0;
}
function getAllMeterial(){

  $stmt=getCnx()->query("SELECT COUNT(Id) AS Name FROM `material`");
  if (!empty($stmt)) {
   while ($row=$stmt->fetch()) {
    return $row['Name'];
  }
}
return 0;
}
function VeiwProducts(){

  $stmt=getCnx()->query("SELECT * FROM `Products`");
  return $stmt;
}


function getProjectList(){

 $stmt=getCnx()->query("SELECT * FROM projects WHERE FinishDate IS NULL");
 return $stmt;
}
      // ...................... MATERIAL ........................

function getTotalPurchased(){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  materialstore WHERE `Status` = 'Purchased'");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}

function getTotalproduced(){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  materialstore WHERE `Status` = 'produced' ");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}


function CurentStoreMaterial(){

  $purchase = getTotalPurchased();
  $produced = getTotalproduced();

  return ($purchase-$produced);
}

       // ...................... PRODUCT ........................

function getTotalPurchased_Produdct(){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  productstock WHERE `Status` = 'Purchased'");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}

function getTotalproduced_Produdct(){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  productstock WHERE `Status` = 'produced'");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}

function getTotalSold_Produdct(){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  productstock WHERE `Status` = 'Sold'");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}


function CurentProductStore(){

  $purchase = getTotalPurchased_Produdct();
  $produced = getTotalproduced_Produdct();
  $sold= getTotalSold_Produdct();

  return ($purchase+$produced-$sold);
}


      // ...................... MATERIAL BY ID ........................

function getTotalPurchasedById($MaterialId){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  materialstore WHERE `Status` = 'Purchased' AND `MaterialId` = $MaterialId");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}

function getTotalproducedById($MaterialId){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  materialstore WHERE `Status` = 'produced' AND `MaterialId` = $MaterialId");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}


function CurentStoreMaterialById($MaterialId){

  $purchase = getTotalPurchasedById($MaterialId);
  $produced = getTotalproducedById($MaterialId);

  return ($purchase-$produced);
}

      // ...................... PRODUCT BY ID ........................

function getTotalPurchased_ProdudctById($ProductId){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  productstock WHERE `Status` = 'Purchased' AND `ProductId` = $ProductId");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}

function getTotalproduced_ProdudctById($ProductId){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  productstock WHERE `Status` = 'produced' AND `ProductId` = $ProductId");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}

function getTotalSold_ProdudctById($ProductId){

  $stmt=getCnx()->query("SELECT SUM(Qty) FROM  productstock WHERE `Status` = 'Sold' AND `ProductId` = $ProductId");
  if (!empty($stmt)) {
    while ($row=$stmt->fetch()) {
      return $row[0];
    }
  }
  return 0;
}

function CurentProductStoreById($ProductId){

  $purchase = getTotalPurchased_ProdudctById($ProductId);
  $produced = getTotalproduced_ProdudctById($ProductId);
  $sold= getTotalSold_ProdudctById($ProductId);

  return ($purchase+$produced-$sold);
}

      //  ...................... END BY ID...........................



?>
