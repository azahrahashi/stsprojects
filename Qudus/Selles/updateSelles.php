<html>
<?php 
require_once '../Include/head.php'; 
require_once 'SellesClass.php';
require_once 'ModalClass.php';
?>
<head>
	<title>Adminty - Premium Admin Template by Colorlib </title>

</head>
<div id="pcoded" class="pcoded">
	<div class="pcoded-overlay-box"></div>
	<div class="pcoded-container navbar-wrapper">
		<?php require_once '../Include/navbar.php'; ?>
		<div class="pcoded-main-container">
			<div class="pcoded-wrapper">
				<?php require_once '../Include/sidebar.php'; ?>

				<div class="pcoded-content">
					<div class="pcoded-inner-content">
						<div class="main-body">
							<div class="page-wrapper">
								<div class="page-header">
									<div class="row align-items-end">
										<div class="col-lg-8">
											<div class="page-header-title">
												<div class="d-inline">
												</div>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="page-header-breadcrumb">
												<ul class="breadcrumb-title">
													<li class="breadcrumb-item">
														<a href="index.html"> <i class="feather icon-home"></i> </a>
													</li>
													<li class="breadcrumb-item"><a href="#!">Home</a>
													</li>
													<li class="breadcrumb-item"><a href="#!">Manage Project</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>

								<div class="card" >
									<div class="card-header">
										<!-- <?php session_start();?> -->

										<?php require_once '../Include/head.php'; 
										require_once '../supplier/supplierClass.php';

										$total = 0;
										$items = 0;
										?>
										<body>


											<div class="pcoded-container navbar-wrapper">
												<?php require_once '../Include/navbar.php'; ?>

												<div class="pcoded-wrapper">
													<!-- <?php require_once '../Include/sidebar.php'; ?> -->

													<!-- <div class="pcoded-content"> -->
														<div class="pcoded-inner-content">

															<div class="row">
																<!-- halkaan waxaa ka bilaawanaayo tableka purchaseka  -->
																<div class="col-xl-6">
																	<div class="card">
																		<div class="card-header">
																			<div class="row">

																				<div class="col-lg-6">
																					<div class="input-group">
																						<input type="hidden" id="sHidden">
																						<select class="custom-select col-11" name="pCustomer" id="Customer">
																							<option id="Cus" selected="" value="" disabled="">Walkin-in Customer</option>
																							<?php 
																							$res=getAllCustomer();
																							while ($row=$res->fetch()) {
																								echo '
																								<option value="'.$row['CustomerId'].'"> '.$row['Name'].' </option>
																								';
																							}
																							?>
																						</select>
																					</div>
																					<span class="text-danger" id="CustomerErr"></span>
																				</div>

																				<button type="button" class="btn btn-info btn-lg 	pull-right" data-toggle="modal" data-target="#AddModal">Add New Customer</button>

																			</div>


																		</div>	

																		<div class="card-block">
																			<div class="table-responsive" id="cart" style="height: 133px; min-height: 278px;">
																				<table class="table table-bordered">
																					<tr>
																						<th width="40%">Product Name</th>
																						<th width="10%">Quantity</th>
																						<th width="20%">Price</th>
																						<th  width="15%">Total</th>
																						<th width="5%"><i class="fa fa-trash-o"></i></th>
																					</tr>
																					<tbody id="updateTable">
																						<?php
																						if (!empty($_SESSION['shopping_cart']))
																						{

																							foreach ($_SESSION['shopping_cart'] as $keys => $values) {
																								echo '
																								<tr>
																								<td>'.$values["product_name"].'</td>
																								<td align="right"><input id="prQty'.$values["product_id"].'" onblur="setSelles('.$values["product_id"].',1);" value="'.$values["product_qty"].'"></td>
																								<td align="right">$ '.$values["product_price"].'</td>
																								<td>$ '.number_format($values["product_qty"] * $values["product_price"],2).'</td>
																								<td><button name="delete" class="delete" id="'.$values["product_id"].'"><i class="fa fa-times tip pointer posdel"></i></button></td>
																								</tr>
																								';
																								$total = $total + ($values["product_qty"] * $values["product_price"]);
																								$items = $items +$values["product_qty"];
																							}
																							echo '
																							<tr>
																							<td colspan="3" align="right">Total</td>
																							<td id="subTotal" align="right">$ '.number_format($total,2).'</td>
																							<td></td>
																							</tr>
																							';
																						}

																						?>
																					</tbody>
																				</table>
																			</div>
																			
													<div class="card user-activity-card">
														<table id="totalTable" style="width:100%; float:right; padding:5px; color:#000; background: #FFF;">
															<tbody><tr>
																<td style="padding: 5px 10px;border-top: 1px solid #DDD;">Items</td>
																<td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;border-top: 1px solid #DDD;">
																	<span id="titems"><?php echo $items;?></span>
																</td>
																
															</tr>
															<tr>
															<td style="padding: 5px 10px;">Discount <a href="#" id="ppdiscount" tabindex="-1">
																<!-- <i class="fa fa-edit"></i> -->
															</a>
														</td>
														<td class="text-right" style="padding: 5px 10px;font-weight:bold;">
															<span id="tds"><input type="number" id="disc" name="disc" value="0.00" align="right" onblur="Discount(<?php echo $total;?>)";></span>
														</td>
													</tr>
													<tr>
														<td style="padding: 5px 10px; border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" colspan="2">
															Total Payable <a href="#" id="pshipping" tabindex="-1">
																<i class="fa fa-plus-square"></i>
															</a>
															<span id="tship"></span>
														</td>
														<td class="text-right" style="padding:5px 10px 5px 10px; font-size: 14px;border-top: 1px solid #666; border-bottom: 1px solid #333; font-weight:bold; background:#333; color:#FFF;" colspan="2">
															<span id="gtotal"><?php echo "$ ".$total;?></span>
														</td>
													</tr>
												</tbody></table>
											</div>
																	</div>
																	<div style="clear:both;"></div>
																	<div> 
																		<input type="button" name="" id="save" value="Submit" class="btn btn-success float-right" onclick="saveSelles()">
																	</div>
																</div>
															</div><!-- /end of purchase table -->

															<!-- halkaan waxaa ka bilaawan doono payments disply area -->



										<!-- <div class="col-xl-8 col-md-12">
											<p>hhhhhhhhhh</p>
										</div> -->

										<div class="col-xl-6">
											<div class="card">
												<div class="card-header">
													<h4>Products</h4>
												</div>
												<div class="card-block">
													<div class="row">
														<?php 
														$result=getProduct();
														while ($Productrow=$result->fetch()) {
															echo '
															<div style="height: 200px; min-height: 450px;">
															<div class="col-xl-4 col-lg-3 col-sm-3 col-xs-12">
															<button data-lightbox="example-set" type="button" class="btn btn-warning" data-container="body" tabindex="-1" onclick="setSelles('.$Productrow['Id'].',0)">
															<span>'.$Productrow['Name'].'</span>
															</button>
															<input type="hidden" name="hidden_qty" id="qty'.$Productrow['Id'].'" value="1"/>
															<input type="hidden" name="hidden_price" id="price'.$Productrow['Id'].'" value="'.$Productrow['UnitPrice'].'"/>
															<input type="hidden" name="hidden_name" id="name'.$Productrow['Id'].'" value="'.$Productrow['Name'].'"/>

															</div>
															</div>';
														}
														?>
													</div>
												</div>
											</div>
										</div> 
									</div>


								</div>
								<!-- </div> -->

							</div>
						</div>

						<?php require_once '../Include/script.php'; ?>
					</body>
					</html>
					<script>

						$(document).ready(function() {
							var sId=localStorage.getItem('SellsID');
							redirect(sId);
						});

						var i=actual_amount=quantity=price=0;var pId;
						var totalAmount=subtotal=0;
						var discount=0;
						var items=postItems=[];
					var total=Noqty=0;
var	Item=function (product_id,product_name,product_qty,product_price) {
		this.product_id=product_id;
		this.product_name=product_name;
		this.product_qty=product_qty;
		this.product_price=product_price;
	};
	function addItemToCart(id,name,qty,price) {
		for(var i in items){
			if (items[i].product_id==id){
				items[i].product_qty ++;
				return;
			}
		}
		var item= new Item(id,name,qty,price);
		items.push(item);
		//JSON.stringfy(item)
	}
						function redirect(SellsID) {
							//alert(SellsID);
							$("#sHidden").val(SellsID);
							$.post("redirect.php",{
								SellsID:SellsID
							},function(data,status){
								alert(data);

								var displySelles = JSON.parse(data);
								$("#Customer").val(displySelles.customerId);
					// $('#cart').html(displySelles.order_table);
					//items=displySelles.shop;
					// $('#prQty').text(displySelles.total);
					$('#gtotal').text(displySelles.total);
					$('#titems').text(displySelles.cart_item);//console.log(displySelles["shop"]);
					for (var i = 0; i < displySelles["shop"].length; i++) {
						addItemToCart(displySelles["shop"][i].product_id,displySelles["shop"][i].product_name,displySelles["shop"][i].product_qty,displySelles["shop"][i].product_price);
						// items.push(item);
						//console.log(displySelles["shop"][i].product_id);
					}
								$('#cart').html('<table class="table table-bordered">'+
																					'<tr>'+
																						'<th width="40%">Product Name</th>'+
																						'<th width="10%">Quantity</th>'+
																						'<th width="20%">Price</th>'+
																						'<th  width="15%">Total</th>'+
																						'<th width="5%"><i class="fa fa-trash-o"></i></th>'+
																					'</tr>'+
																					'<tbody id="updateTable">');
					for (var i = 0; i < displySelles["shop"].length; i++) {
								$('#updateTable').append(
								'<tr>'+
								'<td>'+displySelles["shop"][i].product_name+'</td>'+
								'<td align="right"><input id="prQty'+displySelles["shop"][i].product_id+'" onblur="setSelles('+displySelles["shop"][i].product_id+',1);" value="'+displySelles["shop"][i].product_qty+'"></td>'+
								'<td align="right">$ '+displySelles["shop"][i].product_price+'</td>'+
								'<td>$ '+Number(displySelles["shop"][i].product_qty * displySelles["shop"][i].product_price,2)+'</td>'+
								'<td><button name="delete" class="delete" id="'+displySelles["shop"][i].product_id+'"><i class="fa fa-times tip pointer posdel"></i></button></td>'+
								'</tr>');
								total = total + (displySelles["shop"][i].product_qty * displySelles["shop"][i].product_price);
								Noqty = Noqty +Number(displySelles["shop"][i].product_qty);
							}
							
							// $('#updateTable').append('<tr>'+
							// '<td colspan="3" align="right">Total</td>'+
							// '<td id="subTotal" align="right">$ '+Number(total)+'</td>'+
							// '<td></td>'+
							// '</tr>');
						console.log(Noqty);		
					});
				}
						function setSelles(e,flag) 
		{
							var product_id = e;
							var product_name = $('#name'+product_id).val();
							var product_price = $('#price'+product_id).val();
							var product_qty;
							if (flag==0){
								product_qty=$('#qty'+product_id).val();
							}
							if(flag==1){
								product_qty=$('#prQty'+product_id).val();
							}

							if(flag==2){
								product_qty=$('#qty'+product_id).val();
								//alert(product_qty);
							}
							addItemToCart(product_id,product_name,product_qty,product_price);
							console.log(items);
								$('#cart').html('<table class="table table-bordered">'+
																					'<tr>'+
																						'<th width="40%">Product Name</th>'+
																						'<th width="10%">Quantity</th>'+
																						'<th width="20%">Price</th>'+
																						'<th  width="15%">Total</th>'+
																						'<th width="5%"><i class="fa fa-trash-o"></i></th>'+
																					'</tr>'+
																					'<tbody id="updateTable">');
								Noqty=total=0;
					for (var i = 0; i < items.length; i++) {
						//$('#updateTable').empty();
								$('#updateTable').append(
								'<tr>'+
								'<td>'+items[i].product_name+'</td>'+
								'<td align="right"><input id="prQty'+items.product_id+'" onblur="setSelles('+items[i].product_id+',1);" value="'+items[i].product_qty+'"></td>'+
								'<td align="right">$ '+items[i].product_price+'</td>'+
								'<td>$ '+Number(items[i].product_qty * items[i].product_price,2)+'</td>'+
								'<td><button name="delete" class="delete" id="'+items[i].product_id+'"><i class="fa fa-times tip pointer posdel"></i></button></td>'+
								'</tr>');
								total = total + (Number(items[i].product_qty) * Number(items[i].product_price));
								Noqty = Noqty +Number(items[i].product_qty);
							}
							
							$('#updateTable').append('<tr>'+
							'<td colspan="3" align="right">Total</td>'+
							'<td id="subTotal" align="right">$ '+Number(total)+'</td>'+
							'<td></td>'+
							'</tr>');
						console.log(items);	
					$('#gtotal').text(total);
					$('#titems').text(Noqty);
			// var product_qty = $('#qty'+product_id).val();
			var action = "add";

		

		}

		function Discount(total){
			subtotal=total;
			discount = $('#disc').val();

			totalAmount = subtotal - discount;

			$('#gtotal').text("$ "+totalAmount+"");
		}

		function AmountPaid(){
			var totalAmount  = $('#totalAmount').val();

			var Paid = $('#Paid').val();

			var Blance = totalAmount - Paid;
			$('#Blance').val(Blance+".00");
		}
		$(document).on('click','.delete',function(){
			var product_id = $(this).attr('id');
			var act = "remove";
			if (confirm("Are you sure you want to remove this product?")) 
			{
				$.ajax({
					url:"action.php",
					method:"POST",
					data:{product_id:product_id, action:act},
					success:function(data){
						// alert(data);
						var parsedData= JSON.parse(data);
						$('#cart').html(parsedData.order_table);
						window.location.reload();
					}
				})
			}
			else
			{
				return false;
			}
		});
		function saveSelles() {
			var Customer = $('#Customer').val();
			subtotal = <?php echo $total;?>;
			// alert(subtotal)
			if (discount==0) 
			{
				totalAmount=subtotal;
			}
			if (Customer==null) 
			{
				// alert('Please select Customer');
				$('#CustomerErr').text('Please select Customer');
			}

			else{
				items = <?php if (!empty($_SESSION['shopping_cart'])) { echo json_encode($_SESSION['shopping_cart']); }else { echo 0;}?>;
				for (var i = items.length - 1; i >= 0; i--) {

					var info = items[i].product_id + "," + items[i].product_price + "," + items[i].product_qty;
					postItems.push(info);
				}

			// console.log(items);
			$.ajax({
				url:'addClass.php',
				method:'POST',
				data:{
					items:postItems,
					Customer:Customer,
					subtotal:subtotal,
					totalAmount:totalAmount,
					discount:discount
				},
				success:function(data) {
					// alert(data);
					window.location.replace('sellesList.php');
				}
			});
		}

	}
</script>
<style>
	input {
		border:0px;
		/*border-bottom:2px solid #999;*/
	}
</style>