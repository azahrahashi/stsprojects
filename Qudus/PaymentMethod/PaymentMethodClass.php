<?php 

require_once '../Connection/dbconection.php';
          // Get All Types Funtion
function getAllPaymentMethod(){

  $stmt=getCnx()->query("SELECT * FROM paymentmethod 
    WHERE FinishDate IS NULL ");
  return $stmt;
}

                 // Add Function
function addPaymentMethod($PaymentMethod,$StartDate)
{
  $stmt= getCnx()->prepare("INSERT INTO paymentmethod(PaymentMethod,StartDate)
    VALUES (:PaymentMethod, :StartDate)");

  $stmt->bindParam(':PaymentMethod', $PaymentMethod);
  $stmt->bindParam(':StartDate', $StartDate);


  $stmt->execute();

           // return getCnx()->lastInsertId();
}

             // Update Funtion
function updatePaymentMethod($PaymentMethodId,$PaymentMethod,$StartDate)
{
  $stmt =getCnx()->prepare("UPDATE paymentmethod SET  PaymentMethod=:PaymentMethod, StartDate=:StartDate WHERE  PaymentMethodId = :PaymentMethodId");

  $stmt->bindParam(':PaymentMethodId', $PaymentMethodId);
  $stmt->bindParam(':PaymentMethod', $PaymentMethod);
  $stmt->bindParam(':StartDate', $StartDate);
  $stmt->execute();
}

function terminatePaymentMethod($PaymentMethodId)
{
  $stmt =getCnx()->prepare("UPDATE paymentmethod SET  FinishDate=NOW() WHERE  PaymentMethodId = :PaymentMethodId");

  $stmt->bindParam(':PaymentMethodId', $PaymentMethodId);
  $stmt->execute();
}




?>