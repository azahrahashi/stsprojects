<?php

	function rTwoWords(){
		return "/^.*$/";
	}

	function rOneWord(){
		return "/[a-zA-Z]$/";
	}

	function rTelephone(){
		return "/[0-9]$/";
	}


	function bInteger(){
		return FILTER_VALIDATE_INT;
	}

	function bDecimal(){
		return FILTER_VALIDATE_FLOAT;
	}
	function bEmail(){
		return FILTER_VALIDATE_EMAIL;
	}

	function rDate(){
		return "/^.*$/";
	}
?>