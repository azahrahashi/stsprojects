<?php
if (isset($_POST['SellsID']) && isset($_POST['SellsID']) !=""){
  $SellsID=$_POST['SellsID'];
  $sellesinfo=array();
  $customerId;
 $order_table = '';
   $message = '';
  $total = 0;
  $items = 0;
  require_once('SellesClass.php');
        $customerId=getCustomerById($SellsID);
        
    
  $result =get_selles_By_ID($SellsID);
  if ($result->rowcount()>0) {
    while ($row=$result->fetch()) {
      // $sellesinfo = $row;
            $item_array  = array(
               'product_id' => $row['pId'],
               'product_name' => $row['productName'],
               'product_price' => $row['Price'],
               'product_qty' => $row['Qty']
            );
            $_SESSION['shopping_cart'][] = $item_array;
    }
    $order_table .='
    <table class="table table-bordered">
    <tr>
    <th width="40%">Product Name</th>

    <th width="10%">Quantity</th>
    <th width="20%">Price</th>
    <th  width="15%">Total</th>
    <th width="5%"><i class="fa fa-trash-o"></i></th>
    </tr>';
    if (!empty($_SESSION['shopping_cart'])) 
    {
      foreach ($_SESSION['shopping_cart'] as $keys => $values) {
        $order_table.='
        <tr>
        <td>'.$values["product_name"].'</td>
        <td align="right"> <input id="prQty'.$values["product_id"].'" onblur="setProduct('.$values["product_id"].',1);" value="'.$values["product_qty"].'"></td>
        <td align="right">$ '.$values["product_price"].'</td>
        <td>'.number_format($values["product_qty"] * $values["product_price"],2).'</td>
        <td><button name="delete" class="delete" id="'.$values["product_id"].'"><i class="fa fa-times tip pointer posdel"></i></button></td>
        </tr>
        ';
        $total = $total + ($values["product_qty"] * $values["product_price"]);
        $items = $items +$values["product_qty"];
      }
      $order_table.='
      <tr>
      <td colspan="3" align="right">Total</td>
      <td align="right">'.number_format($total,2).'</td>
      <td></td>
      </tr>
      ';
    }
    $order_table .='</table>';
    $output  = array(
      'order_table' => $order_table,
      'cart_item' => $items,
      'total' => $total,
      'customerId' =>$customerId,
      'shop' =>$_SESSION['shopping_cart']
    );
// header("Content-Type: application/json; charset=UTF-8");
    echo json_encode($output);
      }
  }
  else {
    $sellesinfo['status'] = 200;
    $sellesinfo['message'] = "data not found";
  }
  // echo json_encode($sellesinfo);
 ?>
