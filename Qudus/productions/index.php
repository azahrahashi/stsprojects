<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Productions</h4>
														<!-- 	<span>Lorem ipsum dolor sit <code>amet</code>, consectetur adipisicing elit</span> -->
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">

											<div class="card">
												<div class="card-header">
													<h5>Sample Production Analysis</h5>
													<!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
													<div class="card-header-right">
														<i class="icofont icofont-spinner-alt-5"></i>
													</div>
												</div>
												<div class="card-block">
													<h4 class="sub-title">Sample Rate Analysis</h4>
														<div class="form-group row">
															<label class="col-sm-2 col-form-label">Choose Product</label>
															<div class="form-radio" id="products">


															</div>

														</div>
												</div>

												<div class="card-block" id="product-Form">

												</div>

												<div class="form-group row">
													<div class="col-md-6">
														<label class="col-sm-2 col-form-label text-danger" id="err"></label>
													</div>
													<div class="col-md-6">
														<label class="col-sm-2 col-form-label float-righ" id="prod"></label>
													</div>
								        		</div>

												<div class="form-group row">
													<div class="col-md-6">
														<!-- <label class="col-sm-2 col-form-label float-righ" id="prod"></label> -->
													</div>
													<div class="col-md-6">
														<!-- <label class="col-sm-2 col-form-label float-righ" id="prod"></label> -->
														<button class="btn btn-success float-righ" onclick="produce();">Produce</button>
													</div>
								        		</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script type="text/javascript">
	var materials=[];
	var shamito=jaay=carro=sm=shId=jId=cId=0;
	var flag=0;
	var info="";
	function checkField(e, id,flag) {
		flag=flag;
		if (flag==0) {
		var FieldTxt = $(e).val();
		if (FieldTxt == "") {
			$("#err").text("Please fill all fields");
		}
		else{
					 $("#err").text("");
			$.ajax({
				url: "getProductions.php",
				method: "POST",
				data:{FieldTxt:FieldTxt, id:id},
				success:function(data) {
					if(id==1){
						shId=id;
						shamito=data;
					 sm=getSmallest(shamito,jaay,carro);
					}
					if(id==2){
						jId=id;
						jaay=data;
					 sm=getSmallest(shamito,jaay,carro);
					}
					if(id==3){
						cId=id;
						carro=data;
						console.log(carro);
					info=shId+ "," + shamito+"," +jId+ "," + jaay+"," +cId+ "," + carro;
						materials.push(info);
						sm=getSmallest(shamito,jaay,carro);
					}
					for(var i = 0; i < materials.length; ++i){
   						 materials[i] = materials[i].replace(/(\r\n|\n|\r)/gm,"");
   						}
					console.log(materials);
					$("#prod").text(sm+" Bulkeeti");
				}
			});
		}
		}
	}
	function produce(argument) {
		//alert(sm);
		if (flag==0) {
		shamito=parseInt(sm*0.25);
		jaay=parseInt(sm*0.5);
		carro=parseInt(sm*0.75);
		// alert(shamito);
			$.ajax({
				url: "insertProduction.php",
				method: "POST",
				data:{
					materials:materials,
					product:"Bulkeeti",
					qty:sm,
					flag:flag
					},
				success:function(data) {
					alert(data);
					location.replace("productionList.php");
					//$("#products").html(data);
				}
			});
		}
	}
function getSmallest(shamito,jaay,carro) {
	//alert(shamito+','+jaay+','+carro);
	return Math.min(shamito,jaay,carro);
}
	$(document).ready(function() {




		getProducts();

	});
	function getProducts() {
			var product = "";
			$.ajax({
				url: "getProductions.php",
				method: "POST",
				data:{product:product},
				success:function(data) {
					$("#products").html(data);
				}
			});
		}
		$(document).on("click", ".Jibsi", function () {

		});

		$(document).on("click", ".Bulkeeti", function () {
				var Bulkeeti = "";
				$.ajax({
					url: "getProductions.php",
					method: "POST",
					data:{Bulkeeti:Bulkeeti},
					success:function(data) {
						$("#product-Form").html(data);
					}
				});
		});
</script>
