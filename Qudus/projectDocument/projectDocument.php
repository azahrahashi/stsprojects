<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>


					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Project Document</h4>
														<span>This Is The <code>Project Document</code> Management Page.</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">

													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">

										</div>

										<div class="card-block">
											<div class="row">
												<div class="col-sm-6">
													<select class="form-control m-b-10" id="myid">
														<option selected="" value="0" disabled="">Select Card</option>
														<?php
														require_once 'projectDocumentClass.php';
														$result = getprojectsName();
														if (!empty($result))
														{
															while ($row=$result->fetch()) {
																echo '<option value ="' . $row["ProjectId"] . '">' . $row["Name"]. '</option>';
															}
														}
														?>
													</select>
												</div>
											</div>

											<div id="">
												<table id="lists" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Project No</th>
															<th>Total Quantity</th>
															<th>Total Amount</th>
															<th>Salles Date</th>
															<th style="width: 20px;">Action</th>
														</tr>
													</thead>
													<tbody>

													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>




						<!-- Modal -->
						<div class="modal fade" id="DetailModel" tabindex="-1" role="dialog">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">Project Document Details</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<h5>Details</h5>
										<div id="Showtheresult" style="padding:10px; margin-left: 10%;">


										</div>
										<!-- <br> -->
									</div>
								</div>
							</div>
						</div>

						<!-- End Modal -->


					</div>
					<!--  -->

					<?php require_once '../Include/script.php'; ?>
				</body>
				</html><script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

				<script type="text/javascript">
				// var ProjectId = document.getElementById('myid');
				$(document).ready(function() {
					$("#myid").on('change', function(){
						var ProjectId = $("#myid").val();

						$.ajax({
							url: "GetProjectDocument.php",
							method: "POST",
							data: {ProjectId: ProjectId},
							success: function (data) {
								$("tbody").html(data);
							}
						});
					});

				});


				function projectDetail() {
					var ProjectId = $("#myid").val();
						// alert(ProjectId);

						$.post("GetProjectDocumentDetail.php", {
							ProjectId: ProjectId,
						},function (data, status) {
	                // alert(data)
	                $('#Showtheresult').html(data);
	                $("#DetailModel").modal("show");
	            });
					}





				</script>
				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
				<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
				<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
				<script type="text/javascript">
				$(document).ready(function() {
					$('#lists').DataTable();
				});
				</script>
