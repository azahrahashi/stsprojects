<?php
	
	function test_input($data) {
	  $data = trim($data);
	  $data = stripslashes($data);
	  $data = htmlspecialchars($data);
	  
	  return $data;
	} // end of function

	function validateField($vToBeChecked, $regExp, &$whereToDisplayError, &$booleanError, $regMessageToDisplay, $checkForEmpty, $buildIn=false)
	{

		$value = "";
		if (!empty($vToBeChecked))
		{
			$value = test_input($vToBeChecked);
			if ($buildIn){
				if (filter_var($vToBeChecked, $regExp) === false)
				{
					$whereToDisplayError = $regMessageToDisplay;
				  	$booleanError = true;
				}
			}
			else {
				if (!preg_match($regExp,$value)){
			  	$whereToDisplayError = $regMessageToDisplay;
			  	$booleanError = true;
			  }
			}
		}
		else
		{
			if($checkForEmpty) {
				$whereToDisplayError ="Must NOT be empty";
				$booleanError = true;
			}
		}

		return $value;


	} // End of function
	
	function formatDate($dateToFormat)
	{
		$dateInfo = date_parse_from_format('m/d/Y', $dateToFormat);
		return $dateInfo['year'] . '-' . $dateInfo['month']. '-' . $dateInfo['day'];	
	}
	
	function validateHiddenFeilds($vToBeChecked, $sTitle)
	{
		if (isset($vToBeChecked) && !empty($vToBeChecked))
			return test_input($vToBeChecked);
		else
			displayErrorMessage($sTitle, "Ooops! Something went wrong. Please contact your system administrator");
	}

	function displayErrorMessage($sTitle, $sMessage)
	{ 	?>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
		<h1 class="center"><?php echo $sTitle;?></h1>
		  <div class="middle80">
			<div class="confirmationContainer">
				<div class="registerFormRowContainer">
					<img class="greenTick" src="../images/redX.jpg" alt="Picture" />
					<div class="bigLineHeight bigBadding biggerFont redText center"><?php echo $sMessage;?></div>
				</div>
				<div class="registerFormRowContainer">
					<input id="backButton" class="center btn-lg btn-danger" type="reset" name="clear" value="<< Go Back"></input>
				</div>
				<div><br /></div>
			</div>
		</div>
		<?php
		exit();
	} // end function	
?>