<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; 
require_once '../supplier/supplierClass.php';
require_once 'PurchasesClass.php';
?>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">

							<div class="main-body">
								<div class="page-wrapper">

									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Clone Elements</h4>
														<span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">
														<li class="breadcrumb-item">
															<a href="index.html"> <i class="feather icon-home"></i> </a>
														</li>
														<li class="breadcrumb-item"><a href="#!">Ready To Use</a>
														</li>
														<li class="breadcrumb-item"><a href="#!">Clone Elements</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<!-- halkaan waxaa ka bilaawanaayo tableka purchaseka  -->
										<div class="col-xl-8 col-md-12">
											<div class="card">
												<div class="card-header">
													<div class="row">

														<div class="col-sm-12 col-lg-6">
															<div class="input-group">
																<select class="custom-select col-12" name="pSupplier" id="pSupplier">
																	<option selected="" value="" disabled="">Select Supplier</option>
																	<?php 
																	$res=getstAllsuppliers();
																	while ($row=$res->fetch()) {
																		echo '
																		<option value="'.$row['SupplierId'].'"> '.$row['CompanyName'].' </option>
																		';
																	}
																	?>
																</select>



															</div>
														</div>
														<div class="col-sm-12 col-lg-6">
															<div class="input-group">
																<input class="form-control input-sm" type="date" name="">
															</div>
														</div>
														<div class="col-sm-12 col-lg-6">
															<div class="input-group">
																<input type="hidden" name="total_item" id="total_item" value="2">

															</div>
														</div>
													</div>


													<div class="row">

														<div class="col-sm-4">
															<label class="col-sm-4 col-form-label">Location</label>
															<div class="col-sm-8">
																<select class="custom-select col-12" name="location">
																	<option selected="" value="" disabled="">Select Location</option>
																	<option value="1"> Olow Tower</option><option value="2"> Crown house</option>          </select>

																</div>
															</div>
															<div class="col-sm-4">
																<label class="col-sm-6 col-form-label">Material</label>
																<div class="col-sm-8">
																	<select class="custom-select col-12" name="status" onchange="setProduct(this);">
																		<option selected="" value="" disabled="">Select Material</option>
																		<?php 
																		$result=getMaterials();
																		while ($Materialrow=$result->fetch()) {
																			echo '
																			<option value="'.$Materialrow['Id'].'"> '.$Materialrow['Name'].' </option>
																			';
																		}
																		?>

																	</select>

																</div>
															</div>

															<div class="col-sm-4">
																<label class="col-sm-6 col-form-label">Status</label>
																<div class="col-sm-8">
																	<select class="custom-select col-12" name="status">
																		<option selected="" value="" disabled="">Select Status</option>
																		<option value="1">Ordered</option>
																		<option value="2">Recived</option>
																		<option value="3">Pending</option>

																	</select>

																</div>
															</div>
														</div>
											<!-- <input class="form-control input-sm" type="text" id="Barcode" value=""  placeholder="Scan Barcode Here" onkeyup="myFunction()">
											-->
										</div>
										<div class="card-block">
											<div class="table-responsive">
												<table class="table table-striped table-bordered" id="tblPurchase">
													<thead>
														<tr>
															<th>Id</th>
															<th width="30%">Product</th>

															<th>QTY</th>
															<th>Unit</th>
															<th>Unit Price</th>
															<th>Total</th>
															<th width="5%">Delete</th>
														</tr>
													</thead>
													<tbody id="items">

													</tbody>
												</table>
											</div>
											<button type="button" class="btn btn-primary waves-effect waves-light add" id="add" onclick="newItem();">Add Row </button>
											<!-- <button type="button" class="btn btn-succed waves-effect waves-light add"  id="save">Save Purchase </button> -->
											<input type="submit" name="create_invoice" id="create_invoice" class="btn btn-info" value="Save Purchase">
										</div>
									</div>
								</div><!-- /end of purchase table -->

								<!-- halkaan waxaa ka bilaawan doono payments disply area -->

								<div class="col-xl-4 col-md-12">
									<div class="card user-activity-card">

										<div class="user-card-block card">
											<div class="card-block">

												<div class="price-form">

													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<label for="amount_amirol" class="control-label">Sub Total </label>

															</div>
															<div class="col-sm-6">

																<!-- <p class="price lead" id="total"></p> -->
																<input class="form-control text-center" type="text" name="g-Total" id="subTotal" readonly="readonly" value="0.00">
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<label for="amount_amirol" class="control-label">Discount</label>

															</div>
															<div class="col-sm-6">
																<input type="hidden" id="amount_amirol" class="form-control">
																<!-- <p class="price lead" id="total12"></p> -->
																<input class="form-control input-sm" name="totalprice12" type="text" id="Discount" value="0.00" onkeyup="Discount()">
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<label for="amount_amirol" class="control-label">Total Amount </label>

															</div>
															<div class="col-sm-6">
																<input type="hidden" id="amount_amirol" class="form-control">
																<!-- <p class="price lead" id="total52"></p> -->
																<input class="form-control input-sm" name="totalprice52" type="text" id="totalAmount" disabled="disabled" value="0.00">
															</div>
														</div>
													</div>

													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<label for="amount_amirol" class="control-label">Paid Amount</label>

															</div>
															<div class="col-sm-6">
																<input type="hidden" id="amount_amirol" class="form-control">
																<!-- <p class="price lead" id="total52"></p> -->
																<input class="form-control input-sm" name="totalprice52" type="text" id="Paid" value="0.00" onkeyup="AmountPaid()">
															</div>
														</div>
													</div>

													<div class="form-group">
														<div class="row">
															<div class="col-sm-6">
																<label for="amount_amirol" class="control-label">Blance</label>

															</div>
															<div class="col-sm-6">
																<input type="hidden" id="amount_amirol" class="form-control">
																<!-- <p class="price lead" id="total52"></p> -->
																<input class="form-control input-sm" name="totalprice52" type="text" id="Blance" disabled="disabled" value="0.00">
															</div>
														</div>
													</div>
													<div style="margin-top:30px"></div>
													<hr class="style">

													<div class="form-group">
														<div class="col-sm-12">
															<input type="hidden" name="total_item" id="total_item" value="0">
															<button type="submit" class="btn btn-primary btn-lg btn-block">Proceed <span class="glyphicon glyphicon-chevron-right"></span></button>
														</div>
													</div>


												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-xl-8 col-md-12">
								</div>

								<div class="col-xl-4 col-xl-offset-8 col-md-12">
									<div class="card">
										<div class="card-header">
											<h4>Materials</h4>
										</div>
										<div class="card-block">
											<div class="row">
												<?php 
												$result=getMaterials();
												while ($Materialrow=$result->fetch()) {
													echo '

													<div class="col-xl-4 col-lg-3 col-sm-3 col-xs-12">
													<a href="#" data-lightbox="example-set" type="button" class="btn btn-warning" onclick="setProduct('.$Materialrow['Id'].')">
													'.$Materialrow['Name'].'
													</a>
													</div>';
												}
												?>
											</div>
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script>

	// function newItem() {
	// 	$('#newProduct'+i).hide();
	// 	$('#items').append(
	// 		'<tr>'+
	// 		'<td></td>'+
	// 		'<td></td>'+
	// 		'<td></td>'+
	// 		'<td></td>'+
	// 		'<td></td>'+
	// 		'<td></td>'+
	// 		'<td></td>'+
	// 		'</tr>');

	// 	i++;
	// }
	var i=actual_amount=quantity=price=0;var pId;
	var ids=[];var separate;
	var	Item=function (id,name,qty,unit,price) {
		this.id=id;
		// this.name=name;
		// this.qty=qty;
		// this.unit=unit;
		// this.price=price;
	};
	function addItemToCart(id,name,qty,unit,price) {
		var item= new Item(id,name,qty,unit,price);
		ids.push(item);
	}
		function contains(arr, element) {
			console.log(arr.length);
			// console.log(arr.length);
			// console.log(arr[k].length);
		    for (var k = 0; k <= arr.length; k++) {
			console.log(arr);
		    	if (arr.length==0) {
		    		return 0;
		    	}else{
			console.log("WAa keey"+arr[k].id);
		        if (arr[k].id == element) {
		            return element;
		        }
		    return 0;
		}
		}
		    // console.log("Elements "+element);
} 
	function setProduct(e) {
		// alert($(e).val());
		var Id=e;
			pId=$("#pId"+i).val();
		var quantity =  parseFloat($("#pQty"+i).val());
		var price = parseFloat($("#price"+i).val());

		$.post("getProduct.php", {
			Id:Id
		},
		function (data, status) {
			var productId="";
			$.each(data, function(key, value){ 
				productId=value.Id;
			// alert("WAa "+price);
			// console.log(productId+','+pId+','+i);

			alert(contains(ids,productId));
			if (contains(ids,productId) == pId) {
				quantity = quantity+1;
				$("#pQty"+i).val(quantity);
			actual_amount = quantity * price;
				// alert(price);



			} 
			else{
				var qty=(parseFloat(value.Unit))*(parseFloat(value.UnitPrice));
				// var	items=value.Id+","+value.Name+","+value.Unit+","+value.UnitPrice+","+qty;
				// items['dd']=value.Id;
				// items['Name']=value.Name;
				// items['Unit']=value.Unit;
				// items['UnitPrice']=value.UnitPrice;
				// items['qty']=value.qty;
				// +","+value.Name+","+value.Unit+","+value.UnitPrice+","+qty;
				// ids.push(items);
				// console(ids+'');
				i=i+1;
				$('#items').append(
					'<tr>'+
					'<td><input type="text" id="pId'+i+'" value="'+value.Id+'"  style="width:50%"></td>'+
					'<td><input type="text" id="pName'+i+'" value="'+value.Name+'" style="width:50%"></td>'+
					'<td><input type="text" id="pQty'+i+'" value="1" style="width:50%"></td>'+
					'<td><input type="text" id="unit'+i+'" value="'+value.Unit+'" style="width:50%"></td>'+
					'<td><input type="text" id="price'+i+'" value="'+value.UnitPrice+'" style="width:50%"></td>'+
					'<td><input type="text" id="total'+i+'" value="'+value.UnitPrice+'" style="width:50%"></td>'+
					'<td><input type="text" id="remove'+i+'" value="1" style="width:50%"></td>'+
					'</tr>');
				addItemToCart(value.Id,value.Name,value.Unit,value.Unit,value.UnitPrice);
			// window.location.reload();

		} 
				// console.log(ids);

		quantity =  parseFloat($("#pQty"+i).val());
		price = parseFloat($("#price"+i).val());
		actual_amount = quantity * price;
		$('#total'+i).val(actual_amount+".00");
                $('#total').val(actual_amount+".00");
                var table = document.getElementById("tblPurchase"), sumVal = 0;
                for (var j = 0; j < i; j++) 
                {
                	sumVal = sumVal + actual_amount;
                }
                $('#subTotal').val(sumVal+".00");
            });
		});

	}
// 	 var count = 0;
// function getProduct(e){
//   var code =$(e).val();

//    $.post("getProduct.php", {
//             code: code
//         },
//         function (data) {
//             var id="";

//            $.each(data, function(key, value){           
//             // qabso idka product kast si aad u darsato row kasta una hesho rowkasta iyo product ku jira idkiiisa 
//             var id = value.ProductId; 
//             var quantity =  parseFloat($("#order_item_quantity"+count).val());
//             var price = parseFloat($("#order_item_price"+count).val());
//            if (id == $("#Product_Id"+count).val()) {
//               var newqty = quantity+1;
//               $("#order_item_quantity"+count).val(newqty);

//                   actual_amount = newqty * price;
//                  $('#order_item_actual_amount'+count).text(actual_amount+".00");
//                 $('#hidden_item_actual_amount'+count).val(actual_amount);
//                 $('#subTotal').val(actual_amount+".00");
//                 var table = document.getElementById("tblSales"), sumVal = 0;
//                for (i = 1; i < table.rows.length; i++) 
//                {
//                   sumVal = sumVal + parseInt(table.rows[i].cells[5].innerHTML);
//                }
//                $('#subTotal').val(sumVal+".00");

//             }         
//            else 
//            {          
//           count = count + 1;
//           var html_code = '<tr id="row_id_'+count+'">';
//           html_code += '<td><span id="sr_no">'+count+'</span></td>';
//           html_code+='<td><input type="text" name="Product_Id[]" id="Product_Id'+count+'" value='+value.ProductId+' data-srno="1" class="form-control input-sm Product_Id" /></td>'
//           html_code += '<td><input type="text" name="item_name[]" id="item_name'+count+'" value='+value.ProductName+' class="form-control input-sm" /></td>';
//           html_code += '<td><input type="text" name="order_item_quantity[]" id="order_item_quantity'+count+'" value='+'1'+' class="form-control input-sm" /></td>'; 
//           html_code += '<td><input type="text" name="order_item_price[]" id="order_item_price'+count+'" value='+value.SellPrice+' class="form-control input-sm" /></td>'; 
//           html_code += '<td id="order_item_actual_amount'+count+'">0.00</td>';  
//           html_code += "<td>"
//          +'<input type="hidden" name="order_item_actual_amount[]" id="hidden_item_actual_amount'+count+'" value="0" >'
//          +"<button type='button' name='remove' data-row='row"+count+"' class='btn btn-danger btn-xs remove'><span class='icofont icofont-ui-delete'></span></button></td>";   
//          html_code += "</tr>";  
//         $('#tblSales').append(html_code);
//          var quantity =  parseFloat($("#order_item_quantity"+count).val());
//                 price = parseFloat($("#order_item_price"+count).val());
//           actual_amount = quantity * price;
//                  $('#order_item_actual_amount'+count).text(actual_amount+".00");
//                 $('#hidden_item_actual_amount'+count).val(actual_amount);
//                  $('#subTotal').val(actual_amount+".00");
//                 var table = document.getElementById("tblSales"), sumVal = 0;
//                for (i = 1; i < table.rows.length; i++) 
//                {
//                   sumVal = sumVal + parseInt(table.rows[i].cells[5].innerHTML);
//                }
//                $('#subTotal').val(sumVal+".00");
//          }
//         });// end of each loop 
//      });// end of Post get Product 
// }// end of get Product function
//  // markasta remove button click la siiyo ka jar row
//  $(document).on('click', '.remove', function(){
//   // var delete_row = $(this).data("row");
//   // $('#' + delete_row).remove();
//   count = count - 1;
// $('#total_item').val(count);
//    var table = document.getElementById("tblSales"), sumVal = 0;
//                for (i = 1; i < table.rows.length; i++) 
//                {
//                   sumVal = sumVal + parseInt(table.rows[i].cells[5].innerHTML);
//                }
//                $('#subTotal').val(sumVal+".00");
//                $('#totalAmount').val(Blance+".00");
//                $('#Blance').val(totalAmount+".00");
//    $(this).closest('tr').remove();
//      return false;
//  });



function Discount(){
	var subTotal  = $('#subTotal').val();

	var Discount = $('#Discount').val();

	var totalAmount = subTotal - Discount;
	$('#totalAmount').val(totalAmount+".00");
}

function AmountPaid(){
	var totalAmount  = $('#totalAmount').val();

	var Paid = $('#Paid').val();

	var Blance = totalAmount - Paid;
	$('#Blance').val(Blance+".00");
}

</script>
			<style>
			input {
				border:0px;
				border-bottom:2px solid #999;
			}
		</style>