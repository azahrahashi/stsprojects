<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<body>
  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">
      <?php require_once '../Include/navbar.php'; ?>
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <?php require_once '../Include/sidebar.php'; ?>

          <div class="pcoded-content">
            <div class="pcoded-inner-content">
              <div class="main-body">
                <div class="page-wrapper">
                  <div class="page-header">
                    <div class="row align-items-end">
                      <div class="col-lg-8">
                        <div class="page-header-title">
                          <div class="d-inline">
                            <h4>Manage Your Password</h4>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>


                  <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST"){
                      require_once("password.php");
                      $new_password = $_POST['new_password'];
                      $confime_password = $_POST['confime_password'];
                      if ($new_password === $confime_password) {
                        update_employee_password_by_user_id($_SESSION[getSessionName()], $confime_password);
                        echo "<script type='text/javascript'>
                          window.location.replace('../Reports/dashboard');
                        </script>";
                      }
                    }
                  ?>
                  <div class="card">
                    <div class="card-header">
                      <h5>Change your password</h5>
                    </div>
                    <div class="card-block">
                      <form id="main" method="post" action="" novalidate="">
                        <?php
                          $uname = "";
                          require_once("password.php");
                          $result = get_employee_password_by_user_id($_SESSION[getSessionName()]);
                          while ($row = $result->fetch()) {
                            $uname = $row['UserName'];
                            $password = $row['Password'];
                          echo '
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" name="name" id="name" placeholder="Username" value="'.$password.'">
                            <span class="messages"></span>
                          </div>
                        </div>
                        ';
                        }
                        ?>

                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">New Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="new_password" placeholder="Password">
                            <span class="messages"></span>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-2 col-form-label">Confime Password</label>
                          <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="confime_password" placeholder="Confime Password">
                            <span class="messages"></span>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label class="col-sm-2"></label>
                          <div class="col-sm-10">
                            <input type="submit" class="btn btn-primary m-b-0" name="change" value="Change Password">
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php require_once '../Include/script.php'; ?>
    </body>
    </html>
    <script type="text/javascript">



    </script>
