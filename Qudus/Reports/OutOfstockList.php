<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>

<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Products</h4>
														<span>Out stock <code>product </code> List</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">
														
													</ul>
												</div>
											</div>
										</div>
									</div>
									
									<div class="card">
										<div class="card-header">
										</div>

										<div class="card-block">
											<div class="dt-responsive table-responsive">
												<table id="Stagedata" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>ID</th>
															<th>Name</th>
															<th >Qty</th>
															<th>Unit</th>
														</thead>
														<?php 
														require_once '../products/productClass.php';
														require_once 'reportClass.php';
														$result=getstAllprducts();
														$Total= 0;
														while ($row=$result->fetch()) {

															if ($row['StockLevel']>= CurentProductStoreById($row['Id'])) {

																echo '
																<tbody>
																<tr>
																<td>'.$row['Id'].'</td>
																<td>'.$row['Name'].'</td>
																<td class="table-danger">'.CurentProductStoreById($row['Id']).'</td>
																<td>'.$row['Unit'].'</td>
																<tr>';?>
																<?php
															}
															# code...
														}


														?>
													</tbody>
												</table>
											</div>
										</div>

										<div class="card">
											<div class="card-header">
												<h4 class="text-success" >Material</h4>
												<span>Out stock <code>Material </code> List</span>
											</div>
											<div class="card-block">
												<div class="dt-responsive table-responsive">
													<table id="Stagedata" class="table table-striped table-bordered nowrap">
														<thead>
															<tr>
																<th>ID</th>
																<th>Name</th>
																<th>Qty</th>
																<th>Unit</th>
															</thead>
															<?php 
															require_once '../material/materialClass.php';
															require_once 'reportClass.php';
															$result=getMaterialList();
															$Total= 0;
															while ($row=$result->fetch()) {

																if ($row['StockLevel']>= CurentStoreMaterialById($row['Id'])) {

																	echo '
																	<tbody>
																	<tr>
																	<td>'.$row['Id'].'</td>
																	<td>'.$row['Name'].'</td>
																	<td class="table-danger">'.CurentStoreMaterialById($row['Id']).'</td>
																	<td>'.$row['Unit'].'</td>
																	<tr>';?>
																	<?php
																}
															# code...
															}


															?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once '../Include/script.php'; ?>
</body>
</html>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
