<?php

function getCurrenceSign(){
	return "$";
}

function getMainName(){
	return "Som Oil";
}

function getCurrentUser(){
	// isLoggedIn();
	return $_SESSION[getSessionName()];
}

function getCurrentRole(){
	isLoggedIn();
	return $_SESSION[getSessionName()];
}

function isLoggedIn() 
{ 
	if(!isset($_SESSION[getSessionName()]))
	{
		echo '<meta http-equiv="Refresh" content="0; URL=../index.php" />';  
		exit();
	}
}

function getSessionName(){
	return 'somOilUser';
}

function getSessionRole(){
	return 'somOilRole';
}


?>