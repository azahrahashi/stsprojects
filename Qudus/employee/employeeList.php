<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">

									</div>
									<div class="card">
										<div class="card-header">
											<h5>Manage Employee</h5>
											<a href="addEmployee.php" class="btn btn-success btn-round waves-effect md-trigger float-right" ><i class="icofont icofont-plus-alt"> </i> Add New </a>
										</div>
										<div class="card">
											<div class="card-header">
												<h5>Employee List</h5>

											</div>
											<div class="card-block">
												<div class="dt-responsive table-responsive">
													<table id="lists" class="table table-striped table-bordered nowrap">
														<thead>
															<tr>
																<th class="table-plus datatable-nosort"># ID</th>
																<th>Full Name</th>
																<th>Gender</th>
																<th>Telphone</th>
																<th>Address</th>
																<th>Designation</th>
																<th>Salary</th>
																<th width="10%">Action</th>
															</tr>

														</thead>
														<tbody>
															<?php
															// require_once '../common/commonValues.php';
															require_once'employeeClass.php';
															$result = getEmployeeLis();
															if (!empty($result)){
																while ($row = $result->fetch())
																{
																	$PersonId = $row['PersonId'];
																	echo'<tr>
																	<td>'. $row['PersonId'] .'</td>
																	<td class="table-plus">'. $row['FirstName'] .' '.$row['LastName'].'</td>
																	<td>'. $row['Gender'] .'</td>
																	<td>'. $row['TelephoneNo'] .'</td>
																	<td>'. $row['Address'] .'</td>
																	<td>'. $row['Role'] .'</td>

																	<td>'. $row['Amount'] .'</td>

																	<td>
																	<a href="viewEmployee?pId='.$PersonId.'" class="btn btn-info btn-icon" ><i class="icofont icofont-eye-alt"></i></a>

																	<a href="#"  onclick="getPersonwages('.$PersonId.')" class="btn btn-warning btn-icon" ><i class="icofont icofont-pencil"></i></a>
																	<a href="#" class="btn btn-danger btn-icon" onclick="RemoveEmployee('.$PersonId.')"><i class="icofont icofont-bin"></i></a>

																	</td>

																	</tr>';
																}
															}
															?>
														</tbody>
														<tfoot>
															<tr>
																<th class="table-plus datatable-nosort"># ID</th>
																<th>Full Name</th>
																<th>Gender</th>
																<th>Telphone</th>
																<th>Address</th>
																<th>Designation</th>
																<th>Salary</th>
																<th>Action</th>
															</tr>
														</table>
													</div>
												</div>
											</div>
										</div>

										<!-- Add Model -->
										<div class="modal fade" id="givesalary" >
											<div class="modal-dialog" role="document">
												<form method="post" id="user_form" enctype="multipart/form-data">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Salary Payment</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">

															<strong>Full Name: </strong> <span id="fName"></span>
															<br>
															<strong>Designation :</strong> <span id="type"></span>
															<br>
															<strong>Wages Rate: </strong> <span id="wage">$ 250.00 </span>
															<br><br>

															<div class="row">
																<div class="col-sm-6">
																	<label class="col-sm-12 col-md-12 col-form-label">Now Paying:<span style="color: red">*</span></label>
																	<div class="col-sm-12 col-md-12">
																		<input type="number" name="Amount" id="amount" class="form-control" />
																		<span style="color: red" class="error" id="amountErr">Plese Enter valid amount </span>

																	</div>
																</div>
																<div class="col-sm-6">
																	<label class="col-sm-12 col-md-12 col-form-label">Salary Type </label>
																	<div class="col-sm-12 col-md-12">
																		<select class="custom-select col-12" id="salaryType">
																			<option selected=""  value="">Select Salary Type</option>
																			<option  value="Full Salary">Full Salary</option>
																			<option  value="Advance">Advance</option>
																			<option  value="Other">Other</option>
																		</select>
																		<span style="color: red" class="error" id="salaryTypeErr">Plese Enter valid amount </span>
																	</div>
																</div>
															</div>
															<br>
															<br>

															<input type="hidden" name="Amount" id="hiddenId" class="form-control" />
															<input type="hidden" name="Amount" id="createdBy"  value="Abdinoor test" />


															<div class="modal-footer">
																<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
																<button type="button" class="btn btn-primary waves-effect waves-light " onclick="paysalary()">Save changes</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>

										<!-- End Add Model -->

								</div>
							</div>
						</div>
					</div>

					<?php require_once '../Include/script.php'; ?>
				</body>
				</html><script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>

				<script src="salaryJs.js"></script>

				<script type="text/javascript">
					
					function RemoveEmployee(PersonId) {
						// alert(PersonId);
						$.post("deleteEmployee.php",{
							 		PersonId:PersonId
							 	},
							 	function (data, status) {
							 		alert("Successfully Removed");
							 	});
					}
				</script>
