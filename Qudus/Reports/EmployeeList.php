<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Employee Report</h4>
														<span>This is the <code>Employee</code>Report</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">
											
											<br/>
											<div class="dt-responsive table-responsive">
												<table id="simpletable" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>Id</th>
															<th>Firs Name</th>
															<th>Last Name</th>
															<th>Gender</th>
															<th>Email</th>
															<th>Date</th>
														</tr>
													</thead>

													<?php
													require_once 'reportClass.php';
													$result=GetEmployeeList();
													while ($row=$result->fetch()) {
														echo '
														<tr>
														<td>'.$row['EmploymentId'].'</td>
														<td>'.$row['FirstName'].'</td>
														<td>'.$row['LastName'].'</td>
														<td>'.$row['Gender'].'</td>
														<td>'.$row['Email'].'</td>
														<td>'.$row['StartDate'].'</td>
														</tr>';?>
														<?php
														
													}
													?>

													<tbody>

													</tbody>

												</tbody>
											</table>
										</div>



									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require_once '../Include/script.php'; ?>
</body>
</html>

<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#lists').DataTable();
	});
</script>
