
<?php
//session_start();
//unset($_SESSION['shopping_cart']);
?>
<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Purchases</h4>
														<span>This Is The <code>Purchases</code> Managment Page.</span>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="page-header-breadcrumb">
													<ul class="breadcrumb-title">

													</ul>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-header">
										</div>

										<div class="card-block">
											<div>
												<a href="newPurchases.php"><button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#"><i class="fa fa-plus"></i> Register</button></a>
											</div><br><br><br>
											<div class="dt-responsive table-responsive">
												<table id="listss" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
															<th>NO</th>
															<th>Supplier</th>
															<th>Sub Total</th>
															<th>Discount</th>
															<th>Total Amount</th>
															<th>Payment Method</th>
															<th>Date</th>
															<th style="width: 10%;">Action</th>
														</tr>
													</thead>
													<?php
													require_once 'PurchasesClass.php';
													$result=getPurchases();
													$i=0;
													while ($row=$result->fetch()) {
														$i++;
														echo '
														<tbody>
														<tr>
														<td>'.$i.'</td>
														<td>'.$row['CompanyName'].'</td>
														<td>$'.$row['Amount'].'</td>
														<td>$'.$row['Discount'].'</td>
														<td>$'.$row['TotalAmount'].'</td>
														<td>'.$row['Method'].'</td>
														<td>'.date("d/M/Y",strtotime($row['StartDate'])).'</td>
														<td>
														<a class="btn btn-success"
														href="#"  onClick="gotoUpdate('.$row['PurchaseId'].')" > Update</a>
														 <button class="btn btn-info" onClick="seeDetail('.$row['PurchaseId'].')" ><i class="fa fa-bin">See Details</button>
														<button class="btn btn-warning" onClick="Remove('.$row['PurchaseId'].')" ><i class="fa fa-bin">Remove</button>
														</td>
														</tr>';
													}
													?>
												</tbody>
											</table>
										</div>

										<!-- Detail Model -->
										<?php require_once 'Models.php'; ?>
										<!-- End Detail Model -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php require_once '../Include/script.php'; ?>
</body>
</html>
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script type="text/javascript" src="../files/assets/pages/j-pro/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="../files/assets/pages/j-pro/js/jquery-cloneya.min.js"></script>
<script type="text/javascript">

									$(document).ready( function () {
										// $('#Purchases').DataTable();

										// $('#fondationErr').hide();

									});
									function seeDetail(PurchaseId) {

						            $.post("getPurchaseDetails.php", {
						                PurchaseId: PurchaseId,
						            },function (data, status) {
						                 // alert(data)
						                $('#Showtheresult').html(data);
						                $("#purchasesDetials").modal("show");
						            });
								}

                          function Remove(PurchaseId) {

						            $.post("removePurchase.php", {
						                PurchaseId: PurchaseId,
						            },function (data, status) {
						                // alert("data is here " +data);
						                //alert("data is here " +data);
						                // $('#Showtheresult').html(data);
						                // $("#purchasesDetials").modal("show");
										window.location.replace('purchaseList.php');
						            });
								}


								function gotoUpdate(PurchaseId) {
									localStorage.setItem('PurchaseId',PurchaseId);
									location.replace('updatePurchases.php');
								}

                            </script>


														<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
														<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
														<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
														<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
														<script type="text/javascript">
														$(document).ready(function() {
															$('#lists').DataTable();
														});
														</script>
