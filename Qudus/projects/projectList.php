<!DOCTYPE html>
<html>
<?php require_once '../Include/head.php'; ?>
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
<body>
	<div id="pcoded" class="pcoded">
		<div class="pcoded-overlay-box"></div>
		<div class="pcoded-container navbar-wrapper">
			<?php require_once '../Include/navbar.php'; ?>
			<div class="pcoded-main-container">
				<div class="pcoded-wrapper">
					<?php require_once '../Include/sidebar.php'; ?>

					<div class="pcoded-content">
						<div class="pcoded-inner-content">
							<div class="main-body">
								<div class="page-wrapper">
									<div class="page-header">
										<div class="row align-items-end">
											<div class="col-lg-8">
												<div class="page-header-title">
													<div class="d-inline">
														<h4>Manage Projects</h4>
														<span>This Is The <code>Project</code> Managment Page.</span>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="card">
										<div class="card-block">

											<div>
												<a href="newProjectDoc.php"><button style="margin:10px" class="btn btn-success btn-round waves-effect md-trigger float-right"><i class="icofont icofont-plus-alt"> </i>Project Ducoment</button></a>

												<button style="margin:10px" class="btn btn-success btn-round waves-effect md-trigger float-right" data-toggle="modal" data-target="#default-Modal"><i class="icofont icofont-plus-alt"> </i> Add New</button>

											</div>
											<br><br>
											<br>
											<div class="dt-responsive table-responsive">
												<table id="lists" class="table table-striped table-bordered nowrap">
													<thead>
														<tr>
														<th>No</th>
														<th>Project Name</th>
														<th>Owner</th>
														<th>Describtion</th>
														<th>Start Date</th>
														<th width="5%">Action</th>
														</tr>
													</thead>
													<tbody>
														<?php
																 // require_once '../common/commonValues.php';
																 require_once'projectClass.php';
																	$result = getProjectList();
																		 if (!empty($result)){
																		 while ($row = $result->fetch())
																	{
																	 $ProjectId = $row['ProjectId'];

																	 echo '
															 <tr>

																 <td>'. $row['ProjectId'] .'</td>
																 <td>'. $row['Name'] .' </td>
																 <td>'. $row['Owner'] .' </td>
																 <td>'. $row['Describtion'] .'</td>
																 <td>'. $row['StartDate'] .'</td>
																 <td>
																	 <a href="projectView.php?pId='.$ProjectId.'" class="btn btn-info btn-icon" ><i class="icofont icofont-eye-alt"></i></a>
																	 <a href="../RateAnalysis/sampleRate.php?pId='.$ProjectId.'" class="btn btn-success btn-icon" ><i class="icofont icofont-plus"></i></a>
																	 <a href="#"class="btn btn-warning btn-icon" onclick="getProjectDetail('.$ProjectId.')"><i class="icofont icofont-pencil"></i></a>
																	 <a href="#" class="btn btn-danger btn-icon" onclick="RemoveProject('.$ProjectId.')"><i class="icofont icofont-bin"></i></a>
																 </td>
															 </tr>';
																	}}
															 ?>
													</tbody>
												</table>
											</div>

											<!-- Add project Model -->

											<div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title">Project Register</h4>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<span style="color: red;" Id="error">Fadlan Buuxi meelaha banaan !!!</span>
															<div class="form-group row">
																<label class="col-sm-3 col-form-label">Project Name</label>
																<div class="col-sm-9">
																	<input type="hidden" class="form-control" id="pId">
																	<input type="text" class="form-control" id="pName">
																</div>
															</div>
															<div class="form-group row">
																<label class="col-sm-3 col-form-label">Project Owner</label>
																<div class="col-sm-9">
																	<input type="text" class="form-control" id="pOwner">
																</div>
															</div>
															<div class="form-group row">
																<label class="col-sm-12 col-form-label">Describtion</label>
																<div class="col-sm-12">

																	<textarea id="pDetail" rows="5" cols="5" class="form-control" placeholder="Write Some Describtion here"></textarea>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
															<button type="button" class="btn btn-primary waves-effect waves-light " onclick="saveproject();">Save changes</button>
														</div>
													</div>
												</div>
											</div>





										</div>
									</div>

								</div>
							</div>
						</div>
					</div>

				</div>
				<!--  -->

				<?php require_once '../Include/script.php'; ?>
			</body>
			</html><script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
			<?php require_once 'projectJs.php'; ?>


			<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
			<script type="text/javascript" src="../files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
			<script src="../files/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
			<script src="../files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
			<script type="text/javascript">
			$(document).ready(function() {
				$('#lists').DataTable();
			});
			</script>
